#version 330 core

in vec2 UV;

out vec4 fragmentColor;

uniform sampler2D tex;

void main() {
	fragmentColor = texture(tex, UV).rgba;
}
