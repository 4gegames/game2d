#version 330 core

out vec4 fragmentColor;

uniform vec4 RGBA;

void main() {
	fragmentColor = RGBA;
}
