#include "Game.hpp"

FPSCounter* Game::fps;
Controllers* Game::ctrl;
Settings* Game::settings;
Renderer* Game::renderer;
Audio* Game::audio;
ResourceManager* Game::resources;

SDL_Rect Game::screenrect;

void Game::Init()
{
	Log("____________________________________________\n");
	Log("***************INITIALIZATION***************\n");

	time_t ptime;
	time( & ptime );
	char* date = ctime(&ptime);
	Log("Date: ", date);

	settings =new Settings;

	screenrect = Game::settings->options.screenrect;

	Log("language: ",settings->options.language,"\n");

	fps =new FPSCounter;
	ctrl=new Controllers();
	audio =new Audio;
	audio->SetVolumeMusic(settings->options.volumeMusic); //0..128
	audio->SetVolumeSounds(settings->options.volumeSound); //0..128

	switch(settings->options.renderer)
	{
		case RendererType::OGL2:
			renderer=new RendererOGL2(settings);
			break;
		case RendererType::OGL2Shader:
			//renderer=new RendererOGL2Shader();
			break;
		case RendererType::OGL3:
			renderer = new RendererOGL3(settings);
			static_cast<RendererOGL3*>(renderer)->Init();
			break;
		case RendererType::OGLES:
			//renderer=new RendererOGLES();
			break;
	}

	resources = new ResourceManager(settings, renderer);

	Log("____________________________________________\n");
	Log("********************GAME********************\n");
}

void Game::Clean()
{
	delete audio;
	delete fps;
	delete ctrl;
	delete resources;
	delete renderer;
	delete settings;

}
