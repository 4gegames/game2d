#include "Settings.hpp"


Settings::Settings()
{
	LoadSettings();
	InitSDL();
	ComputeResolution();
	WindowCreate();
	mini=IMG_Load("Data/Images/mini.png");
	SDL_SetWindowIcon(window, mini);
	SDL_FreeSurface(mini);

	Log("Video driver: ",SDL_GetCurrentVideoDriver(),"\n");
}

Settings::~Settings()
{
	SDL_DestroyWindow(window);
	TTF_Quit();

	SDLNet_Quit();
	SDL_Quit();
}


void Settings::LoadSettings()
{
	std::fstream text_file;
	std::string line;
	text_file.open( "Data/settings.ini", std::ios::in);
	if( text_file.good() == false ) { }

	while(text_file.good())
	{
		getline(text_file, line);
		sets.push_back(line);
	}
	text_file.close();
	options.resolution_x = std::stoi(sets[10]);

	options.resolution_y = std::stoi(sets[11]);
	options.fullscreen= std::stoi(sets[13]);
	options.multisampling= std::stoi(sets[15]);
	options.vsync= std::stoi(sets[17]);
	options.volumeMusic= std::stoi(sets[19]);
	options.volumeSound= std::stoi(sets[21]);
	options.language= sets[23];

	std::string rendererversion = sets[25];

	if(rendererversion == "OGL3")
		options.renderer = RendererType::OGL3;
	else //default OGL2
		options.renderer = RendererType::OGL2;


	options.userPath= sets[27];
	options.velocityIterations = std::stoi(sets[29]);
	options.positionIterations = std::stoi(sets[31]);

	options.first_run= std::stoi(sets[8]);
}

void Settings::ComputeResolution()
{
	SDL_DisplayMode current;

	if(SDL_GetCurrentDisplayMode(0, &current) == 0)
	{
		Log("current.w:", current.w,", current.h: ",current.h,", first run=",options.first_run,"\n");
		if(options.resolution_x <= current.w && options.resolution_x >=640
				&& options.resolution_y <= current.h && options.resolution_y >=480
				&& options.first_run == false)
		{
			options.default_resolution_y = 1050;
			if(options.resolution_x/options.resolution_y>= 1.3&&options.resolution_x/options.resolution_y<= 1.4) //4:3
			{
				options.default_resolution_x = 1400;//std::stoi(sets[1]);
				options.resRatio = ScreenRatio::ratio4x3;
				options.shift=43;  //(1400-1312)/2
			}
			else if(options.resolution_x/options.resolution_y>= 1.2&&options.resolution_x/options.resolution_y<= 1.3) //5:4
			{
				options.default_resolution_x = 1312;
				options.resRatio = ScreenRatio::ratio5x4;
				options.shift=0;
			}
			else if(options.resolution_x/options.resolution_y>= 1.7&&options.resolution_x/options.resolution_y<= 1.8) //16:9
			{
				options.default_resolution_x = 1866;
				options.resRatio = ScreenRatio::ratio16x9;
				options.shift=277; // if count as 5:4
			}
			else if(options.resolution_x/options.resolution_y>= 1.55&&options.resolution_x/options.resolution_y<= 1.65) //16:10
			{
				options.default_resolution_x = 1680;
				options.resRatio = ScreenRatio::ratio16x10;
				options.shift= 184; // if count as 5:4
			}
			else
			{
				options.resolution_x = 800;
				options.resolution_y = 600;
				Log("bad ratio resolution\n");
				ComputeResolution();
			}
		}
		else
		{
			options.resolution_x = current.w;
			options.resolution_y = current.h;
			options.fullscreen = true;
			options.first_run = false;
			options.multisampling = 2;
			SaveRes();
			ComputeResolution();
		}
	}
	else
	{
		Log("Could not get display mode for video display #:  " , SDL_GetError(),"\n");
		options.resolution_x = 800;
		options.resolution_y = 600;
		options.default_resolution_x = 1400;
		options.default_resolution_y = 1050;
		options.resRatio = ScreenRatio::ratio4x3;
		options.shift = 43;  //(1400-1312)/2
		SaveRes();
	}

	options.ratio_x = options.resolution_x / options.default_resolution_x;
	options.ratio_y = options.resolution_y / options.default_resolution_y;

	options.screenrect = {0, 0, static_cast<int>(options.resolution_x),
						  static_cast<int>(options.resolution_y)};

	Log("Compute resolution: ",options.resolution_x,"x",options.resolution_y,"\n");
}

void Settings::SaveRes()
{
	sets[8] = std::to_string(options.first_run);
	sets[10] = std::to_string(options.resolution_x);
	sets[11] = std::to_string(options.resolution_y);
	sets[13] = std::to_string(static_cast<int>(options.fullscreen));
	sets[15] = std::to_string(options.multisampling);

	SaveSettings();
}

void Settings::InitSDL()
{
	SDL_Init(SDL_INIT_EVERYTHING);

	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	//SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	//SDL_GL_SetAttribute( SDL_GL_ALPHA_SIZE, 0 );

	if(options.multisampling != 0)
	{
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, options.multisampling);
	}
	else
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 0);

	SDLNet_Init();
	TTF_Init();

	SDL_ShowCursor(false);

	srand(time(NULL));
}

void Settings::WindowCreate()
{
	Uint32 flags;
	if(options.fullscreen == 1)
		flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN;
	else
		flags = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;

	window = SDL_CreateWindow("Window name", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
							  options.resolution_x, options.resolution_y, flags);

	if( !window)
	{
		Log("Unable to create Window\n");
		exit(1);
	}
}

void Settings::SaveSettings()
{
	std::fstream settings_file;
	settings_file.open( "Data/settings.ini", std::ios::out );//| ios::trunc);
	settings_file << sets[0];
	for(unsigned int i = 1; i < sets.size(); i++)
	{
		settings_file << "\n" << sets[i];
	}
	settings_file.close();
}

