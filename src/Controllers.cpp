#include "Controllers.hpp"

Key::Key(KeyType ptype, int value)
{
	type=ptype;
	if(type==KeyType::KEYBOARD)
		code= static_cast<SDL_Scancode>(value);
	else if(type==KeyType::MOUSE)
		mousebutton=value;
	else if(type==KeyType::MOUSEWHEEL)
		mousewheel=value;
	else //joystick
		Log("joystick key is not yet supported\n");
}

void Controllers::LoadKeyboardControls()
{
	std::fstream key_file;

	key_file.open("User/keyboard.csv", std::ios::in);
	KeyType type;
	std::string name, name2, value;
	while(key_file.good())
	{
		getline(key_file, name, ';');
		getline(key_file, name2, ';');
		getline(key_file, value, '\n');

		switch(std::stoi(name2))
		{
			case 0:
				type=KeyType::KEYBOARD;
				break;
			case 1:
				type=KeyType::MOUSE;
				break;
			case 2:
				type=KeyType::MOUSEWHEEL;
				break;
			default:
				type=KeyType::JOYSTICK;
		}

		keys[name] =new Key(type, std::stoi(value));
	}
}

Controllers::Controllers()
{
	mouse_x = 0;
	mouse_y = 0;

	click_x = 0;
	click_y = 0;

	wheel_timer = SDL_GetTicks();

	LoadKeyboardControls();
	Reset();
	Log("Init Controllers\n");
}

Controllers::~Controllers()
{
	Log("Close Controllers\n");
}

void Controllers::Reset()
{
	keystate = const_cast<Uint8*>(SDL_GetKeyboardState(&key_size));
	for(int i = 0; i < key_size; i++)
	{
		keystate[i] = false;
	}
	lmb_state = MouseState::NOTPRESSED;
	for(int i = 0; i < 8; i++)
		mousekey[i] = false;

	wheelstate[0] = false;
	wheelstate[1] = false;
}

void Controllers::Update()
{
	keystate = const_cast<Uint8*>(SDL_GetKeyboardState(NULL));

	SDL_GetMouseState(&mouse_x, &mouse_y);
	for(int i = 1; i <= 8; i++)
	{
		if(SDL_GetMouseState(NULL, NULL)&SDL_BUTTON(i))
		{
			mousekey[i-1] = true;
		}
		else
			mousekey[i-1] = false;
	}

	static bool active_lmb = false;
	static float lmb_timer = 0;

	if(mousekey[0] == true)
	{
		active_lmb = true;
	}
	else if(active_lmb)
	{
		lmb_state = MouseState::BUTTONUP;
		lmb_timer = SDL_GetTicks()+100;
		active_lmb = false;
	}

	if( !active_lmb && lmb_timer<SDL_GetTicks())
	{
		lmb_state = MouseState::NOTPRESSED;
	}

	if(wheel_timer < SDL_GetTicks())
	{
		wheelstate[0] = false;
		wheelstate[1] = false;
	}
}

void Controllers::HandleEvent(SDL_Event& event)
{
	if(event.type == SDL_MOUSEBUTTONDOWN)
	{
		lmb_state = MouseState::BUTTONDOWN;
		click_x = mouse_x;
		click_y = mouse_y;
	}

	if(event.type == SDL_MOUSEWHEEL)
	{
		int wheelvalue = event.wheel.y;
		if(wheelvalue>0)
		{
			wheelstate[0] = true;
			wheelstate[1] = false;
		}
		else
		{
			wheelstate[0] = false;
			wheelstate[1] = true;
		}
		wheel_timer = SDL_GetTicks()+300;
	}
}

Key* Controllers::GetKey(const std::string& name)
{
	if(keys.find(name) == keys.end())
	{
		Log("Key \"", name, "\" not found\n");
		assert(0);
	}
	return keys.find(name)->second;
}

bool Controllers::Check(Key* k)
{
	if(k != nullptr)
	{
		switch( k->type )
		{
			case KeyType::MOUSE:
				if(mousekey[k->mousebutton])
					return true;
				break;
			case KeyType::KEYBOARD:
				if(keystate[k->code])
					return true;
				break;
			case KeyType::MOUSEWHEEL:
				if(wheelstate[k->mousewheel])
					return true;
				break;
			case KeyType::JOYSTICK:
				return false; //temporary
		}
	}
	return false;
}

bool Controllers::Check(const std::string& name)
{
	if(name!="")
	{
		if(keys.find(name)==keys.end())
			Log("Key \"",name,"\" not found\n"), assert(0);

		switch( keys.find(name)->second->type )
		{
			case KeyType::MOUSE:
				if(mousekey[keys.find(name)->second->mousebutton])
					return true;
				break;
			case KeyType::KEYBOARD:
				if(keystate[keys.find(name)->second->code])
					return true;
				break;
			case KeyType::MOUSEWHEEL:
				if(wheelstate[keys.find(name)->second->mousewheel])
					return true;
				break;
			case KeyType::JOYSTICK:
				return false; //temporary
		}
	}
	return false;
}

bool Controllers::Check(const std::string& name, const std::string& name2)
{
	if(Check(name) && Check(name2))
		return true;
	return false;
}

bool Controllers::Check(const std::string& name, const std::string& name2, const std::string& name3)
{
	if(Check(name) && Check(name2) && Check(name3))
		return true;
	return false;
}

bool Controllers::TestClickInArea(SDL_Rect area)
{
	if(click_x > area.x && click_x < area.x + area.w)
	{
		if(click_y > area.y && click_y < area.y + area.h)
			return true;
	}

	return false;
}
