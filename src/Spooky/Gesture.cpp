#include "Gesture.hpp"

float VertexDistance(glm::vec2 v1, glm::vec2 v2)
{
	return sqrt( pow(v1.x-v2.x, 2) + pow(v1.y-v2.y, 2));
}

GestureManager::GestureManager()
{
	std::fstream file;

	file.open("Data/gestures.csv", std::ios::in);

	std::string id, text, action_id;
	getline(file, text, '\n'); //first line with headers
	while(file.good())
	{
		getline(file, id, ';');
		getline(file, text, ';');
		getline(file, action_id, '\n');

		Gesture g;
		for(size_t i = 0; i < text.size(); i++)
		{
			g.directions.push_back( static_cast<GestureDirection::Direction>(text[i] - '0') );
		}
		g.actionid = std::stoi(action_id);

		gestures.push_back(g);
	}

	file.close();

	mode = false;
}

GestureManager::~GestureManager()
{

}

void GestureManager::Render(Camera* cam)
{
	//NEED FIX, only temporary
	std::vector<float> points;

	for(unsigned int i = 0; i < gesturepoints.size(); i++)
	{
		points.push_back(gesturepoints[i].x);
		points.push_back(gesturepoints[i].y);
	}

	Game::renderer->RenderEdges(points, cam, &Game::resources->colors.yellow, GL_LINE_STRIP);
}

int GestureManager::Update(bool attachcam, int distance)
{
	static unsigned int timer = SDL_GetTicks();
	int gesturenumber = -1;

	//Gestures work only in normal mode, not tactic
	if(Game::ctrl->mousekey[2] && attachcam)
	{
		if( !mode) //start new gesture
		{
			gesturepoints.clear();

			startpos.x = Game::ctrl->mouse_x;
			startpos.y = Game::ctrl->mouse_y;
		}

		mode = true;

		if(timer < SDL_GetTicks())
		{
			glm::vec2 p(Game::ctrl->mouse_x, Game::ctrl->mouse_y);

			if( gesturepoints.empty() || (VertexDistance(p, gesturepoints.back()) > distance) )
			{
				gesturepoints.push_back(p);
			}

			timer = SDL_GetTicks() + 30;
		}
	}
	else if(mode)
	{
		mode = false;

		if(gesturepoints.size() >= 2)
		{
			std::cout<<"gesture with "<< gesturepoints.size() <<" points\n";
			std::vector<GestureDirection::Direction> tempdir;
			tempdir.reserve( (gesturepoints.size()+1)/2 );

			for(size_t i = 0; i < gesturepoints.size()-1; i++)
			{
				glm::vec2 totarget = gesturepoints[i+1] - gesturepoints[i];
				float angle = atan2f( totarget.x, totarget.y ); //maybe -totarget.x

				GestureDirection::Direction dir = ConvertDirection(angle);

				tempdir.push_back(dir);
			}

			//must save data to Gesture directions container
			Gesture g;
			g.directions.reserve(tempdir.size());

			g.directions.push_back(tempdir[0]); //add first direction
			for(size_t i = 1; i < tempdir.size() ; i++)
			{
				if(tempdir[i-1] != tempdir[i])
					g.directions.push_back(tempdir[i]);
			}

			//directions without replace
			for(size_t i = 0; i < g.directions.size(); i++)
			{
				std::cout<<g.directions[i];
			}
			std::cout<<"\n";

			gesturenumber = FindCorrespondGesture(g);
			std::cout<<"Gesture number: " << gesturenumber <<"\n";
		}
		else
		{
			std::cout<<"this gesture have not enough points\n";
		}
	}

	///find action which is assigned to this gesture
	int action_id = GetActionId(gesturenumber);

	return action_id;
}

int GestureManager::FindCorrespondGesture(Gesture& g)
{
	size_t i = 0;
	while(!GestureComparer(g, gestures[i]) )
	{
		i++;

		if(i == gestures.size())
			return -1;
	}

	return i;
}

bool GestureManager::GestureComparer(Gesture& g1, Gesture& g2)
{
	if(g1.directions.size() != g2.directions.size())
	{
		return false;
	}
	size_t good = 0;
	for(size_t i = 0; i < g1.directions.size(); i++)
	{
		if(g1.directions[i] == g2.directions[i])
			good++;
		//maybe else block with attempt to error? //attempt to reduce - trying of reduce
	}
	if(good < g1.directions.size())
		return false;
	else
		return true;
	//we can add little margin of error in this function
}


GestureDirection::Direction GestureManager::ConvertDirection(float angle)
{
	GestureDirection::Direction dir;

	float halfrange = PI / 8; //full rotate = 2 PI, divide to 8 directions, and a half

	if(angle <= (-PI + halfrange) || angle >= (PI - halfrange))
		dir = GestureDirection::N;
	else if(angle >= (PI - halfrange*3) && angle <= (PI - halfrange))
		dir = GestureDirection::NE;
	else if(angle >= (PI - halfrange*5) && angle <= (PI - halfrange*3))
		dir = GestureDirection::E;
	else if(angle >= (PI - halfrange*7) && angle <= (PI - halfrange*5))
		dir = GestureDirection::SE;

	else if(angle >= (-halfrange) && angle <= (halfrange))
		dir = GestureDirection::S;
	else if(angle >= (-PI + halfrange*5) && angle <= (-PI + halfrange*7))
		dir = GestureDirection::SW;
	else if(angle >= (-PI + halfrange*3) && angle <= (-PI + halfrange*5))
		dir = GestureDirection::W;
	else if(angle >= (-PI + halfrange) && angle <= (-PI + halfrange*3))
		dir = GestureDirection::NW;
	else
		dir = GestureDirection::NONE;

	return dir;
}

int GestureManager::GetActionId(int gesture_id)
{
	if(gesture_id >= 0)
		return gestures[gesture_id].actionid;
	else return -1;
}
