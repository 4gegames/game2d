#ifndef TILEMAP_HPP_INCLUDED
#define TILEMAP_HPP_INCLUDED

#include<Box2D/Box2D.h>
#include <vector>

#include "../Utils/Log.hpp"
#include "../ResourceManager/ResourceManager.hpp"


enum class LayerType
{
	TILE,
	IMAGE,
	OBJECT
};

enum class Shape
{
	RECTANGLE,
	POLYGON,
	ELLIPSE,
	POLYLINE // == chain
};

enum class MapObjectType
{
	COLLISION,
	DESTRUCTIBLE,
	//to do - more types

	SPAWNPOINT,
	ACTIONPOINT,
	TRIGGER,

	PARTICLEEMITTER,
	SOUNDEMITTER,

	///testing types
	UNIT,
	BULLET,
	PLAYER,
	SENSOR,

	NONE
};

struct SoundEmitterData
{
	std::string soundname;
	int deltatime;
	int shift;

	SoundEmitterData()
	{
		soundname = "error";
		deltatime = 1000;
		shift = 0;
	}
};

struct MapObject
{
	std::string name;
	Shape shapetype;
	MapObjectType type;

	float x, y; //left corner in pixels
	float width, height; // important for rectangle and ellipse

	b2Vec2* vertices;
	int vertexcount;

	SoundEmitterData soundemitter;

	MapObject()
	{
		type = MapObjectType::NONE;
		vertices = nullptr;
	}
	~MapObject()
	{
		delete []vertices;
	}
};

struct Tile
{
	int value;
	Tile(int v)
		: value(v)
	{ }
};

struct Tileset
{
	public:
		std::string name;
		std::string imgname;

		int imgwidth;
		int imgheight;

		int firstid;

		int spacing;
		int margin;

		//must add spacing and margin parameter, because aliasing tiles have color adjacent tiles
		//must handle this in Tiled map constructor
};



struct UsedTileset
{
	TexturePTR texturedata;
	TextureSrc source;
};

class Layer
{
	public:
		LayerType type;
		std::string name;
		bool visible;
		float opacity;

		virtual ~Layer()
		{

		}
};

class TileLayer : public Layer
{

	public:
		TileLayer(int width, int height);
		~TileLayer();
		std::vector< std::vector<Tile*> > tiles; //256x256 //must allocate enough memory to Tile*256*256

		int shiftx; //possible it's unnecessary
		int shifty;
};

class ImageLayer : public Layer
{
	public:
		ImageLayer()
		{
			type=LayerType::IMAGE;
		}

};

class ObjectLayer : public Layer
{
	public:
		ObjectLayer();
		~ObjectLayer();

		std::vector<MapObject*> objects;
};

class TiledMap
{
	public:
		std::vector<Tileset*> tilesets;
		TileLayer* tilelayer;

		ObjectLayer* collisionlayer;
		ObjectLayer* actionlayer;
		std::vector<ObjectLayer*> objectlayers;
		std::vector<ImageLayer*> imglayers;

		float mapversion;

		int width;
		int height;

		int tilewidth;
		int tileheight;
		//properties
		//maybe total width and height, tile.w tile.h

		int totalmapwidth;
		int totalmapheight;

		SDL_Rect walkable; // area, where player can walk, must automatically calculate

		std::vector<UsedTileset*> tilemodel;
		//vector with size equals number of tiles in all tilesets,which contains
		//name of using tileset and TextureSrc for tile with this value

		TiledMap();
		~TiledMap();

		void Init(ResourceManager* resources);
};


#endif // SPOOKY_HPP_INCLUDED
