#ifndef SPOOKY_HPP_INCLUDED
#define SPOOKY_HPP_INCLUDED

#include<queue>
#include<unordered_map>

#include "../GuiObjects/Panel.hpp"
#include "../States/GameplayState.hpp"
#include "../ScriptEngine.hpp"
#include "SpookyObjects.hpp"
#include "Gesture.hpp"
#include "SoundWaves.hpp"

class SpookyGameplay : public GameplayState
{
		SDL_Rect viewport; //rect which is calculate in CalculateViewport(),
		//we can align new panels to him
		void CalculateViewport();

		void InitGui();
		void AddGuiObject(Panel* obj);

		Panel* statspanel;
		Panel* panelminimap;
		GuiObject* minimap; // child of panel

		/*MINIMAP
		in Update() is minimap moving, but it's working only for 1 pixel per tile in _minimap.png
		must make zoom feature, and shift of minimap should be depend of zoom level
		must add player icon in minimap
		*/

		float timemultiplier = 1.0f;

		//ARMORS!
		std::map <std::string, ArmorType> armortypes;

		void LoadArmorTypes();

		////////////////////////////////////////////////////////////////////////
		//Player hero
		////////////////////////////////////////////////////////////////////////
		Player* mainplayer;
		Player* enemyplayer;


		Character* hero = nullptr;


		GuiObject* hpframe;
		GuiObject* hpbar;
		int hpbar_length;
		int lasthp; //last known value of hero hp

		GuiObject* staminaframe;
		GuiObject* staminabar;
		int staminabar_length;
		int laststamina; //last known value of hero stamina

		bool autorotate;

		void CreateHero();
		void HeroControl();

		int killed; //number of killed enemies


		Spell* spells[7]; //or more
		/*
		tab with shortcuts to spells, key names: spell0, spell1 .. spell6
		*/

		Spell* activespell;

		struct ArenaWave
		{
			int zombie1;
			int zombie2;
			int zombietank;
			int archer;
			int boss;

			int deltatime;
			int delay; //additional time after spawning
		};

		std::vector<int> enemiesqueue; //int or enum ZombieType

		unsigned int actualwave;

		std::vector<ArenaWave> arenawaves;

		void InitArenaGameplay();

		void StartArenaWave(int number);

		int alive = 0; //number of alive enemies

		SpawnPoint* spawnboss = nullptr;
		int bossnumber = 0;

		GuiObject* textwave = nullptr;
		GameTimer wavetimer;

		bool beforewavetime = false;

		GuiObject* endwavebonus = nullptr;
		bool deletingbonus = false;

		void UpdateArenaGameplay();

		unsigned int musictimer; //time to fade in music
		bool startmusic = false;

		bool bossmusic = false;

		////////////////////////////////////////////////////////////////////////
		//Physic
		////////////////////////////////////////////////////////////////////////
		Bullet* AddBullet(const std::string& name, b2Vec2 source, b2Vec2 target);
		//Load values for bullet with name, from container (which include structures from adequate file)


		////////////////////////////////////////////////////////////////////////
		//Units
		////////////////////////////////////////////////////////////////////////
		std::map <std::string, UnitData> units;

		void LoadUnits();
		Unit* AddUnit(const std::string& name, b2Vec2 pos, Player* owner);

		////////////////////////
		ContactListener listener;


		////////////////////////////////////////////////////////////////////////
		//Sounds
		////////////////////////////////////////////////////////////////////////
		SoundWaveManager sounds;

		////////////////////////////////////////////////////////////////////////
		//Stats
		////////////////////////////////////////////////////////////////////////
		std::map<std::string,float> stats;
		//std::unordered_map<std::string,float> stats;
		void LoadStats();
		float GetStats(const std::string& name);

		////////////////////////////////////////////////////////////////////////
		//Camera
		////////////////////////////////////////////////////////////////////////
		float cam_centered_zoom;
		float cam_tactic_zoom;
		float cam_edit_zoom;

		bool camattach; //attach camera to hero - center in player character

		void CameraControl();

		void CameraAttach();
		void CameraDetach();
		void CameraDetachEdit();

		////////////////////////////////////////////////////////////////////////
		//map
		////////////////////////////////////////////////////////////////////////
		TiledMap* tmap;
		std::string mapname;
		void MapInit(const std::string& name);
		void DeleteMap();

		void AddMapObject(MapObject* obj);
		MapObjectPos CalcMapObjectPos(MapObject* obj);

		void ChangeMap(const std::string& name);

		////////////////////////////////////////////////////////////////////////
		//Map objects
		////////////////////////////////////////////////////////////////////////
		std::vector<SpawnPoint*> spawnpoints;
		std::vector<ActionPoint*> actionpoints;
		std::vector<ParticleEmitter*> particleemitters;
		std::vector<SoundEmitter*> soundemitters;

		////////////////////////////////////////////////////////////////////////
		//Obstacles
		////////////////////////////////////////////////////////////////////////

		std::map<std::string, ObstacleData> obstacles;

		void LoadObstacles();

		////////////////////////////////////////////////////////////////////////
		//Bullets
		////////////////////////////////////////////////////////////////////////
		std::map<std::string, BulletData> bullets;

		void LoadBullets();

		//MUST move call this function to Map Initialization
		void LoadObstaclePositions();
		Obstacle* AddObstacle(const std::string& name, b2Vec2 pos, float angle = 0.0f,
							  b2Vec2 scaleratio = b2Vec2(1.0f, 1.0f));

		void SaveObstacles();

		////////////////////////////////////////////////////////////////////////
		//Editor
		////////////////////////////////////////////////////////////////////////

		//load/save game state from external file
		void LoadGame(const std::string& name);
		void SaveGame(const std::string& name);

		bool editmode = false;
		bool enteredit = true;

		float angle = 0.0f;

		GameplayObject* attachcursor = nullptr;

		Panel* edittoolbar;

		bool firsteditrun = true;
		void InitEditMode(); //call only once

		void OpenEditMode(); //call always when press F5
		void UpdateEditMode();
		void CloseEditMode();

		void SetActiveTool(EditModeTool tool);
		void CloseLastTool();

		EditModeTool activetool = EditModeTool::NONE;
		EditModeTool lasttool = EditModeTool::NONE;

		bool toolinit = false;

		std::map<EditModeTool, bool> tools;

		bool selectframe = false;
		std::vector<float> selectvertices; // size 8

		std::vector<GameplayObject*> selectedobjs;

		//filters for selecting
		int editorfilter = 0;
		int gameplayfilter = 0;

		std::string todeploy; //obstacle to deploy


		bool changeselect = false;

		bool moving = false;
		bool rotating = false;
		bool scaling = false;

		b2Vec2 startmousepos;
		b2Vec2 manipulatorpos;

		GameplayObject* manipulator = nullptr;

		void CreateManipulator(b2Vec2 pos); //create object to rotating, moving or scaling selected objects

		//maybe add new MapObjectType for manipulators?

		b2Vec2 ComputeAvgPosition(); //computing average position of selected objects


		unsigned int deploytimer = SDL_GetTicks();

		void UpdateSelectTool();
		void UpdateDeployTool();
		void UpdateMoveTool();
		void UpdateRotateTool();
		void UpdateScaleTool();

		////////////////////////////////////////////////////////////////////////
		//Gestures
		////////////////////////////////////////////////////////////////////////
		GestureManager gestures;


		////////////////////////////////////////////////////////////////////////
		//Unit management
		////////////////////////////////////////////////////////////////////////

		std::vector<GameplayObject*> unitgroup[10];



		void DestroyGameplayObject(GameplayObject* obj);

	public:
		SpookyGameplay();
		void Update(float time_step);
		~SpookyGameplay();

		void DebugRender();
		void BackgroundRender();
};

#endif // SPOOKY_HPP_INCLUDED
