#ifndef SOUNDWAVES_HPP_INCLUDED
#define SOUNDWAVES_HPP_INCLUDED

#include "../States/GameplayState.hpp"

enum class SoundType
{
	NEUTRAL, //white
	WARNING, //yellow
	DANGER   //red
};

/******************************************
need system to random choose sound with name
in Spooky/sounds.csv is list of sounds,
if are some sounds with same name, should randomly choose one
******************************************/

// sound circle wave

struct Sound
{
	std::string name;
	SoundType type;
	int length; //in ms
	float radius;
};

class SoundWave
{
	public:
		Sound sound;
		b2Vec2 centre;
		int endtime;
		//int initialvolume; //must decrease with distance from center

		SoundWave(Sound s, b2Vec2 pos)
		{
			sound = s;
			centre = pos;
			endtime = SDL_GetTicks() + sound.length;
			Game::audio->PlaySound(sound.name);
		}

		//need a start time? to updating and deleting in adequate time
};

class SoundWaveManager
{

		int vcount; //number of vertex in circle
		b2Vec2* circlesides; //tab with calculating all sides of circle to sound waves
		std::map<std::string, Sound> sounds;

		std::list<SoundWave> soundwaves;

		void RenderCircle(b2Vec2 pos, float radius, SDL_Color* color, float length_unit, Camera* cam);


	public:
		bool render; //if true, then render sound waves

		void InitSound(bool display, int sides = 9);

		void AddSoundWave(const std::string& name, b2Vec2 pos);

		int GetSoundWaveTime(const std::string& name);

		void RenderSoundWaves(float length_unit, Camera* cam);
		//Render all waves from container,
		void SetRenderingWaves(bool value);
		bool GetRenderingWaves();

		void UpdateSoundWaves();

		SoundWaveManager();
		~SoundWaveManager();
};

#endif // SOUNDWAVES_HPP_INCLUDED
