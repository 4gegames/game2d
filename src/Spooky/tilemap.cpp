#include "tilemap.hpp"

TileLayer::TileLayer(int width, int height)
{
	type=LayerType::TILE;
	tiles.reserve(height);
}

TileLayer::~TileLayer()
{
	for(size_t i = 0; i < tiles.size(); i++)
	{
		for(size_t j = 0; j < tiles[i].size(); j++)
		{
			delete tiles[i][j];
		}
	}
	tiles.clear();
}

ObjectLayer::ObjectLayer()
{
	type=LayerType::OBJECT;
}

ObjectLayer::~ObjectLayer()
{
	for(size_t i = 0; i < objects.size(); i++)
	{
		delete objects[i];
	}
	objects.clear();

}

TiledMap::TiledMap()
{
	tilelayer = nullptr;
	collisionlayer = nullptr;
	actionlayer = nullptr;
}

void TiledMap::Init(ResourceManager* resources)
{
	/****************
	must improve margin - it's not worked, when tileset have margin
	must add spacing
	*****************/

	UsedTileset* test= new UsedTileset;
	tilemodel.push_back(test);

	for(size_t i=0; i<tilesets.size(); i++)
	{
		float mapw = tilesets[i]->imgwidth - 2 * tilesets[i]->margin;
		float maph = tilesets[i]->imgheight - 2 * tilesets[i]->margin;

		//int w = tilesets[i]->imgwidth/tilewidth; //must be int - it's number of tiles in horizontal
		int w = mapw /tilewidth; //must be int - it's number of tiles in horizontal
		// need to complete divide
		//int h = tilesets[i]->imgheight/tilewidth;
		int h = maph /tilewidth;

		//int number=(tilesets[i]->imgwidth/tilewidth) * (tilesets[i]->imgheight/tilewidth);
		int number=w * h;


		float horizontal= 1.0f / w;
		float vertical= 1.0f / h;

		float margin = 1.0f / (mapw / (float)tilesets[i]->margin);

		//need add margin and spacing
		/*       std::cout<<"vertical: "<<vertical<<"\n";
		       std::cout<<"horizontal: "<<horizontal<<"\n";
		       std::cout<<"number: "<<number<<"\n";
		       std::cout<<"w: "<<w<<"\n";
		       std::cout<<"h: "<<h<<"\n";
		       std::cout<<"margin: "<<margin<<"\n";
		*/

		for(int j=0; j < number; j++)
		{
			UsedTileset* used = new UsedTileset;
			//WARNING - difference between  imgname and name
			used->texturedata = resources->GetTextureData(tilesets[i]->imgname);

			used->source.left = horizontal * (j) + margin;
			used->source.right = used->source.left + horizontal; //horizontal * (j+1) ;//+ margin;
			used->source.top = vertical * (j / w) - margin;
			used->source.bottom = used->source.top + vertical;// -  margin;
			used->source.w = tilewidth;
			used->source.h = tileheight;

//			std::cout
			/*			<<"left: "<<used->source.left<<", "
						<<"right: "<<used->source.right<<"\n";
						<<"top: "<<used->source.top<<", "
						<<"bottom: "<<used->source.bottom<<"\n";
			*/
			//<<"j: "<<j<<"\n";

			tilemodel.push_back(used);
		}
	}


	totalmapwidth = width * tilewidth;
	totalmapheight = height * tileheight;


	//must automatically calculate this
	walkable.x = 0;
	walkable.y = 0;
	walkable.w = totalmapwidth;
	walkable.h = totalmapheight;

	//Print to log file data of all tiles from all loaded tilesets
	/*for(int i= 1; i<tilemodel.size(); i++)
	{
	    Log("Count:", i, "Name: ",tilemodel[i]->name,", left=",tilemodel[i]->source.left,", right=",tilemodel[i]->source.right
	        ,", top=",tilemodel[i]->source.top,", bottom=",tilemodel[i]->source.bottom,"\n");
	}*/
}

TiledMap::~TiledMap()
{
	for(size_t i = 0; i < tilesets.size(); i++)
	{
		delete tilesets[i];
	}
	tilesets.clear();

	delete tilelayer;
	delete collisionlayer;
}
