#include "Spooky.hpp"

SpookyGameplay::SpookyGameplay()
	:
	GameplayState(0.0f, 0.0f, 64.0f)
{
	currentstate = States::SPOOKYGAMEPLAY;

	CreateCursor("cursor");
	SetGameplayCameraType(Perspective::FRUSTUM);

	if(Game::settings->options.volumeMusic)
		Game::audio->StopMusic(1000);

	musictimer = SDL_GetTicks() + 1200;

	Game::audio->AddMusic("mus_bat_monster2","Data/Music/mus_bat_monster2.ogg");


	LoadStats();
	InitGui();

	//MapInit("map1");
	MapInit("arena");
	AddTestCircle(5,-15);

	testingrender = GetStats("debugrender");


	int sides = GetStats("soundwave_sides");
	bool displaywaves = GetStats("render_soundwave");
	sounds.InitSound(displaywaves, sides);

	LoadArmorTypes();
	LoadUnits();
	LoadObstacles();
	LoadBullets();

	//Loading Obstacles from .csv
	LoadObstaclePositions();

	world->SetContactListener(&listener);

	CreateHero();

	autorotate = true;

	killed = 0;

	InitArenaGameplay();

	selectframe = false;

	gameplayfilter = Collide::PLAYER0_UNIT;

	mainplayer = new Player(0, Collide::PLAYER0_UNIT, Collide::PLAYER0_BULLET);
	enemyplayer = new Player(1, Collide::PLAYER1_UNIT, Collide::PLAYER1_BULLET);

	mainplayer->SetRelation(enemyplayer, FractionRelation::ENEMY);
	enemyplayer->SetRelation(mainplayer, FractionRelation::ENEMY);
}

void SpookyGameplay::CalculateViewport()
{
	int gui_width = 0;
	int gui_height = 0;
	int startx = 0, starty = 0;

	//TO DO:
	///must correct calculate viewport size, when some panels are in this same position (overlap)
	//int leftshift = 0, rightshift = 0, topshift = 0, bottomshift = 0;

	auto it = panels.begin();
	while(it != panels.end())
	{
		if( (*it)->GetDisplay() && (*it)->GetCoverGameplay() )
		{
			Image* img = (*it)->GetImage();

			if( (*it)->GetLayout() == PanelLayout::VERTICAL)
			{
				if(img->GetHorizontalAlignment() == AlignmentX::LEFT)
				{
					gui_width += img->position.w;
					startx += img->position.w;
				}
				else if(img->GetHorizontalAlignment() == AlignmentX::RIGHT)
					gui_width += img->position.w;
			}
			else if( (*it)->GetLayout() == PanelLayout::HORIZONTAL)
			{
				if(img->GetVerticalAlignment() == AlignmentY::UP)
				{
					gui_height += img->position.h;
					starty += img->position.h;
				}
				else if(img->GetVerticalAlignment() == AlignmentY::DOWN)
					gui_height += img->position.h;
			}
		}

		++it;
	}

	cam_gameplay->SetViewport(startx, starty,
							  Game::settings->options.resolution_x - gui_width,
							  Game::settings->options.resolution_y - gui_height);
	viewport.x = startx;
	viewport.y = starty;
	viewport.w = Game::settings->options.resolution_x - gui_width;
	viewport.h = Game::settings->options.resolution_y - gui_height;
}


void SpookyGameplay::InitGui()
{
	statspanel = new Panel("statspanel", PanelLayout::VERTICAL, true, Game::screenrect,
						   AlignmentX::LEFT, AlignmentY::DOWN);
	AddGuiObject(statspanel);

	Button* b = new Button("button4", "", "", 0, 100);
	statspanel->AddToPanel(b, AlignmentX::RIGHT, AlignmentY::DOWN);
	b->click = std::bind(&SpookyGameplay::Return, this);

	statspanel->SetDisplay(false);

	//////////////////////////////////////

	Panel* info = new Panel("ss_infobar", PanelLayout::HORIZONTAL, true, Game::screenrect,
							AlignmentX::LEFT, AlignmentY::UP);
	AddGuiObject(info);

	panelminimap = new Panel("panelminimap", PanelLayout::VERTICAL, false, Game::screenrect,
							 AlignmentX::LEFT, AlignmentY::DOWN);
	AddGuiObject(panelminimap);

	int miniw = GetStats("minimap_width");
	int minih = GetStats("minimap_height");

	minimap = new GuiObject("map1_mini");
	panelminimap->AddToPanel(minimap, AlignmentX::CENTER, AlignmentY::CENTER);
	panelminimap->SetDisplay(false);

	CalculateViewport();

	Image* framehp = new Image("barframe", IsGui::GUI, 50, 50);
	hpframe = new GuiObject(framehp, GuiDepth::TEXT);
	State::AddGuiObject(hpframe);

	Image* hpimg = new Image("hpbar", IsGui::GUI, 55, 55);
	hpbar = new GuiObject(hpimg, GuiDepth::ABOVEBUTTON);
	State::AddGuiObject(hpbar);

	hpbar_length = hpbar->GetImage()->position.w;


	Image* framestamina = new Image("barframe", IsGui::GUI, 50, 100);
	staminaframe = new GuiObject(framestamina, GuiDepth::TEXT);
	State::AddGuiObject(staminaframe);

	Image* staminaimg = new Image("staminabar", IsGui::GUI, 55, 105);
	staminabar = new GuiObject(staminaimg, GuiDepth::ABOVEBUTTON);
	State::AddGuiObject(staminabar);

	staminabar_length = staminabar->GetImage()->position.w;
}

void SpookyGameplay::AddGuiObject(Panel* obj)
{
	State::AddGuiObject( static_cast<GuiObject*>(obj) );
	panels.push_back(obj);

	CalculateViewport();
}


void SpookyGameplay::HeroControl()
{
	b2Vec2 vel = hero->body->GetLinearVelocity();

	float v = GetStats("player_delta_v");
	float maxv = GetStats("player_max_v");

	//must calculate resultant vector from Pythagorean method
	//float a = sqrt(pow(vel.x, 2) + pow(vel.y, 2)); // resultant vector

	if(Game::ctrl->Check("h_left"))
	{
		hero->anim->Play(1, false);
		if(vel.x > -maxv)
			hero->body->ApplyForceToCenter(b2Vec2(-v,0.0f), true);
		//cam_gameplay->MoveLeft( abs(vel.x) );
	}
	else if(Game::ctrl->Check("h_right"))
	{
		hero->anim->Play(1, false);
		if(vel.x < maxv)
			hero->body->ApplyForceToCenter(b2Vec2(v,0.0f), true);
	}

	if(Game::ctrl->Check("h_up"))
	{
		hero->anim->Play(1, false);
		if(vel.y < maxv)
			hero->body->ApplyForceToCenter(b2Vec2(0.0f,v), true);
	}
	else if(Game::ctrl->Check("h_down"))
	{
		hero->anim->Play(1, false);
		if(vel.y > -maxv)
			hero->body->ApplyForceToCenter(b2Vec2(0.0f,-v), true);
	}

	static GameTimer timer;
	if( (vel.x > 0.1 || vel.x < -0.1 || vel.y > 0.1 || vel.y < -0.1)
			&& timer.Passed())
	{
		sounds.AddSoundWave("walk", hero->body->GetPosition());
		timer.AddDelta( sounds.GetSoundWaveTime("walk") );
	}

	if(Game::ctrl->keystate[SDL_SCANCODE_1])
		hero->activespell = SpellType::SMALLFIREBALL;
	else if(Game::ctrl->keystate[SDL_SCANCODE_2])
		hero->activespell = SpellType::BIGFIREBALL;


	static GameTimer spelltimer;

	if(Game::ctrl->Check("shot") && spelltimer.Passed())
	{
		//call UseLeftSlot() function or something like that
		b2Vec2 vmouse = GetMousePhysicPos();
		int casttime;
		if(hero->activespell == SpellType::SMALLFIREBALL && hero->GetStamina() >= 10)
		{
			Bullet* obj = AddBullet("fireball_small", hero->body->GetPosition(), vmouse);//v.x/objdata.boxunit, -v.y/objdata.boxunit);
			casttime = obj->GetCasttime();
			hero->DecreaseStamina(obj->GetCost());
			spelltimer.AddDelta(casttime);
			sounds.AddSoundWave("cast_fireball1", hero->body->GetPosition());
		}
		else if(hero->activespell == SpellType::BIGFIREBALL && hero->GetStamina() >= 20)
		{
			Bullet* obj = AddBullet("fireball_big", hero->body->GetPosition(), vmouse);//v.x/objdata.boxunit, -v.y/objdata.boxunit);
			casttime = obj->GetCasttime();
			hero->DecreaseStamina(obj->GetCost());
			spelltimer.AddDelta(casttime);
			sounds.AddSoundWave("cast_fireball1", hero->body->GetPosition());
		}

	}

	static GameTimer spawntimer;

	if(Game::ctrl->Check("testspawn") && spawntimer.Passed())
	{
		AddUnit("zombie1", hero->GetCenterPhysicPos(), mainplayer);
		spawntimer.AddDelta(300);
	}

}

void SpookyGameplay::CameraControl()
{
	static unsigned int zoomtimer = SDL_GetTicks();
	static unsigned int zoomtimer2 = SDL_GetTicks();

	/**************************************************************************
	if we have change zooming time, must change value in App.cpp,
	Controllers::Update() in line 125: wheel_timer
	**************************************************************************/

	float cam_zoom = GetStats("cam_zoom");

	if((Game::ctrl->Check("zoom_in") || Game::ctrl->Check("wheel_up")) && zoomtimer < SDL_GetTicks() )
	{
		if(camattach)
		{
			if(hero != nullptr)
			{
				glm::vec2 pos = ConvertToScreenPos(b2Vec2(hero->body->GetPosition().x,
												   hero->body->GetPosition().y));
				cam_gameplay->ZoomIn(cam_zoom, pos.x, pos.y);
			}
			else
				cam_gameplay->ZoomIn(cam_zoom);

			cam_centered_zoom = cam_gameplay->GetZoom();
		}
		else
		{
			cam_gameplay->ZoomIn(cam_zoom);

			if(editmode)
				cam_edit_zoom = cam_gameplay->GetZoom();
			else
				cam_tactic_zoom = cam_gameplay->GetZoom();
		}
		zoomtimer = SDL_GetTicks() + 10;
	}

	if((Game::ctrl->Check("zoom_out") || Game::ctrl->Check("wheel_down")) && zoomtimer2 < SDL_GetTicks() )
	{
		cam_gameplay->ZoomOut(cam_zoom);
		zoomtimer2 = SDL_GetTicks() + 10;

		if(camattach)
		{
			cam_centered_zoom = cam_gameplay->GetZoom();
		}
		else
		{
			if(editmode)
				cam_edit_zoom = cam_gameplay->GetZoom();
			else
				cam_tactic_zoom = cam_gameplay->GetZoom();
		}
	}


	static bool switchtype = false; // 0 - tab, 1 - space

	static unsigned int timer = SDL_GetTicks();

	if(!editmode)
	{
		if(Game::ctrl->Check("camattach_switch") && timer < SDL_GetTicks() )
		{
			switchtype = false;
			camattach = !camattach;

			if( !camattach) //tactic
			{
				CameraDetach();
			}
			else //hero center
			{
				CameraAttach();
			}

			timer = SDL_GetTicks() + 500;
		}
		else if(Game::ctrl->Check("camattach_pressed"))
		{
			switchtype = true;

			if(camattach) //first iterate with this mode
			{
				CameraDetach();
			}
		}
		else if(switchtype)
		{
			if( !camattach) //first iterate with this mode
			{
				cam_gameplay->SetZoom(cam_centered_zoom);
			}

			camattach = true;

			cam_gameplay->SetZoomScope( -GetStats("cam_max_zoom"),
										-GetStats("cam_min_zoom"));
			switchtype = false;
		}
	}

	if(camattach && hero != nullptr)
	{
		int cx = hero->GetCenterPos().x;
		int cy = hero->GetCenterPos().y;

		cam_gameplay->Center(cx, cy);
	}
	else
	{
		float cam_move = GetStats("cam_move");

		if(Game::ctrl->Check("cam_up")
				|| Game::ctrl->mouse_y < Game::settings->options.resolution_y / 100)
		{
			cam_gameplay->MoveVertical(-cam_move* (-cam_gameplay->GetTranslation().z));
		}
		else if(Game::ctrl->Check("cam_down")
				|| Game::ctrl->mouse_y > Game::settings->options.resolution_y * 0.99)
		{
			cam_gameplay->MoveVertical(cam_move * (-cam_gameplay->GetTranslation().z));
		}

		if(Game::ctrl->Check("cam_left")
				|| Game::ctrl->mouse_x < Game::settings->options.resolution_x / 100)
		{
			cam_gameplay->MoveHorizontal(-cam_move * (-cam_gameplay->GetTranslation().z));
		}
		else if(Game::ctrl->Check("cam_right")
				|| Game::ctrl->mouse_x > Game::settings->options.resolution_x * 0.99)
		{
			cam_gameplay->MoveHorizontal(cam_move * (-cam_gameplay->GetTranslation().z));
		}
	}

	//Panel hide/unhide
	/*if(Game::ctrl->Check("x") && timer < SDL_GetTicks())
	{
		bool value = !(panel->GetDisplay());
		panel->SetDisplay(value);
		//gui.PanelEnable("infobar", value);
		timer = SDL_GetTicks() + 200;
	}*/

	/*
	if(Game::ctrl->Check("lctrl", "z") && timer < SDL_GetTicks() )
	{
		if(Game::ctrl->Check("left"))
		{
			panelminimap->Align(Game::screenrect, AlignmentX::LEFT, AlignmentY::DOWN);
			CalculateViewport();
			timer = SDL_GetTicks() + 200;
		}
		else if(Game::ctrl->Check("right"))
		{
			panelminimap->Align(Game::screenrect, AlignmentX::RIGHT, AlignmentY::DOWN);
			CalculateViewport();
			timer = SDL_GetTicks() + 200;
		}
		else if(Game::ctrl->Check("up"))
		{
			panelminimap->Align(Game::screenrect, AlignmentX::RIGHT, AlignmentY::UP);
			CalculateViewport();
			timer = SDL_GetTicks() + 200;
		}
	}
	*/
}

void SpookyGameplay::CameraAttach()
{
	cam_gameplay->SetZoomScope( -GetStats("cam_max_zoom"),
								-GetStats("cam_min_zoom"));
	cam_gameplay->SetZoom(cam_centered_zoom);

	camattach = true;
}

void SpookyGameplay::CameraDetach()
{
	cam_gameplay->SetZoomScope( -GetStats("cam_tactic_max_zoom"),
								-GetStats("cam_tactic_min_zoom"));
	cam_gameplay->SetZoom(cam_tactic_zoom);

	//cam_gameplay->Center(hero->GetCenterPos().x, hero->GetCenterPos().y);

	camattach = false;
}

void SpookyGameplay::CameraDetachEdit()
{
	cam_gameplay->SetZoomScope( -GetStats("cam_edit_max_zoom"),
								-GetStats("cam_edit_min_zoom"));
	cam_gameplay->SetZoom(cam_edit_zoom);

	camattach = false;
}

void SpookyGameplay::Update(float time_step)
{
	//GameplayState::Update(time_step); //physics simulation and others
	///modified GameplayState::Update() - added type Unit handling
	State::Update(time_step);

	if(editmode)
	{
		world->Step(0, Game::settings->options.velocityIterations,
					Game::settings->options.positionIterations);
		Game::fps->UpdateTime(0);
	}
	else
	{
		world->Step(time_step * timemultiplier, Game::settings->options.velocityIterations,
					Game::settings->options.positionIterations);
		Game::fps->UpdateTime(time_step * timemultiplier * 1000);
	}

	for(auto it = gameplayobjects.begin(); it != gameplayobjects.end(); ++it)
	{
		(*it)->Update();

		if( (*it)->todestroy)
		{
			DestroyGameplayObject(*it);

			delete (*it);
			it = gameplayobjects.erase(it);
		}
	}


	//debug
	static unsigned int timer = SDL_GetTicks();
	if(Game::ctrl->keystate[SDL_SCANCODE_F10] && timer < SDL_GetTicks())
	{
		testingrender = !testingrender;
		timer = SDL_GetTicks() + 200;
	}

	///////////////////////////////////////////////////

	if(Game::ctrl->Check("exit"))
	{
		action = ChangeStates::PUSH;
		nextstate = States::PAUSEMENU;
	}

	if(Game::ctrl->Check("changemap"))
	{
		if(mapname != "map2")
			ChangeMap("map2");
	}
	if(Game::ctrl->Check("changemap2"))
	{
		if(mapname != "map1")
			ChangeMap("map1");
	}
	if(Game::ctrl->Check("changemap3"))
	{
		if(mapname != "mapbig1")
			ChangeMap("mapbig1");
	}

	if(hero != nullptr && hero->GetHealth() <= 0)
	{
		sounds.AddSoundWave("death", hero->GetCenterPhysicPos());

		Log("Killed ", killed, " enemies\n");

		action = ChangeStates::PUSH;
		nextstate = States::DEFEATMENU;
	}

	CameraControl();

	if(Game::ctrl->Check("spawnbox"))
	{
		glm::vec2 v = GetMouseGlPos();
		GameplayObject* obj = AddTestBody(v.x/objdata.boxunit, -v.y/objdata.boxunit);
		obj->SetAnimation(obj->GetImage(), 500, 50, 50, 500, 50);
		obj->anim->Play(0, true);
	}

	if(Game::ctrl->Check("z"))
	{
		AddUnit("zombie2",b2Vec2(5,-5), enemyplayer);
		alive++;
		//spawn->SpawnUnit("Unit");
	}

	//minimap
	/*
		glm::vec2 pos = ConvertToGlPos(hero->body->GetPosition());
		int x = pos.x / 64 - 150;
		int y = pos.y / 64 - 150;

		if( x < tmap->walkable.x)
			x = tmap->walkable.x;
		else if( x > tmap->walkable.x + tmap->walkable.w)
			x = tmap->walkable.x + tmap->walkable.w;
		if( y < tmap->walkable.y)
			y = tmap->walkable.y;
		else if( y > tmap->walkable.y + tmap->walkable.h)
			y = tmap->walkable.y + tmap->walkable.h;

		minimap->GetImage()->ChangeSource(x, y);
	*/

	if(!gestures.mode && autorotate && hero != nullptr)
		hero->RotateToPoint(GetMousePhysicPos());

	static GameTimer timersounds;

	if(timersounds.Passed())
	{
		sounds.UpdateSoundWaves();
		timersounds.AddDelta(300);
	}

	///////////////////////////////////////////////////////////////////////////

	static unsigned int editmodetimer = SDL_GetTicks();
	if(Game::ctrl->keystate[SDL_SCANCODE_F5] && editmodetimer<SDL_GetTicks())
	{
		editmode = !editmode;
		editmodetimer = SDL_GetTicks() + 200;
	}

	if(Game::ctrl->keystate[SDL_SCANCODE_F6] && editmodetimer < SDL_GetTicks())
	{
		SaveObstacles();
		editmodetimer = SDL_GetTicks() + 200;
	}
	///////////////////////////////////////////////////////////////////////////

	if(editmode)
	{
		if(enteredit)
		{
			OpenEditMode();
		}

		UpdateEditMode();
	}
	else
	{
		if( !enteredit)
		{
			CloseEditMode();
		}

		if(mapname == "arena")
		{
			UpdateArenaGameplay();
		}

		if(hero != nullptr)
		{

			HeroControl();

			///hp bar
			float hp = hero->GetHealth();
			if(lasthp != hp)
			{
				lasthp = hp;
				hpbar->GetImage()->position.w = (hp / (float)hero->GetMaxHealth()) * hpbar_length;
			}

			///stamina bar
			float stamina = hero->GetStamina();
			if(laststamina != stamina)
			{
				laststamina = stamina;
				staminabar->GetImage()->position.w =
					(stamina / (float)hero->GetMaxStamina()) * staminabar_length;
			}
		}

		///testing time manipulation
		if(Game::ctrl->Check("time_slow"))
		{
			timemultiplier = 0.5f;
		}
		else if(Game::ctrl->Check("time_normal"))
		{
			timemultiplier = 1.0f;
		}
		else if(Game::ctrl->Check("time_fast"))
		{
			timemultiplier = 2.0f;
		}

		static unsigned int rotatetimer = SDL_GetTicks();
		if(Game::ctrl->Check("autorotate") && rotatetimer < SDL_GetTicks())
		{
			autorotate = !autorotate;
			rotatetimer = SDL_GetTicks() + 200;
		}

		//gestures
		static int vertexdistance = GetStats("gesture_vertexdistance");

		int action_id = gestures.Update(camattach, vertexdistance);

		if(action_id != -1 && hero != nullptr)
		{
			if(hero->GetStamina() >= 10)
			{
				hero->DecreaseStamina(10);
				b2Vec2 startpoint = ConvertScreenToPhysicPos(gestures.startpos);
				AddBullet("fireball_small", hero->body->GetPosition(), startpoint);//v.x/objdata.boxunit, -v.y/objdata.boxunit);

				sounds.AddSoundWave("cast_fireball1", hero->body->GetPosition());
			}
			//must call action
		}


		///unit management

		//selecting units
		UpdateSelectTool();

		if( !selectedobjs.empty())
		{
			for(GameplayObject* obj : selectedobjs)
			{
				obj->ComputeBorderPoints();
			}
		}


		//creating unit group
		if( !selectedobjs.empty() && (Game::ctrl->Check("lctrl") || Game::ctrl->Check("rctrl")) )
		{
			std::string keyname = "unitgroup";
			for(int i = 0; i < 10; i++)
			{
				if( Game::ctrl->Check(keyname + std::to_string(i)) )
				{
					unitgroup[i].assign(selectedobjs.begin(), selectedobjs.end());
					break;
				}
			}
		}


		std::string keyname = "unitgroup";
		for(int i = 0; i < 10; i++)
		{
			if( Game::ctrl->Check(keyname + std::to_string(i)) )
			{
				selectedobjs.assign(unitgroup[i].begin(), unitgroup[i].end());
				break;
			}
		}

	}

	if(musictimer < SDL_GetTicks() && !startmusic)
	{
		startmusic = !startmusic;
		Game::audio->PlayMusic("muzyka1");
	}

	/*
	//testing
	if(SDL_HasClipboardText())
	{
		std::cout<<"from clipboard: "<<SDL_GetClipboardText()<<"\n";
	}
	*/
}

void SpookyGameplay::DestroyGameplayObject(GameplayObject* obj)
{
	if( obj->type == MapObjectType::UNIT)
	{
		killed++;
		alive--;
		Unit* u = static_cast<Unit*>(obj);
		int value = u->statistics.maxhealth / 8;

		if(hero != nullptr)
			hero->IncreaseStamina(value);

		//added corpse
		AddObstacle(u->statistics.corpse, u->GetCenterPhysicPos(), u->body->GetAngle());

		//deleting from select groups
		for(int i = 0; i < 10; i++)
		{
			if( !unitgroup[i].empty())
			{
				auto objvec = unitgroup[i];
				objvec.erase(std::find(objvec.begin(), objvec.end(), obj));
			}
		}
	}
	else if( obj->type == MapObjectType::DESTRUCTIBLE)
	{

	}
	else
	{
		if(hero == obj)
			hero = nullptr;
	}

	if( obj->body != nullptr)
		world->DestroyBody( obj->body);
}


void SpookyGameplay::OpenEditMode()
{
	if(firsteditrun)
		InitEditMode();

	staminaframe->SetDisplay(false);
	staminabar->SetDisplay(false);
	hpframe->SetDisplay(false);
	hpbar->SetDisplay(false);

	edittoolbar->SetDisplay(true);
	CalculateViewport();

	enteredit = false;

	CameraDetachEdit();

	editorfilter = 0xFFFFFFFF;
}

void SpookyGameplay::InitEditMode()
{
	//only once in session with editor mode

	tools[EditModeTool::SELECT] = false;
	tools[EditModeTool::DEPLOY] = false;
	tools[EditModeTool::MOVE] = false;
	tools[EditModeTool::ROTATE] = false;
	tools[EditModeTool::SCALE] = false;

	edittoolbar = new Panel("editmode_toolbar", PanelLayout::HORIZONTAL, true, viewport,
							AlignmentX::LEFT, AlignmentY::UP);
	AddGuiObject(edittoolbar);

	int w = 50;

	Button* toolselect = new Button("editmode_select",
									&tools.find(EditModeTool::SELECT)->second, "edit_select");
	edittoolbar->AddToPanel(toolselect, AlignmentX::LEFT, AlignmentY::CENTER);

	Button* tooldeploy = new Button("editmode_deploy",
									&tools.find(EditModeTool::DEPLOY)->second, "edit_deploy");
	edittoolbar->AddToPanel(tooldeploy, AlignmentX::LEFT, AlignmentY::CENTER);
	tooldeploy->Translate(w, 0);

	Button* toolmove = new Button("editmode_move",
								  &tools.find(EditModeTool::MOVE)->second, "edit_move");
	edittoolbar->AddToPanel(toolmove, AlignmentX::LEFT, AlignmentY::CENTER);
	toolmove->Translate(2*w, 0);

	Button* toolrotate = new Button("editmode_rotate",
									&tools.find(EditModeTool::ROTATE)->second, "edit_rotate");
	edittoolbar->AddToPanel(toolrotate, AlignmentX::LEFT, AlignmentY::CENTER);
	toolrotate->Translate(3*w, 0);

	Button* toolscale = new Button("editmode_scale",
								   &tools.find(EditModeTool::SCALE)->second, "edit_scale");
	edittoolbar->AddToPanel(toolscale, AlignmentX::LEFT, AlignmentY::CENTER);
	toolscale->Translate(4*w, 0);

	std::cout<<"init\n";
	enteredit = false;

	firsteditrun = false;

	todeploy = (*obstacles.begin()).first;
}

void SpookyGameplay::UpdateEditMode()
{
	auto itertool = tools.begin();
	while(itertool != tools.end())
	{
		if( (*itertool).second && (*itertool).first != activetool)
		{
			SetActiveTool( (*itertool).first);
			break;
		}

		++itertool;
	}


	//updating tools
	switch(activetool)
	{
		case EditModeTool::DEPLOY:
		{
			UpdateDeployTool();
		}
		break;
		case EditModeTool::ROTATE:
		{
			UpdateRotateTool();
		}
		break;
		case EditModeTool::SCALE:
		{
			UpdateScaleTool();
		}
		break;
		case EditModeTool::SELECT:
		{
			UpdateSelectTool();
		}
		break;
		case EditModeTool::MOVE:
		{
			UpdateMoveTool();
		}
		break;
		case EditModeTool::NONE:
		default:
			selectframe = false;//no select tool
	}


	if(Game::ctrl->keystate[SDL_SCANCODE_DELETE])
	{
		auto it = selectedobjs.begin();
		while(it != selectedobjs.end())
		{
			(*it)->Destroy();
			++it;
		}

		selectedobjs.clear();
	}


	if(activetool != EditModeTool::ROTATE && activetool != EditModeTool::SCALE)
	{
		if(manipulator)
		{
			manipulator->Destroy();
			manipulator = nullptr;
		}
	}
}

void SpookyGameplay::UpdateSelectTool()
{
	static glm::vec2 clickpos(0,0);
	static glm::vec2 mousepos;

	//render rectangle for represent selecting
	if(Game::ctrl->Check("edit_selectframe") && Game::ctrl->TestClickInArea(viewport))
	{
		if(!selectframe)
		{
			clickpos = ConvertScreenToGlPos( glm::vec2(Game::ctrl->click_x, Game::ctrl->click_y));
			selectframe = true;
		}

		mousepos = GetMouseGlPos();

		selectvertices.clear();
		selectvertices.push_back(clickpos.x);
		selectvertices.push_back(clickpos.y);
		selectvertices.push_back(mousepos.x);
		selectvertices.push_back(clickpos.y);

		selectvertices.push_back(mousepos.x);
		selectvertices.push_back(mousepos.y);
		selectvertices.push_back(clickpos.x);
		selectvertices.push_back(mousepos.y);
	}
	else
	{
		if(selectframe) // close
		{
			bool addtocontainer = true;

			int filter;

			if(editmode)
				filter = editorfilter;
			else
				filter = gameplayfilter;

			if(filter)
			{
				//erasing instead adding to select
				if(Game::ctrl->Check("lalt") || Game::ctrl->Check("ralt"))
				{
					addtocontainer = false;
				}
				//if ctrl is pressed, objects are adding to selected before
				else if( !Game::ctrl->Check("lctrl") && !Game::ctrl->Check("rctrl"))
				{
					selectedobjs.clear();
				}

				bool clickselect = false;

				if(VertexDistance(clickpos, mousepos) < 32)
					clickselect = true;

				for(auto it = gameplayobjects.begin(); it != gameplayobjects.end(); ++it)
				{
					b2Fixture* fixture = (*it)->body->GetFixtureList();

					if(fixture->GetFilterData().categoryBits & filter && (*it) != hero)
					{
						//crash with another maps
						//glm::vec2 pos = ConvertToGlPos( (*it)->body->GetPosition() );
						glm::vec2 pos = ConvertToGlPos( (*it)->GetCenterPhysicPos() );

						bool selecting = false;

						if(pos.x > clickpos.x)
						{
							if(pos.x < mousepos.x)
							{
								if(pos.y > clickpos.y)
								{
									if(pos.y < mousepos.y)
									{
										selecting = true;
									}
								}
								else if(pos.y > mousepos.y)
								{
									selecting = true;
								}
							}
						}
						else if(pos.x > mousepos.x)
						{
							if(pos.y > clickpos.y)
							{
								if(pos.y < mousepos.y)
								{
									selecting = true;
								}
							}
							else if(pos.y > mousepos.y)
							{
								selecting = true;
							}
						}

						//for only click (no drawing frame) must check click point
						if( !selecting && clickselect)
						{
							b2Vec2 physicmousepos = GetMousePhysicPos();
							for (b2Fixture* selectfixture = (*it)->body->GetFixtureList();
									selectfixture; selectfixture = selectfixture->GetNext())
							{
								if(selectfixture->TestPoint(physicmousepos))
								{
									selecting = true;
									break;
								}
							}
						}

						if(selecting)
						{
							auto objiter = std::find(selectedobjs.begin(), selectedobjs.end(), (*it));

							if(objiter == selectedobjs.end()) //object in this container not found, can add object to container
							{
								if(addtocontainer)
									selectedobjs.push_back(*it);
							}
							else if( !addtocontainer) //alt pressed, if object is in container must erase him
							{
								selectedobjs.erase(objiter);
							}
						}
					}
				}
			}

			selectframe = false;

			changeselect = true;
		}

		selectframe = false;
	}

	///select should be a default tool
	/*if(editmode && !tools.find(activetool)->second)
	{
		SetActiveTool(EditModeTool::NONE);
	}*/

}

void SpookyGameplay::UpdateDeployTool()
{
	if(toolinit)
	{
		angle = 0.0f;

		ObstacleData& obstacle = obstacles.find(todeploy)->second;

		attachcursor = new GameplayObject(objdata);
		attachcursor->SetImage(new Image(obstacle.graphicname, IsGui::NONE, 0,0, 0,0,
										 obstacle.framewidth, obstacle.frameheight));

		toolinit = false;
	}


	attachcursor->GetImage()->position.x = GetMouseGlPos().x - attachcursor->GetImage()->position.w/2;
	attachcursor->GetImage()->position.y = GetMouseGlPos().y - attachcursor->GetImage()->position.h/2;

	if(Game::ctrl->keystate[SDL_SCANCODE_S] && deploytimer < SDL_GetTicks())
	{
		angle = 0.0f;

		auto it = obstacles.find(todeploy);

		it++;

		if(it == obstacles.end())
			it = obstacles.begin();

		//name of obstacle to deploy
		todeploy = (*it).first;

		ObstacleData& obstacle = (*it).second;

		attachcursor->Destroy();

		attachcursor = new GameplayObject(objdata);
		attachcursor->SetImage(new Image(obstacle.graphicname, IsGui::NONE, 0,0, 0,0,
										 obstacle.framewidth, obstacle.frameheight));

		deploytimer = SDL_GetTicks() + 200;
	}

	if(Game::ctrl->keystate[SDL_SCANCODE_A] && deploytimer < SDL_GetTicks())
	{
		angle += 0.05f;
		if(angle > 2 * M_PI)
			angle = 0.0f;

		std::cout<<"angle: "<<angle<<"\n";

		attachcursor->GetImage()->SetAngle(-angle * RADTODEG);

		deploytimer = SDL_GetTicks() + 20;
	}

	if(Game::ctrl->keystate[SDL_SCANCODE_D] && deploytimer < SDL_GetTicks())
	{
		angle -= 0.05f;
		std::cout<<"angle: "<<angle<<"\n";

		if(angle < -2 * M_PI)
			angle = 0.0f;

		attachcursor->GetImage()->SetAngle(-angle * RADTODEG);

		deploytimer = SDL_GetTicks() + 20;
	}

	if(Game::ctrl->lmb_state == MouseState::BUTTONUP && Game::ctrl->TestClickInArea(viewport)
			&& deploytimer < SDL_GetTicks())
	{
		AddObstacle(todeploy, GetMousePhysicPos(), angle);

		deploytimer = SDL_GetTicks() + 200;
	}

	//PPM//////////////////////////////////////////////////////////////
	if(Game::ctrl->mousekey[2])
	{
		tools.find(EditModeTool::DEPLOY)->second = false;
	}


	if( !tools.find(activetool)->second)
	{
		SetActiveTool(EditModeTool::SELECT);
	}
}

void SpookyGameplay::UpdateMoveTool()
{
	static bool click = false;
	if( !click && Game::ctrl->mousekey[0] && Game::ctrl->TestClickInArea(viewport) && !moving)
	{
		click = true;
		startmousepos = GetMousePhysicPos();

		//if alt is pressed, not moving but need delete selected objects
		if( !Game::ctrl->Check("lalt") && !Game::ctrl->Check("ralt"))
		{
			if( !selectedobjs.empty())
			{
				auto it = selectedobjs.begin();
				while(it != selectedobjs.end())
				{
					for (b2Fixture* fixture = (*it)->body->GetFixtureList(); fixture; fixture = fixture->GetNext())
					{
						if(fixture->TestPoint(startmousepos))
						{
							moving = true;
							break;
						}
					}

					if(moving)
						break;

					++it;
				}
			}

			//not selected any object OR last test not pass (click not selected object)
			if( !moving)
			{
				auto it = gameplayobjects.begin();
				while(it != gameplayobjects.end())
				{
					for (b2Fixture* fixture = (*it)->body->GetFixtureList(); fixture; fixture = fixture->GetNext())
					{
						if(fixture->TestPoint(startmousepos))
						{
							if( !Game::ctrl->Check("lctrl") && !Game::ctrl->Check("rctrl"))
							{
								selectedobjs.clear();
							}

							selectedobjs.push_back( (*it));

							moving = true;
							break;
						}
					}

					if(moving)
						break;

					++it;
				}
			}
		}
	}

	if(moving)
	{
		b2Vec2 mousepos = GetMousePhysicPos();
		b2Vec2 delta = mousepos - startmousepos;

		auto it = selectedobjs.begin();
		while(it != selectedobjs.end())
		{
			b2Body* obj = (*it)->body;
			b2Vec2 newpos = obj->GetPosition() + delta;
			obj->SetTransform(newpos, obj->GetAngle());

			(*it)->ComputeBorderPoints();

			++it;
		}

		startmousepos = mousepos;
	}
	else
	{
		UpdateSelectTool();
	}

	if(Game::ctrl->lmb_state == MouseState::BUTTONUP)
	{
		click = false;

		if(moving)
		{
			moving = false;
		}
	}


	if( !tools.find(activetool)->second)
	{
		SetActiveTool(EditModeTool::NONE);
	}
}

void SpookyGameplay::UpdateRotateTool()
{
	if(toolinit)
	{
		changeselect = true;

		toolinit = false;
	}

	static bool click = false;

	if( !click && Game::ctrl->mousekey[0] && Game::ctrl->TestClickInArea(viewport) && !rotating)
	{
		click = true;
		startmousepos = GetMousePhysicPos();

		if(manipulator != nullptr && manipulator->body->GetFixtureList()->TestPoint(startmousepos) )
		{
			rotating = true;
			changeselect = true;
		}
	}

	if( !selectedobjs.empty() && changeselect)
	{
		changeselect = false;

		angle = 0.0f;

		manipulatorpos = ComputeAvgPosition();
		CreateManipulator(manipulatorpos);

		for(auto it = selectedobjs.begin(); it != selectedobjs.end(); ++it)
		{
			b2Vec2 v = (*it)->body->GetPosition() - manipulatorpos;

			(*it)->startangle = atan2f(v.y, v.x);
			(*it)->localangle = (*it)->body->GetAngle();
		}
	}

	if(selectedobjs.empty())
	{
		if(manipulator != nullptr)
		{
			manipulator->Destroy();
			manipulator = nullptr;
		}
	}


	if(rotating)
	{
		b2Vec2 mousepos = GetMousePhysicPos();
		b2Vec2 delta = mousepos - startmousepos;

		angle = delta.y / 2.0f;//0.01f;
		if(angle > 2 * M_PI && angle < -2 * M_PI)
			angle = 0.0f;

		auto it = selectedobjs.begin();
		while(it != selectedobjs.end())
		{
			float objangle = (*it)->startangle + angle;
			b2Body* obj = (*it)->body;
			b2Vec2 v;
			float distance = VertexDistance(ConvertToGlPos(manipulatorpos), ConvertToGlPos(obj->GetPosition()));
			v.x = manipulatorpos.x  + (distance / objdata.boxunit) * cos(objangle);
			v.y = manipulatorpos.y  + (distance / objdata.boxunit) * sin(objangle);

			obj->SetTransform(v, (*it)->localangle + angle);

			(*it)->ComputeBorderPoints();

			++it;
		}
	}
	else
	{
		UpdateSelectTool();
	}

	if(Game::ctrl->lmb_state == MouseState::BUTTONUP)
	{
		click = false;

		if(rotating)
		{
			rotating = false;
		}
	}

	if( !tools.find(activetool)->second)
	{
		SetActiveTool(EditModeTool::NONE);
	}
}

void SpookyGameplay::UpdateScaleTool()
{
	if(toolinit)
	{
		changeselect = true;

		toolinit = false;
	}

	static float scalevalue = 0.0f;

	static bool click = false;

	if( !click && Game::ctrl->mousekey[0] && Game::ctrl->TestClickInArea(viewport) && !scaling)
	{
		click = true;
		startmousepos = GetMousePhysicPos();

		if(manipulator != nullptr && manipulator->body->GetFixtureList()->TestPoint(startmousepos) )
		{
			scaling = true;
			changeselect = true;
		}
	}

	if( !selectedobjs.empty() && changeselect)
	{
		changeselect = false;

		scalevalue = 0.0f;

		manipulatorpos = ComputeAvgPosition();
		CreateManipulator(manipulatorpos);


		for(auto it = selectedobjs.begin(); it != selectedobjs.end(); ++it)
		{
			//b2Vec2 v = (*it)->body->GetPosition() - manipulatorpos;

			(*it)->initscaleratio = (*it)->scaleratio;
		}
	}

	if(selectedobjs.empty())
	{
		if(manipulator != nullptr)
		{
			manipulator->Destroy();
			manipulator = nullptr;
		}
	}


	if(scaling)
	{
		b2Vec2 mousepos = GetMousePhysicPos();
		b2Vec2 delta = mousepos - startmousepos;

		//float distance = VertexDistance(GetMouseGlPos(), ConvertToGlPos(startmousepos));

		auto it = selectedobjs.begin();
		while(it != selectedobjs.end())
		{
			scalevalue = std::max( (*it)->initscaleratio.x + delta.y/10.0f, 0.1f);

			(*it)->Scale(scalevalue, scalevalue);

			++it;
		}
	}
	else
	{
		UpdateSelectTool();
	}

	if(Game::ctrl->lmb_state == MouseState::BUTTONUP)
	{
		click = false;

		if(scaling)
		{
			scaling = false;
		}
	}




	if( !tools.find(activetool)->second)
	{
		SetActiveTool(EditModeTool::NONE);
	}
}

void SpookyGameplay::SetActiveTool(EditModeTool tool)
{
	lasttool = activetool;
	activetool = tool;
	CloseLastTool();

	toolinit = true;

	for( auto it = tools.begin(); it != tools.end(); ++it)
		(*it).second = false;
	tools.find(tool)->second = true;
}

void SpookyGameplay::CloseLastTool()
{
	if(lasttool == EditModeTool::DEPLOY)
	{
		attachcursor->Destroy();
		attachcursor = nullptr;
	}

}

void SpookyGameplay::CloseEditMode()
{
	lasttool = activetool;
	CloseLastTool();
	enteredit = true;

	if(manipulator)
		manipulator->Destroy();

	selectedobjs.clear();

	staminaframe->SetDisplay(true);
	staminabar->SetDisplay(true);
	hpframe->SetDisplay(true);
	hpbar->SetDisplay(true);

	edittoolbar->SetDisplay(false);
	CalculateViewport();

	CameraAttach();

	activetool = EditModeTool::NONE;
	lasttool = EditModeTool::NONE;

	angle = 0.0f;
}

void SpookyGameplay::CreateManipulator(b2Vec2 pos)
{
	if(manipulator != nullptr)
		manipulator->Destroy();

	Image* img = new Image("objpivot", IsGui::NONE);
	manipulator = new GameplayObject(objdata, img);
	manipulator->type = MapObjectType::SENSOR;
	manipulator->CreateBody(BodyType::DYNAMIC, pos.x, pos.y);

	b2CircleShape shape;
	shape.m_radius = 3.0f;
	b2FixtureDef fixture;
	fixture.shape = &shape;
	fixture.isSensor = true;
	manipulator->AddFixture(fixture);
}

b2Vec2 SpookyGameplay::ComputeAvgPosition()
{
	b2Vec2 sumpos = b2Vec2(0,0);

	for(auto it = selectedobjs.begin(); it != selectedobjs.end(); ++it)
	{
		sumpos = sumpos + (*it)->GetCenterPhysicPos();
	}

	sumpos.x = sumpos.x / selectedobjs.size();
	sumpos.y = sumpos.y / selectedobjs.size();

	return sumpos;
}

///////////////////////////////////////////////////////////////////////////////

void SpookyGameplay::MapInit(const std::string& name)
{
	tmap = ScriptEngine::GetSingleton().LoadMap(name);
	mapname = name;

	for(size_t i=0; i< tmap->tilesets.size(); i++)
	{
		tmap->tilesets[i]->imgname= Game::resources->GetNameFromPath(tmap->tilesets[i]->imgname);
	}

	tmap->Init(Game::resources);

	//init physic world
	InitPhysicWorld(gravity->x, gravity->y, tmap->tilewidth);


	world->SetContactListener(&listener);

	if(tmap->collisionlayer != nullptr)
	{
		for(size_t i=0; i < tmap->collisionlayer->objects.size(); i++)
		{
			AddMapObject(tmap->collisionlayer->objects[i]);
		}
	}

	if(tmap->actionlayer != nullptr)
	{
		for(size_t i=0; i < tmap->actionlayer->objects.size(); i++)
		{
			AddMapObject(tmap->actionlayer->objects[i]);
		}
	}

	float r = (float)tmap->tilewidth / objdata.boxunit;

	AddStaticRect(0,0, r, tmap->height * r);
	AddStaticRect(0,0, tmap->width * r, r);

	AddStaticRect((tmap->width - 1) * r, 0, r, tmap->height * r);
	AddStaticRect(0, -(tmap->height - 1) * r, tmap->width * r, r); //this -1 is necessary?


	cam_gameplay->Center(Game::settings->options.resolution_x/2,
						 Game::settings->options.resolution_y/2);

	cam_gameplay->SetZoomScope( -GetStats("cam_max_zoom"),
								-GetStats("cam_min_zoom"));

	cam_gameplay->SetArea(tmap->walkable.x, tmap->walkable.x + tmap->walkable.w,
						  tmap->walkable.y, tmap->walkable.y + tmap->walkable.h);

	camattach = true;

	cam_centered_zoom = -GetStats("cam_min_zoom");
	cam_tactic_zoom = -GetStats("cam_tactic_min_zoom");
	cam_edit_zoom = (-GetStats("cam_edit_min_zoom") - GetStats("cam_edit_max_zoom")) /2;
	std::cout<<"zoom: "<<cam_edit_zoom<<"\n";

	cam_gameplay->SetMinZoom();

	//MINIMAP
	/*
	std::string mininame = mapname + "_mini";
	RegisterTexture(mininame);
	minimap->ChangeGraphic(mininame);
	*/

	//set default cam zooming, maybe need SetDefaultZoom() function?

	//if changing map in edit mode, this close edit mode and reopen with new map
	if(editmode)
		CloseEditMode();

}

void SpookyGameplay::LoadUnits()
{
	std::fstream units_file;

	std::string tempstr;

	units_file.open("Data/Spooky/units.csv", std::ios::in);
	if(units_file.good())
	{
		getline(units_file, tempstr); //first row have only heads of columns
		while(units_file.good())
		{
			getline(units_file,tempstr,';');

			if(tempstr == "\n" || tempstr.empty())
			{
				Log("empty line in units.csv\n");
				break;
			}

			UnitData tempdata;
			units[tempstr] = tempdata;
			UnitData& data = units.find(tempstr)->second;

			data.name = tempstr;

			getline(units_file,tempstr,';');
			data.graphicname = tempstr;

			getline(units_file,tempstr,';');
			data.behaviour = tempstr;

			getline(units_file,tempstr,';');
			data.maxhealth = std::stof(tempstr);
			data.currenthealth = data.maxhealth;

			getline(units_file,tempstr,';');
			data.hpregen = std::stof(tempstr);

			getline(units_file,tempstr,';');
			float tempdamage = std::stof(tempstr);

			getline(units_file,tempstr,';');

			if(tempstr == "normal")
				data.damage.normal = tempdamage;
			else if (tempstr == "pierce")
				data.damage.pierce = tempdamage;
			else if (tempstr == "siege")
				data.damage.siege = tempdamage;
			else if (tempstr == "barefist")
				data.damage.barefist = tempdamage;
			else if (tempstr == "bite")
				data.damage.bite = tempdamage;
			else if (tempstr == "chaos")
				data.damage.chaos = tempdamage;
			else
				Log("Unit: ",data.name,", no such DamageType: ",tempstr);

			getline(units_file,tempstr,';');
			data.range = std::stof(tempstr);

			getline(units_file,tempstr,';');
			data.armor = std::stof(tempstr);

			getline(units_file,tempstr,';');

			/*
						if(tempstr == "unarmored")
							data.armortype = ArmorType::UNARMORED;
						else if (tempstr == "light")
							data.armortype = ArmorType::LIGHT;
						else if (tempstr == "medium")
							data.armortype = ArmorType::MEDIUM;
						else if (tempstr == "heavy")
							data.armortype = ArmorType::HEAVY;
						else if (tempstr == "eteric")
							data.armortype = ArmorType::ETERIC;
						else if (tempstr == "fortified")
							data.armortype = ArmorType::FORTIFIED;
						else if (tempstr == "divine")
							data.armortype = ArmorType::DIVINE;
						else if (tempstr == "invulnerable")
							data.armortype = ArmorType::INVULNERABLE;
						else
							Log("Unit: ",data.name,", no such ArmorType: ",tempstr);

							*/

			data.armortype = &armortypes.find(tempstr)->second;

			getline(units_file,tempstr,';');
			data.deltav = std::stof(tempstr);

			getline(units_file,tempstr,';');
			data.maxv = std::stof(tempstr);

			getline(units_file,tempstr,';');
			data.maxstamina = std::stof(tempstr);

			getline(units_file,tempstr,'\n');
			data.corpse = tempstr;
		}
		units_file.close();
	}
	else
		Log("Error: can not open units.csv\n");
}

void SpookyGameplay::AddMapObject(MapObject* mapobj)
{
	if(mapobj->type != MapObjectType::NONE)
	{
		GameplayObject* body;

		MapObjectPos pos = CalcMapObjectPos(mapobj);

		bool physical = false;

		switch(mapobj->type)
		{
			case MapObjectType::ACTIONPOINT:
			{
				ActionPoint* point = new ActionPoint(objdata);
				//point->SetDebugColor(&Game::resources->colors.yellow);
				body = point;
			}
			break;
			case MapObjectType::COLLISION:
			{
				physical = true;
				body = new GameplayObject(objdata);
				body->CreateBody(BodyType::STATIC, pos.x, pos.y);
			}
			break;
				/*	case MapObjectType::DESTRUCTIBLE:
					{
						Obstacle* hurdle = new Obstacle(objdata, pos,
				                &hero->body->GetWorldCenter(), units.find("fence")->second);
						hurdle->SetDebugColor(&Game::resources->colors.black);

						physical = true;
						body = hurdle;
					}*/
			break;
			case MapObjectType::PARTICLEEMITTER:
			{
				ParticleEmitter* emitter = new ParticleEmitter(objdata);

				body = emitter;
			}
			break;
			case MapObjectType::SOUNDEMITTER:
			{
				SoundEmitter* emitter;
				emitter = new SoundEmitter(objdata, &sounds, mapobj->soundemitter.soundname,
										   mapobj->soundemitter.deltatime, mapobj->soundemitter.shift);
				emitter->CreateBody(BodyType::STATIC, pos.center_x, pos.center_y);
				emitter->SetActive(false);
				body = emitter;
			}
			break;

			case MapObjectType::SPAWNPOINT:
			{
				SpawnPoint* point = new SpawnPoint(objdata, std::bind(&SpookyGameplay::AddUnit, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
				point->CreateBody(BodyType::STATIC, pos.x, pos.y);
				point->SetActive(false);
				physical = true;

				point->SetDebugColor(&Game::resources->colors.green);

				body = point;

				if(mapobj->name == "spawnboss1")
				{
					spawnboss = point;
					spawnboss->SetDebugColor(&Game::resources->colors.blue);
				}
				else
					spawnpoints.push_back(point);
			}
			break;

			case MapObjectType::TRIGGER:
			{
				Trigger* area = new Trigger(objdata);

				body = area;
			}
			break;
			case MapObjectType::NONE:
			default:
				Log("Error, object without type!\n");
		}

		if(physical)
		{
			if(mapobj->shapetype == Shape::RECTANGLE)
			{
				b2PolygonShape shape;
				shape.SetAsBox(pos.w/2, pos.h/2, b2Vec2(pos.w/2, -pos.h/2), 0);
				body->CreateFixture(shape);
			}
			else if(mapobj->shapetype == Shape::ELLIPSE)
			{
				b2CircleShape shape;
				shape.m_p.Set(pos.w/2, -pos.w/2);
				shape.m_radius = pos.w / 2;

				b2FixtureDef fixture;
				fixture.shape = &shape;

				body->AddFixture(fixture);
			}
			else if(mapobj->shapetype == Shape::POLYGON)
			{
				for(int i = 0; i < mapobj->vertexcount; i++)
				{
					if(mapobj->vertices[i].x != 0)
						mapobj->vertices[i].x /= objdata.boxunit;
					if(mapobj->vertices[i].y != 0)
					{
						mapobj->vertices[i].y /= objdata.boxunit;
						mapobj->vertices[i].y *= -1;
					}
				}

				b2PolygonShape shape;
				shape.Set(mapobj->vertices, mapobj->vertexcount);

				b2FixtureDef fixture;
				fixture.shape=&shape;

				body->AddFixture(fixture);
			}
			else if(mapobj->shapetype == Shape::POLYLINE)
			{
				for(int i = 0; i < mapobj->vertexcount; i++)
				{
					mapobj->vertices[i].x = (float)mapobj->vertices[i].x / objdata.boxunit;
					mapobj->vertices[i].y = -( (float)mapobj->vertices[i].y / objdata.boxunit);
				}

				b2ChainShape shape;
				shape.CreateChain(mapobj->vertices, mapobj->vertexcount);

				b2FixtureDef fixture;
				fixture.shape=&shape;

				body->AddFixture(fixture);
			}
			else
			{
				Log("Error, shape without type!\n");
			}
		}
	}
	else
	{
		Log("Error, MapObjectType == NONE\n");
	}

}

void SpookyGameplay::LoadArmorTypes()
{
	std::fstream armor_file;

	std::string tempstr;

	armor_file.open("Data/Spooky/armor_types.csv", std::ios::in);

	if (armor_file.good())
	{
		getline(armor_file, tempstr); //first row

		while(armor_file.good())
		{
			getline(armor_file,tempstr,';');

			if(tempstr == "\n" || tempstr.empty())
			{
				Log("empty line in armor_types.csv\n");
				break;
			}

			ArmorType temptype;
			armortypes[tempstr] = temptype;
			ArmorType& type = armortypes.find(tempstr)->second;

			type.name = tempstr;

			getline(armor_file,tempstr,';');
			type.normal = std::stof(tempstr);

			getline(armor_file,tempstr,';');
			type.pierce = std::stof(tempstr);

			getline(armor_file,tempstr,';');
			type.siege = std::stof(tempstr);

			getline(armor_file,tempstr,';');
			type.barefist = std::stof(tempstr);

			getline(armor_file,tempstr,';');
			type.bite = std::stof(tempstr);

			getline(armor_file,tempstr,';');
			type.magic = std::stof(tempstr);

			getline(armor_file,tempstr,'\n');
			type.chaos = std::stof(tempstr);

		}
		armor_file.close();
	}
	else
		Log("Error: can not open armor_types.csv\n");
}

void SpookyGameplay::LoadBullets()
{
	std::fstream bullets_file;

	std::string tempstr;

	bullets_file.open("Data/Spooky/bullets.csv", std::ios::in);
	if(bullets_file.good())
	{
		getline(bullets_file, tempstr); //Headers

		while(bullets_file.good())
		{
			getline(bullets_file, tempstr, ';');

			if(tempstr == "\n" || tempstr.empty())
			{
				Log("empty line in bullets.csv");
				break;
			}

			BulletData tempdata;
			bullets[tempstr] = tempdata;
			BulletData& data = bullets.find(tempstr)->second;

			data.name = tempstr;

			getline(bullets_file, tempstr, ';');
			data.graphicname = tempstr;

			getline(bullets_file, tempstr, ';');
			data.cost = std::stof(tempstr);

			getline(bullets_file, tempstr, ';');
			float tempdamage = std::stof(tempstr);

			getline(bullets_file, tempstr, ';');

			if(tempstr == "normal")
				data.damage.normal = tempdamage;
			else if (tempstr == "pierce")
				data.damage.pierce = tempdamage;
			else if (tempstr == "siege")
				data.damage.siege = tempdamage;
			else if (tempstr == "barefist")
				data.damage.barefist = tempdamage;
			else if (tempstr == "magic")
				data.damage.magic = tempdamage;
			else if (tempstr == "bite")
				data.damage.bite = tempdamage;
			else if (tempstr == "chaos")
				data.damage.chaos = tempdamage;
			else
				Log("Bullet: ",data.name,", no such DamageType: ",tempstr);

			getline(bullets_file, tempstr, ';');
			data.deltav = std::stof(tempstr);

			getline(bullets_file, tempstr, ';');
			data.maxv = std::stof(tempstr);

			getline(bullets_file, tempstr, ';');
			data.casttime = std::stof(tempstr);

			getline(bullets_file, tempstr, '\n');
			data.lifetime = std::stof(tempstr);

		}
		bullets_file.close();
	}
	else
		Log("Error : can not open bullets.csv");
}

void SpookyGameplay::LoadObstacles()
{
	std::fstream obstacles_file;

	std::string tempstr;

	obstacles_file.open("Data/Spooky/obstacles.csv", std::ios::in);
	if(obstacles_file.good())
	{
		getline(obstacles_file, tempstr); //first row have only heads of columns

		while(obstacles_file.good())
		{
			getline(obstacles_file,tempstr,';');

			if(tempstr == "\n" || tempstr.empty())
			{
				Log("empty line in obstacles.csv\n");
				break;
			}

			ObstacleData tempdata;
			obstacles[tempstr] = tempdata;
			ObstacleData& data = obstacles.find(tempstr)->second;

			data.name = tempstr;

			getline(obstacles_file,tempstr,';');
			data.graphicname = tempstr;

			getline(obstacles_file,tempstr,';');
			data.framewidth = std::stoi(tempstr);

			getline(obstacles_file,tempstr,';');
			data.frameheight = std::stoi(tempstr);

			getline(obstacles_file,tempstr,';');
			data.frames = std::stoi(tempstr);

			getline(obstacles_file,tempstr,';');
			data.maxendurance = std::stof(tempstr);
			data.currentendurance = data.maxendurance;

			getline(obstacles_file,tempstr,';');
			data.restitution = std::stof(tempstr);

			getline(obstacles_file,tempstr,';');
			data.armor = std::stof(tempstr);

			getline(obstacles_file,tempstr,';');
			data.armortype = &armortypes.find(tempstr)->second;

			getline(obstacles_file,tempstr,';');

			if(tempstr == "lumber")
				data.material = Material::LUMBER;
			else if(tempstr == "none")
				data.material = Material::NONE;
			else if (tempstr == "stone")
				data.material = Material::STONE;
			else if (tempstr == "metal")
				data.material = Material::METAL;
			else
			{
				data.material = Material::NONE;
				Log("Obstacle: ",data.name,", no such Material: ",tempstr);
			}

			getline(obstacles_file,tempstr,';');
			data.objects_after_destruct = std::stoi(tempstr);

			getline(obstacles_file,tempstr,';');
			data.destructedto = tempstr;

			getline(obstacles_file,tempstr,';');
			if (tempstr == "1")
				data.destroyable = true;
			else if (tempstr == "0")
				data.destroyable = false;
			else
				Log("Obstacle: ", data.name, ", wrong text at destroyable");

			getline(obstacles_file,tempstr,';');

			if (tempstr == "1")
				data.moveable = true;
			else if (tempstr == "0")
				data.moveable = false;
			else
				Log("Obstacle: ", data.name, ", wrong text at moveable");

			getline(obstacles_file,tempstr,';');
			data.dmgpersec = std::stoi(tempstr);

			getline(obstacles_file,tempstr,';');
			data.lifetime = std::stof(tempstr);

			getline(obstacles_file,tempstr,'\n');

			if (tempstr == "1")
				data.collide = true;
			else if (tempstr == "0")
				data.collide = false;
			else
				Log("Obstacle: ", data.name, ", wrong text at collide");
		}
		obstacles_file.close();
	}
	else
		Log("Error: can not open obstacles.csv\n");
}

void SpookyGameplay::LoadObstaclePositions()
{
	std::fstream obstacles_file;

	std::string name, text_positionx, text_positiony, text_angle, scale_x, scale_y;

	obstacles_file.open("Data/Spooky/obstacle_positions.csv", std::ios::in);
	if(obstacles_file.good())
	{
		getline(obstacles_file, name); //first row have only heads of columns

		while(obstacles_file.good())
		{
			getline(obstacles_file, name, ';');

			if(name == "\n" || name.empty())
			{
				Log("empty line in obstacles_positions.csv\n");
				break;
			}

			getline(obstacles_file, text_positionx, ';');
			getline(obstacles_file, text_positiony, ';');
			getline(obstacles_file, text_angle, ';');
			getline(obstacles_file, scale_x, ';');
			getline(obstacles_file, scale_y, '\n');

			b2Vec2 pos;
			pos.x = std::stof(text_positionx);
			pos.y = std::stof(text_positiony);

			float obstacleangle = std::stof(text_angle);

			b2Vec2 scale;
			scale.x = std::stof(scale_x);
			scale.y = std::stof(scale_y);

			AddObstacle(name, pos, obstacleangle, scale);
		}
	}
	else
		Log("Error: can not open obstacle_positions.csv\n");
}

void SpookyGameplay::SaveObstacles()
{
	std::fstream obstaclefile, obstaclebackup;

	obstaclefile.open("Data/Spooky/obstacle_positions.csv", std::ios::out);
	obstaclebackup.open("Data/Spooky/obstacle_positions_backup.csv", std::ios::out | std::ios::app);

	obstaclefile << "name;position x;position y;angle;scale.x;scale.y\n";
	obstaclebackup << "name;position x;position y;angle;scale.x;scale.y\n";

	time_t ptime;
	time(&ptime);
	char* date = ctime(&ptime);
	obstaclebackup << "_____________________________________________\n";
	obstaclebackup << "Date: " << date <<"\n";

	for(GameplayObject* gameplayobj : gameplayobjects)
	{
		if(gameplayobj->type == MapObjectType::DESTRUCTIBLE)
		{
			Obstacle* obj = static_cast<Obstacle*>(gameplayobj);
			b2Vec2 pos = obj->body->GetPosition();

			obstaclefile << obj->GetName() << ";";
			obstaclefile << pos.x << ";" << pos.y << ";";
			obstaclefile << obj->body->GetAngle() << ";";
			obstaclefile << obj->scaleratio.x << ";";
			obstaclefile << obj->scaleratio.y << "\n";

			obstaclebackup << obj->GetName() << ";";
			obstaclebackup << pos.x << ";" << pos.y << ";";
			obstaclebackup << obj->body->GetAngle() << ";";
			obstaclebackup << obj->scaleratio.x << ";";
			obstaclebackup << obj->scaleratio.y << "\n";
		}
	}

	obstaclefile.close();
	obstaclebackup.close();
}

MapObjectPos SpookyGameplay::CalcMapObjectPos(MapObject* obj)
{
	MapObjectPos pos;
	pos.x = obj->x / objdata.boxunit;
	pos.y = -(obj->y / objdata.boxunit);
	pos.w = obj->width / objdata.boxunit;
	pos.h = obj->height / objdata.boxunit;
	pos.center_x = pos.x + pos.w/2;
	pos.center_y = pos.y - pos.h/2;
	return pos;
}


void SpookyGameplay::ChangeMap(const std::string& name)
{
	DeleteMap();

	MapInit(name);
	CreateHero();
}

void SpookyGameplay::DeleteMap()
{
	delete tmap;

	if(hero != nullptr)
		hero->Destroy(); // 99% it works, 1% - need manually call GameplayState::Update() after this

	DeletePhysicWorld();
}

void SpookyGameplay::LoadGame(const std::string&)
{

}

void SpookyGameplay::SaveGame(const std::string&)
{

}


void SpookyGameplay::DebugRender()
{
	if(testingrender)
	{

		for(auto it = gameplayobjects.begin(); it != gameplayobjects.end(); ++it)
		{
			(*it)->DebugRender(cam_gameplay);
		}

		//only in normal mode, not tactic
		if(Game::ctrl->mousekey[2] && camattach && gestures.mode)
		{
			cam_gui->Active();
			gestures.Render(cam_gui);
			cam_gameplay->Active();
		}
	}

	if(selectframe)
	{
		Game::renderer->RenderEdges(selectvertices, cam_gameplay);
	}

	//rendering of select border from selectedobjs container
	auto it = selectedobjs.begin();
	while(it != selectedobjs.end())
	{
		(*it)->RenderSelectBorder(cam_gameplay);

		++it;
	}


	if(GetStats("render_soundwave"))
	{
		sounds.RenderSoundWaves(objdata.boxunit, cam_gameplay);
	}
}

SpookyGameplay::~SpookyGameplay()
{
	Log("Killed ", killed, " enemies\n");
	delete tmap;

	Game::audio->StopMusic(500);
	Game::audio->PlayMusic("muzyka2");
}


void SpookyGameplay::BackgroundRender()
{
	cam_gameplay->Active();

	SDL_Rect pos;
	pos.x = 0;
	pos.y = 0;
	pos.w = tmap->tilewidth;
	pos.h = tmap->tileheight;

	SDL_Rect view = cam_gameplay->GetViewArea();

	int x = view.x / tmap->tilewidth;
	int y = view.y / tmap->tileheight;

	int lx = (view.x + view.w) / tmap->tilewidth;
	int ly = (view.y + view.h) / tmap->tileheight;


	// ly + 1 can  out of range tiles[][] array?
	for(int i=y; i < ly+1; i++) // i < h
	{
		for(int j=x; j < lx+1; j++) // j < w
		{
			if(i < tmap->height && j < tmap->width)
			{
				int value = tmap->tilelayer->tiles[i][j]->value;

				pos.x = j * tmap->tilewidth;
				pos.y = i * tmap->tileheight;

				Game::renderer->Render(tmap->tilemodel[value]->texturedata.get()->id,
									   pos, tmap->tilemodel[value]->source, cam_gameplay);
			}
		}
	}
}


void SpookyGameplay::CreateHero()
{
	HeroStatistics herostats;
	herostats.health = GetStats("hero_health");
	herostats.maxhealth = GetStats("hero_maxhealth");
	herostats.health_regen = GetStats("hero_health_regen");
	herostats.stamina = GetStats("hero_stamina");
	herostats.maxstamina = GetStats("hero_maxstamina");
	herostats.stamina_regen = GetStats("hero_stamina_regen");

	hero = new Character(objdata, &sounds, herostats);

	lasthp = hero->GetHealth();
}

void SpookyGameplay::InitArenaGameplay()
{
	std::fstream arenafile;
	std::string temp;
	arenafile.open("Data/Spooky/arena_mode.csv", std::ios::in);

	if(arenafile.good())
	{
		getline(arenafile,temp);

		while(arenafile.good())
		{
			ArenaWave wave;
			getline(arenafile, temp, ';'); //index

			if(temp == "\n" || temp.empty())
			{
				Log("empty line in arena_mode.csv\n");
				break;
			}

			getline(arenafile, temp, ';'); //zombie1
			wave.zombie1 = std::stoi(temp);

			getline(arenafile, temp, ';'); //zombie2
			wave.zombie2 = std::stoi(temp);

			getline(arenafile, temp, ';'); //zombietank
			wave.zombietank = std::stoi(temp);

			getline(arenafile, temp, ';'); //skeleton archer
			wave.archer = std::stoi(temp);

			getline(arenafile, temp, ';'); //boss
			wave.boss = std::stoi(temp);


			getline(arenafile, temp, ';'); //delta time
			wave.deltatime = std::stoi(temp);

			getline(arenafile, temp, '\n'); //delay
			wave.delay = std::stoi(temp);

			arenawaves.push_back(wave);
		}
		arenafile.close();
	}
	else
		Log("arenafile not found\n");

	LoadText("arena_win_wave", "normal", Game::resources->colors.red);

	actualwave = 0;
}


void SpookyGameplay::StartArenaWave(int number)
{
	int amount = arenawaves[number].zombie1 + arenawaves[number].zombie2;
	amount += arenawaves[number].zombietank + arenawaves[number].archer;

	enemiesqueue.reserve(amount);

	for(int i = 0; i < arenawaves[number].zombie1; i++)
		enemiesqueue.push_back(1);
	for(int i = 0; i < arenawaves[number].zombie2; i++)
		enemiesqueue.push_back(2);
	for(int i = 0; i < arenawaves[number].zombietank; i++)
		enemiesqueue.push_back(3);
	for(int i = 0; i < arenawaves[number].archer; i++)
		enemiesqueue.push_back(4);

	std::random_shuffle(enemiesqueue.begin(), enemiesqueue.end());

	bossnumber = arenawaves[number].boss;


	if(arenawaves[number].boss > 0)
		Game::audio->StopMusic(2000);
	else if(bossmusic)
	{
		Game::audio->PlayMusic("muzyka1");
		bossmusic = false;
	}

	std::string wavename = Game::resources->GetTextLine("arena_wave");
	wavename += " " + std::to_string(number);
	Game::resources->LoadCustomTextTexture(wavename, wavename, "title", Game::resources->colors.red);

	textwave = new GuiObject(wavename, 0, 200);
	textwave->Align(Game::screenrect, AlignmentX::CENTER);
	State::AddGuiObject(textwave);

	wavetimer.AddDelta(5000);

	beforewavetime = true;
	deletingbonus = true;
}

void SpookyGameplay::UpdateArenaGameplay()
{
	static GameTimer spawntimer(5000);
	static GameTimer nextwavetimer(5000);
	static GameTimer bonustimer; //if player killed all enemies before time


	static unsigned int cheattimer = SDL_GetTicks();
	if(Game::ctrl->keystate[SDL_SCANCODE_GRAVE] && cheattimer < SDL_GetTicks())
	{
		actualwave++;
		if(actualwave >= arenawaves.size())
			actualwave = 0;

		std::cout<<"next wave: " << actualwave + 1 << "\n";
		cheattimer = SDL_GetTicks() + 200;
	}

	if( !enemiesqueue.empty())
	{
		if(wavetimer.Passed())
		{
			if(textwave != nullptr && wavetimer.Passed())
			{
				textwave->Destroy();
				textwave = nullptr;
			}

			if(spawntimer.Passed())
			{
				std::string enemyid;
				switch(enemiesqueue.back())
				{
					case 1:
						enemyid = "zombie1";
						break;
					case 2:
						enemyid = "zombie2";
						break;
					case 3:
						enemyid = "zombie_tank1";
						break;
					case 4:
						enemyid = "zombie3";
						break;
					default:
						std::cout<<"bad number: " << enemiesqueue.back() << "\n";
				}

				int spawnamount = spawnpoints.size();
				int id = rand() % spawnamount;

				spawnpoints[id]->SpawnUnit(enemyid, enemyplayer);
				alive++;
				enemiesqueue.pop_back();


				if(bossnumber > 0)
				{
					for(int i = 0; i < bossnumber; i++)
						spawnboss->SpawnUnit("zombie_boss1", enemyplayer);
					alive++;
					bossnumber = 0;
					Game::audio->PlayMusic("mus_bat_monster2");
					bossmusic = true;
				}

				spawntimer.AddDelta(arenawaves[actualwave].deltatime);
			}
		}

		if(enemiesqueue.empty()) //last enemy was spawned
		{
			nextwavetimer.AddDelta(arenawaves[actualwave].delay);
			std::cout<<"end wave\n";
		}
	}
	else if(nextwavetimer.Passed())
	{
		if(actualwave < arenawaves.size()-1)
		{
			actualwave++;
			StartArenaWave(actualwave);
		}
		else
		{
			//win all waves
			std::cout<<"You win!\n";
		}
	}
	else if(alive == 0 && beforewavetime)
	{
		std::cout<<"killed all enemies before time!\n";
		endwavebonus = new GuiObject("arena_win_wave", 0, 300);
		endwavebonus->Align(Game::screenrect, AlignmentX::CENTER);
		State::AddGuiObject(endwavebonus);
		nextwavetimer.AddDelta(5000);
		bonustimer.AddDelta(3000);

		if(bossmusic)
		{
			Game::audio->StopMusic(1000);
			//bossmusic = false;
		}
		beforewavetime = false;
	}

	if(alive == 0 && deletingbonus && bonustimer.Passed() && !beforewavetime)
	{
		deletingbonus = false;
		endwavebonus->Destroy();
		endwavebonus = nullptr;
	}
}

Bullet* SpookyGameplay::AddBullet(const std::string& name, b2Vec2 source, b2Vec2 target) //float posx, float posy)
{
	Bullet* obj = new Bullet(objdata, &sounds, source, target,
							 bullets.find(name)->second);

	return obj;
}

Unit* SpookyGameplay::AddUnit(const std::string& name, b2Vec2 pos, Player* owner)
{
	const b2Vec2* target;
	if(hero != nullptr)
		target = &hero->body->GetWorldCenter();
	else
		target = new b2Vec2(tmap->width / 2, -tmap->height / 2);

	Unit* obj = new Unit(objdata, &sounds, pos, target,
						 units.find(name)->second, owner);

	return obj;
}

Obstacle* SpookyGameplay::AddObstacle(const std::string& name, b2Vec2 pos,
									  float startangle, b2Vec2 scaleratio)
{
	Obstacle* obj = new Obstacle(objdata, pos, &sounds, startangle,
								 scaleratio, obstacles.find(name)->second);

	return obj;
}

void SpookyGameplay::LoadStats()
{
	std::fstream stats_file;
	std::string name, value, temp;
	stats_file.open("Data/Spooky/stats.csv", std::ios::in);
	if(stats_file.good())
	{
		getline(stats_file,temp);

		while(stats_file.good())
		{
			getline(stats_file,name,';');
			getline(stats_file,value,';');
			getline(stats_file,temp,'\n'); //notes

			stats[name] = std::stof(value);
		}
		stats_file.close();
	}
	else
		Log("Error while opening stats_file\n");
}

float SpookyGameplay::GetStats(const std::string& name)
{
	if(stats.find(name) != stats.end())
		return stats.find(name)->second;
	else
	{
		Log("error, no stats for name:",name,"\n");
		return 0;
	}
}
