#ifndef GESTURE_HPP_INCLUDED
#define GESTURE_HPP_INCLUDED

#include "../States/GameplayState.hpp"

float VertexDistance(glm::vec2 v1, glm::vec2 v2);

namespace GestureDirection
{
enum Direction
{
	N,
	NE,
	E,
	SE,
	S,
	SW,
	W,
	NW,
	NONE=9
};
}

class Gesture
{
	public:
		std::vector<GestureDirection::Direction> directions;
		int actionid;

		Gesture()
		{

		}
		~Gesture()
		{

		}
};

class GestureManager
{
		std::vector<Gesture> gestures; // loaded from file
		std::vector<glm::vec2> gesturepoints;

		int FindCorrespondGesture(Gesture& g);
		bool GestureComparer(Gesture& g1, Gesture& g2);
		GestureDirection::Direction ConvertDirection(float angle);
		int GetActionId(int gesture_id);
	public:
		bool mode;
		glm::vec2 startpos;

		void Render(Camera* cam);
		int Update(bool attachcam, int distance);

		GestureManager();
		~GestureManager();
};

#endif // GESTURE_HPP_INCLUDED
