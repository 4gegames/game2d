#ifndef SPOOKYOBJECTS_HPP_INCLUDED
#define SPOOKYOBJECTS_HPP_INCLUDED

#include<queue>

#include <list>
#include<Box2D/Box2D.h>

#include "../GameplayObjects/GameplayObject.hpp"
#include "SoundWaves.hpp"

#define DEGTORAD 0.0174532925199432957f
#define RADTODEG 57.295779513082320876f

namespace Collide
{
enum CollideCategory
{
	TERRAIN = 0x0001,
	DESTRUCTIBLE = 0x0002, //placeable, buildings, walls, maybe trees?

	ROOF = 0x0004, //optional, to hiding roofs of building; hide when player come under
	//if roof collide with player/unit, then should be hide

	/*BULLET = 0x0008,

	UNIT = 0x0010, //maybe divide to player number units? - like in game programming gems,
	//all players are in enum bit mask

	PLAYER = 0x0020,

	ENEMYBULLET = 0x0040,
	*/

	PLAYER0_UNIT   = 0x0100, //always main player
	PLAYER0_BULLET = 0x0200,

	PLAYER1_UNIT   = 0x0400, //default computer enemy
	PLAYER1_BULLET = 0x0800,

	PLAYER2_UNIT   = 0x1000,
	PLAYER2_BULLET = 0x2000,

	PLAYER3_UNIT   = 0x4000,
	PLAYER3_BULLET = 0x8000,

	/**************************
	if we need separately handling collision with (for example):
	-bullets (non colliding with friendly units)
	-flying units (non colliding with infantry)
	-ghosts (non colliding with all objects except magic bullets)
	should add types such as PLAYER0_FLY, PLAYER0_GHOST etc.
	**************************/
};
}

class ContactListener : public b2ContactListener
{
	public:
		void BeginContact(b2Contact* contact);
		void EndContact(b2Contact* contact);
};

enum class PlayerType
{
	HUMAN,
	COMPUTER

	//if add multi player, then need additional type for human playing via Internet
};

enum class FractionRelation
{
	ALLY,
	NEUTRAL,
	ENEMY
};

class Player
{
		//CollideCategory
		int ally;
		int neutral;
		int enemy;

	public:
		int id;

		PlayerType type;

		int unitflag; //flag from CollideCategory, (e.g. PLAYER1_UNIT)
		int bulletflag; //flag from CollideCategory, (e.g., PLAYER1_BULLET)

		//std::vector<Unit*> units; // container with pointers to controlled units

		//structure with Collide::CollideCategory fractions (PLAYER 1, 2, 3)
		//in all relations colliding with units, but only in neutral and enemy colliding with bullets
		/*struct
		{
			std::vector<Player*> ally;
			std::vector<Player*> neutral;
			std::vector<Player*> enemy;
		} fractions; */

		int collidingflag; //sum of ally, neutral and enemy, for fixture.filter.maskBits

		std::vector< std::pair<Player*, FractionRelation> > relationplayers;

		/***
		must add player_id to Player class, and in SetRelation finding this id
		***/

		void SetRelation(Player* player, FractionRelation relation)
		{
			for(auto objpair : relationplayers)
			{
				if(objpair.first->id == player->id)
				{
					objpair.second = relation;
					return;
				}
			}
			relationplayers.push_back(std::make_pair(player, relation));

			CalculateColliding();
		}

		void CalculateColliding()
		{
			for(auto objpair : relationplayers)
			{
				if(objpair.second == FractionRelation::ALLY)
				{
					ally |= objpair.first->unitflag;
					//ally |= objpair.first->bulletflag;
				}
				else if(objpair.second == FractionRelation::NEUTRAL)
				{
					neutral |= objpair.first->unitflag;
					neutral |= objpair.first->bulletflag;
				}
				else //enemy
				{
					enemy |= objpair.first->unitflag;
					enemy |= objpair.first->bulletflag;
				}
			}

			collidingflag = ally | neutral | enemy;
		}


		/***
		-container with pointers to all managed units
		-variables with flags (Collide::CollideCategory) for ally, neutral and hostile fraction
		-summary line of sight
		***/

		Player(int playerid, Collide::CollideCategory flagunit, Collide::CollideCategory flagbullet)
		{
			id = playerid;
			unitflag = flagunit;
			bulletflag = flagbullet;

			if(playerid == 0)
				type = PlayerType::HUMAN;
			else
				type = PlayerType::COMPUTER;

			ally = flagunit;
			neutral = 0;
			enemy = 0;

			CalculateColliding();
		}

};

//void ChangeRelationship(Player& p1, Player& p2, FractionRelation relation);

//need group of collisions depend of kind of object

enum class UnitType
{
	FLY,
	WALKING,
	RIDER
};

enum class OrderType
{
	ATTACK,
	DEFEND,
	FOLLOW,
	INTERACTION,
	PATROL,
	MOVE
};
/*
enum class DamageType
{
	NORMAL, //swords, weapons
	PIERCE, //arrows, javelins
	SIEGE, //bombs, siege machines
	BAREFIST, //raging villager
	BITE, //ghouls
	MAGIC, //mages and their bullets
	CHAOS //deals 100% damage to all armors except invulnerable
};

enum class ArmorType
{
	UNARMORED,
	LIGHT,
	MEDIUM,
	HEAVY,
	ETERIC, //dmg only form spells
	FORTIFIED, //buildings
	DIVINE, // almost indestructable
	INVULNERABLE //for NPC`s
};*/

//probably make magic damage also a struct with elements/resistaces inside;

struct Damage
{
	float normal = 0, pierce = 0, siege = 0,
		  barefist = 0, bite = 0, magic = 0, chaos = 0;
};

struct ArmorType
{
	std::string name;
	//Damage from..
	float normal, pierce, siege, barefist, bite, magic, chaos;
};

struct UnitData
{
	std::string name, graphicname, behaviour, corpse;
	float maxhealth, currenthealth, range, armor, deltav, maxv, maxstamina;
	float hpregen;
	Damage damage;
	ArmorType* armortype;
};

enum class Material
{
	NONE,
	LUMBER,
	STONE,
	METAL
};

struct ObstacleData
{
	std::string name, graphicname, destructedto;
	float maxendurance, currentendurance, restitution, lifetime, armor;
	int dmgpersec;
	int framewidth, frameheight, frames;
	int objects_after_destruct;
	bool destroyable, moveable, collide;
	Material material;
	ArmorType* armortype;
};

struct BulletData
{
	std::string name, graphicname;
	float deltav, maxv, lifetime, casttime, cost;
	Damage damage;
};

//Obstacles
class Obstacle : public GameplayObject
{
		ObstacleData statistics;

		SoundWaveManager* sounds;

		GameTimer lifetimer;

	public:
		Obstacle(const PhysicObjectData& physicdata, b2Vec2 pos, SoundWaveManager* soundsptr,
				 float angle, b2Vec2 scaleratio, const ObstacleData& data );

		~Obstacle() {}

		std::string GetName()
		{
			return statistics.name;
		}

		void Update();

		void OnHit(const Damage& damage);

		void OnHit(int value);

		void Destroy();
};



// Orders for Units
struct Order
{
	OrderType type;

	float x, y; //coordinates
	float x2, y2; //coordinates for some orders, e.g. for patrol
	//maybe pointer to object with which must interact
};

enum class ItemType
{
	WEAPON,
	BOOK,
	ARMOR,
	INGREDIENT,
	RECIPE,
	MIXTURE
};

//need file with all items, where will be all items data,
//or divide to a few files, one for category

class Item
{
		ItemType itemkind;

		float attack;
		float defence;
		bool quest;
	public:
		Item(int dataline);
		~Item();
};

class Skill
{

};

struct SkillTree
{
	//skills loaded from file
};

enum class SpellType
{
	SMALLFIREBALL,
	BIGFIREBALL
};

struct HeroStatistics
{
	float stamina;
	float health;

	float maxstamina;
	float maxhealth;

	float health_regen;
	float stamina_regen;

	ArmorType armortype;
	//all stats!

	//calculating of result stats consists in adding/multiplying value with all skills modifiers
};

//player
class Character : public GameplayObject
{
		HeroStatistics stats;
		SoundWaveManager* sounds;

	public:

		SpellType activespell; // 0 - small fireball, 1 - big fireball

		Character(const PhysicObjectData& physicdata, SoundWaveManager* sounds,
				  const HeroStatistics& statistics);
		~Character();

		void Update();

		void RotateToPoint(const b2Vec2& param);
		//void Control();

		void DecreaseHealth(const Damage& damage)
		{
			float value = 0;

			value += damage.normal * stats.armortype.normal;
			value += damage.pierce * stats.armortype.pierce;
			value += damage.siege * stats.armortype.siege;
			value += damage.barefist * stats.armortype.barefist;
			value += damage.bite * stats.armortype.bite;
			value += damage.magic * stats.armortype.magic;
			value += damage.chaos * stats.armortype.chaos;

			stats.health -= value;
			sounds->AddSoundWave("player_hit", body->GetWorldCenter());
		}

		int GetHealth()
		{
			return stats.health;
		}

		int GetMaxHealth()
		{
			return stats.maxhealth;
		}

		void DecreaseStamina(unsigned int value)
		{
			stats.stamina -= value;
		}

		void IncreaseStamina(unsigned int value)
		{
			stats.stamina += value;
			if(stats.stamina > stats.maxstamina)
				stats.stamina = stats.maxstamina;
		}

		int GetStamina()
		{
			return stats.stamina;
		}

		int GetMaxStamina()
		{
			return stats.maxstamina;
		}

		bool OnCollision(GameplayObject*)
		{
			return false;
		}

		//void RecalculateStats(); //call if hero gain new level, skill, maybe magic item
};

struct MapObjectPos
{
	float x, y, w, h, center_x, center_y;
};

/******************************************************************************
All physic objects should have a health points statistic
loose hp when collide with another object with velocity > v min
result damage = damage of object (weapon) * v - defend point (of defender)
******************************************************************************/

//Shortcut can bind items, skills, group of units
/*
struct Shortcut
{

};

class ShortcutBar
{

};
*/

class Spell
{
	public:
		void Cast()
		{
			/**
			creating bullets, according to file (?) with specification
			in this place AddBullet function
			**/
		}

		Spell()
		{
			//GameplayObject* obj = new GameplayObject(objdata);
		}
};


class Bullet : public GameplayObject
{
		struct
		{
			int oncollision;
			int ondestroy;
			int oncreate;
		} effects;

		GameTimer timer;

		BulletData statistics;

		SoundWaveManager* sounds;

		bool destroycollide;

		bool explosion;

		std::string name;
	public:
		Bullet(const PhysicObjectData& physicdata, SoundWaveManager* soundsptr,
			   b2Vec2 source, b2Vec2 target, BulletData& data,
			   bool collisiondestroy = true, bool collide_explosion = false);

		/****
		need fraction of unit, which create this bullet
		probably need constructor parameter Collide::PLAYERx_BULLET
		****/

		void Update();
		~Bullet();

		float GetCost();

		float GetCasttime();

		bool OnCollision(GameplayObject* colliding);

		void Destroy();
};

class Unit : public GameplayObject
{
		SoundWaveManager* sounds;

		const b2Vec2* target;

		GameTimer collidetimer;

		GameTimer regentimer; //regeneration of hp and stamina

		UnitType unitkind;
		std::queue<Order> orderqueue;

		bool playercollide;
		Character* playerptr;

		bool obstaclecollide;
		Obstacle* obstacleptr;

		Player* owner;
	public:

		UnitData statistics;

		Unit(const PhysicObjectData& physicdata, SoundWaveManager* sounds,
			 b2Vec2 pos, const b2Vec2* targetptr, const UnitData& data, Player* playerowner);
		void Update();
		~Unit();

		int GetHealth();
		int GetMaxHealth();
		void IncreaseHealth(float value);
		void DecreaseHealth(const Damage& damage);

		bool OnCollision(GameplayObject* colliding);
		void OnEndCollision();
};




typedef std::function<void(std::string, b2Vec2, Player*)> spawndelegate;

class SpawnPoint : public GameplayObject
{
		spawndelegate spawnfunc;
		//std::string unit; //ID of unit to spawn
		/**
		probably we need spawnsets file with all informations
		(units to spawn and amount, when)
		**/

	public:
		SpawnPoint(const PhysicObjectData& physicdata, spawndelegate func)
			: GameplayObject(physicdata)
		{
			type = MapObjectType::SPAWNPOINT;
			spawnfunc = func;
			destroyable = false;
		}

		void SpawnUnit(const std::string& id, Player* playerptr)
		{
			spawnfunc(id, body->GetWorldCenter(), playerptr);
		}

		~SpawnPoint()
		{

		}
};

class ActionPoint : public GameplayObject
{
		std::vector<std::string> id; //id of actions in this point
	public:
		ActionPoint(const PhysicObjectData& physicdata)
			: GameplayObject(physicdata)
		{
			type = MapObjectType::ACTIONPOINT;

			destroyable = false;
		}

		~ActionPoint()
		{

		}
};

class Trigger : public GameplayObject
{
		//probably need physical body to checking collision with player
	public:
		Trigger(const PhysicObjectData& physicdata)
			: GameplayObject(physicdata)
		{
			type = MapObjectType::TRIGGER;

			destroyable = false;
		}

		~Trigger() {}
};

class ParticleEmitter : public GameplayObject
{
	public:
		ParticleEmitter(const PhysicObjectData& physicdata)
			: GameplayObject(physicdata)
		{
			type = MapObjectType::PARTICLEEMITTER;

			destroyable = false;
		}

		~ParticleEmitter() {}
};

class SoundEmitter : public GameplayObject
{
		std::string soundname;
		unsigned int timer;
		unsigned int deltatime;
		SoundWaveManager* sounds;
		bool enablesounds;
	public:
		SoundEmitter(const PhysicObjectData& physicdata, SoundWaveManager* soundmanager,
					 const std::string& name="walk", int delta= 1000, int shift = 0);

		void EnableSounds(bool value);

		void Update();

		~SoundEmitter() {}
};


///////////////////////////////////////////////////////////////////////////////
// EDIT MODE
///////////////////////////////////////////////////////////////////////////////

enum class EditModeTool
{
	DEPLOY,
	SELECT,
	MOVE,
	ROTATE,
	SCALE,
	NONE
};


#endif // SPOOKYOBJECTS_HPP_INCLUDED
