#include "SoundWaves.hpp"

SoundWaveManager::SoundWaveManager()
{

}

SoundWaveManager::~SoundWaveManager()
{
	delete []circlesides;
}

void SoundWaveManager::InitSound(bool display, int sides)
{
	render = display;
	vcount = sides;

	circlesides = new b2Vec2[vcount];

	int j = 0;
	for (int a = 0; a < 360; a += 360 / vcount)
	{
		double heading = a * PI / 180;
		circlesides[j].Set( cos(heading), sin(heading) );
		j++;
	}

	//loading sounds.csv file
	std::fstream sounds_file;
	std::string name, filename, texttype, textlength, textradius;
	sounds_file.open("Data/Spooky/sounds.csv", std::ios::in);
	if(sounds_file.good())
	{
		getline(sounds_file,name); //first row have only heads of columns

		while(sounds_file.good())
		{
			getline(sounds_file,name,';');
			getline(sounds_file,filename,';');
			getline(sounds_file,texttype,';');
			getline(sounds_file,textlength,';');
			getline(sounds_file,textradius,'\n');

			SoundType type;
			if(texttype == "neutral")
				type = SoundType::NEUTRAL;
			else if(texttype == "warning")
				type = SoundType::WARNING;
			else if(texttype == "danger")
				type = SoundType::DANGER;
			else
				Log("sound: ", name, " has invalid type\n");

			int length = std::stoi(textlength);
			float radius = std::stof(textradius);

			sounds[name] = {name, type, length, radius};

			//need load this sound file to Audio module
			Game::audio->AddSound(name, "Data/Sounds/"+filename);
		}
		sounds_file.close();
	}
	else
		Log("Error while opening sounds_file\n");
}

void SoundWaveManager::AddSoundWave(const std::string& name, b2Vec2 pos)
{
	SoundWave wave(sounds.find(name)->second, pos);
	//soundwaves.push_back(new SoundWave(sounds.find(name)->second, pos));
	soundwaves.push_back(wave);
}

int SoundWaveManager::GetSoundWaveTime(const std::string& name)
{
	return sounds.find(name)->second.length;
}

void SoundWaveManager::RenderCircle(b2Vec2 pos, float radius, SDL_Color* color, float length_unit, Camera* cam)
{
	std::vector<float> v;
	for (int i = 0; i < vcount; ++i)
	{
		v.push_back((circlesides[i].x * radius + pos.x) * length_unit);
		v.push_back( -(circlesides[i].y * radius + pos.y) * length_unit);
	}

	Game::renderer->RenderEdges(v, cam, color);
}

void SoundWaveManager::RenderSoundWaves(float length_unit, Camera* cam)
{
	//need constant time cycle with pulse wave animation, like in Mark of the Ninja
	if(render)
	{
		SDL_Color* color;
		for(auto iter = soundwaves.begin(); iter != soundwaves.end(); ++iter)
		{
			switch( (*iter).sound.type)
			{
				case SoundType::NEUTRAL:
					color = &Game::resources->colors.white;
					break;
				case SoundType::WARNING:
					color = &Game::resources->colors.yellow;
					break;
				case SoundType::DANGER:
				default:
					color = &Game::resources->colors.red;
			}

			RenderCircle( (*iter).centre, (*iter).sound.radius, color, length_unit, cam);
		}
	}
}

void SoundWaveManager::SetRenderingWaves(bool value)
{
	render = value;
}

bool SoundWaveManager::GetRenderingWaves()
{
	return render;
}


void SoundWaveManager::UpdateSoundWaves()
{
	//deleting
	for(auto iter = soundwaves.begin(); iter != soundwaves.end(); ++iter)
	{
		int t = SDL_GetTicks();
		if( (*iter).endtime < t )
		{
			//delete (*iter);
			iter = soundwaves.erase(iter);
		}
	}
}
