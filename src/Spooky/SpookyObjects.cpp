#include "SpookyObjects.hpp"


void ContactListener::BeginContact(b2Contact* contact)
{
	void* firstdata = contact->GetFixtureA()->GetBody()->GetUserData();
	void* seconddata = contact->GetFixtureB()->GetBody()->GetUserData();

	if(firstdata && seconddata)
	{
		GameplayObject* obj1 = static_cast<GameplayObject*>( firstdata );
		GameplayObject* obj2 = static_cast<GameplayObject*>( seconddata );


		///if obj1->OnCollision() not handling this collision,
		///then trying handling this in obj2->OnCollision()
		if( !obj1->OnCollision(obj2))
		{
			obj2->OnCollision(obj1);
		}
	}
}

void ContactListener::EndContact(b2Contact* contact)
{
	void* firstdata = contact->GetFixtureA()->GetBody()->GetUserData();
	if (firstdata)
	{
		static_cast<GameplayObject*>(firstdata)->OnEndCollision();
	}

	void* seconddata = contact->GetFixtureB()->GetBody()->GetUserData();
	if (seconddata)
	{
		static_cast<GameplayObject*>(firstdata)->OnEndCollision();
	}
}


Item::Item(int)
{

}

Item::~Item()
{

}

//character class

Character::Character(const PhysicObjectData& physicdata, SoundWaveManager* soundsptr,
					 const HeroStatistics& statistics)
	: GameplayObject(physicdata)
{
	type = MapObjectType::PLAYER;

	stats = statistics;

	sounds = soundsptr;

	//Warnin! Hardcode!!1
	stats.armortype.name = "heroic";
	stats.armortype.normal = 0.9f;
	stats.armortype.pierce = 0.9f;
	stats.armortype.siege = 0.9f;
	stats.armortype.barefist = 0.9f;
	stats.armortype.bite = 0.9f;
	stats.armortype.magic = 0.9f;
	stats.armortype.chaos = 1;


	//hero->body->SetFixedRotation(true);

	int w = 64;

	img = new Image("player", IsGui::NONE, w,w,0,0,w,w);

	SetAnimation(img,150, w,w, 8*w, 7*w);
	anim->SetNumberFrames(5, 5);
	anim->SetNumberFrames(4, 3);

	anim->SetBaseAnimation(5);

	CreateBody(BodyType::DYNAMIC, 5.0f, -5.0f);

	b2CircleShape shape;
	shape.m_radius = 0.5f;

	b2FixtureDef fixture;
	fixture.shape = &shape;
	fixture.density = 1.0f;
	fixture.friction = 0.2f;
	fixture.restitution= 0.0f;

	fixture.filter.categoryBits = Collide::PLAYER0_UNIT;
	fixture.filter.maskBits = Collide::TERRAIN | Collide::DESTRUCTIBLE
							  | Collide::PLAYER0_UNIT | Collide::PLAYER1_UNIT | Collide::PLAYER1_UNIT;

	AddFixture(fixture);

	body->SetLinearDamping(5.0f);
	body->SetAngularDamping(5.0f);

	activespell = SpellType::SMALLFIREBALL;
}

Character::~Character()
{

}


void Character::Update()
{
	GameplayObject::Update();

	static GameTimer staminatimer;
	if(stats.stamina < stats.maxstamina && staminatimer.Passed())
	{
		stats.stamina += stats.stamina_regen;
		staminatimer.AddDelta(1000);
	}

	static GameTimer healthtimer;
	if(stats.health < stats.maxhealth && healthtimer.Passed())
	{
		stats.health += stats.health_regen;
		healthtimer.AddDelta(1000);
	}
}


void Character::RotateToPoint(const b2Vec2& param)
{
	b2Vec2 totarget = param - body->GetPosition();
	float angle = atan2f( -totarget.x, totarget.y );

	body->SetTransform( body->GetPosition(), angle);

	///gradual rotating, but not works properly with non-regular bodies
	///(for example with joints)
	/*
	float nextAngle = body->GetAngle() + body->GetAngularVelocity() / 60.0;
	float totalRotation = angle - nextAngle;
	while ( totalRotation < -180 * DEGTORAD )
		totalRotation += 360 * DEGTORAD;
	while ( totalRotation >  180 * DEGTORAD )
		totalRotation -= 360 * DEGTORAD;
	float desiredAngularVelocity = totalRotation * 60;
	float torque = body->GetInertia() * desiredAngularVelocity / (1/60.0);
	body->ApplyTorque( torque, true );
	*/
}


Bullet::Bullet(const PhysicObjectData& physicdata, SoundWaveManager* soundsptr,
			   b2Vec2 source, b2Vec2 target, BulletData& data,
			   bool collisiondestroy, bool collide_explosion)
	: GameplayObject(physicdata)
{
	statistics = data;

	type = MapObjectType::BULLET;

	//damageptr = damage;

	sounds = soundsptr;

	img = new Image(data.graphicname, IsGui::NONE);//, 0,0, 0,0,50,50);

	CreateBody(BodyType::DYNAMIC, source.x, source.y);
	body->SetBullet(true);

	b2CircleShape shape;

	if(data.name == "fireball_small")
		shape.m_radius = 0.25f;
	else
		shape.m_radius = 0.5f;


	b2FixtureDef fixture;
	fixture.shape = &shape;
	fixture.density = 1.0f;
	fixture.friction = 0.5f;
	fixture.restitution= 0.2f;
	//body->SetAngularDamping(1);

	fixture.filter.categoryBits = Collide::PLAYER0_BULLET; //hardcoded
	fixture.filter.maskBits = Collide::TERRAIN | Collide::DESTRUCTIBLE | Collide::PLAYER1_UNIT;

	AddFixture(fixture);
	//////////////////////////////////////////////////
	b2Vec2 totarget = target - source;

	//must be (-x) like in function RotateToPoint()
	float angle = atan2f( -totarget.x, totarget.y );
	body->SetTransform( body->GetPosition(), angle);


	b2Vec2 force;
	angle = atan2f( totarget.x, totarget.y );
	force.x = sin(angle) * statistics.maxv * body->GetMass();
	force.y = cos(angle) * statistics.maxv * body->GetMass();

	//body->ApplyAngularImpulse( 45.0f, true );
	body->ApplyLinearImpulse(force, body->GetWorldCenter(), true);

	timer.AddDelta(data.lifetime);

	destroycollide = collisiondestroy;
	explosion = collide_explosion;
}


bool Bullet::OnCollision(GameplayObject* colliding)
{
	if(destroycollide)
		Destroy();

	if(explosion)
	{

	}

	if(colliding->type == MapObjectType::UNIT)
	{
		Unit* u = static_cast<Unit*>(colliding);

		u->DecreaseHealth(statistics.damage);
		return true;
	}
	else if(colliding->type == MapObjectType::DESTRUCTIBLE)
	{
		Obstacle* obst = static_cast<Obstacle*>(colliding);

		obst->OnHit(statistics.damage);
		return true;
	}
	else
		return false;
}

void Bullet::Update()
{
	GameplayObject::Update();

	if(timer.Passed())
	{
		Destroy();
	}
}

void Bullet::Destroy()
{
	todestroy = true;

	//explosion effect
}

Bullet::~Bullet()
{

}

float Bullet::GetCasttime()
{
	return statistics.casttime;
}

float Bullet::GetCost()
{
	return statistics.cost;
}

Unit::Unit(const PhysicObjectData& physicdata, SoundWaveManager* soundsptr,
		   b2Vec2 pos, const b2Vec2* targetptr, const UnitData& data, Player* ownerplayer)
	: GameplayObject(physicdata)
{
	sounds = soundsptr;

	statistics = data;

	owner = ownerplayer;

	type = MapObjectType::UNIT;

	playercollide = false;
	playerptr = nullptr;

	obstaclecollide = false;
	obstacleptr = nullptr;

	Image* newimg = new Image(data.graphicname, IsGui::NONE);
	SetImage(newimg);

	CreateBody(BodyType::DYNAMIC, pos.x, pos.y);
	b2PolygonShape dynamicBox;

	if(statistics.name == "zombie_boss1")
		dynamicBox.SetAsBox(1.0f, 1.0f);
	else
		dynamicBox.SetAsBox(0.5f, 0.5f);

	//CreateFixture(dynamicBox, 1.0f, 0.5f, 0.2f);
	b2FixtureDef fixture;
	fixture.shape = &dynamicBox;
	fixture.density = 1.0f;
	fixture.friction = 0.5f;
	fixture.restitution= 0.2f;

	fixture.filter.categoryBits = owner->unitflag;
	fixture.filter.maskBits = Collide::TERRAIN | Collide::DESTRUCTIBLE
							  | owner->collidingflag;

	AddFixture(fixture);


	body->SetLinearDamping(5.0f);
	body->SetAngularDamping(5.0f);

	target = targetptr;
}

int Unit::GetHealth()
{
	return statistics.currenthealth;
}

int Unit::GetMaxHealth()
{
	return statistics.maxhealth;
}

void Unit::IncreaseHealth(float value)
{
	statistics.currenthealth += value;

	if(statistics.currenthealth > statistics.maxhealth)
		statistics.currenthealth = statistics.maxhealth;
}

void Unit::DecreaseHealth(const Damage& damage)
{
	float value = 0;

	value += damage.normal * statistics.armortype->normal;
	value += damage.pierce * statistics.armortype->pierce;
	value += damage.siege * statistics.armortype->siege;
	value += damage.barefist * statistics.armortype->barefist;
	value += damage.bite * statistics.armortype->bite;
	value += damage.magic * statistics.armortype->magic;
	value += damage.chaos * statistics.armortype->chaos;

	statistics.currenthealth -= value;
}

bool Unit::OnCollision(GameplayObject* colliding)
{
	if(colliding->type == MapObjectType::PLAYER)
	{
		playerptr = static_cast<Character*>(colliding);
		playercollide = true;

		return true;
	}
	else if(colliding->type == MapObjectType::DESTRUCTIBLE)
	{
		obstacleptr = static_cast<Obstacle*>(colliding);
		obstaclecollide = true;

		return true;
	}
	else
		return false;
}

void Unit::OnEndCollision()
{
	if(playercollide)
	{
		playerptr = nullptr;
		playercollide = false;
	}
	else if(obstaclecollide)
	{
		obstacleptr = nullptr;
		obstaclecollide = false;
	}
}

void Unit::Update()
{
	GameplayObject::Update();

	//rotate to target
	b2Vec2 totarget = *target - body->GetPosition();
	float angle = atan2f( -totarget.x, totarget.y );
	body->SetTransform( body->GetPosition(), angle);

	b2Vec2 pos = body->GetWorldCenter();
	b2Vec2 vel = body->GetLinearVelocity();

	if(pos.x > target->x)
	{
		if(vel.x > -statistics.maxv)
			body->ApplyForceToCenter(b2Vec2( -statistics.deltav, 0.0f), true);
	}
	else if(pos.x < target->x)
	{
		if(vel.x < statistics.maxv)
			body->ApplyForceToCenter(b2Vec2(statistics.deltav, 0.0f), true);
	}

	if(pos.y > target->y)
	{
		if(vel.y > -statistics.maxv)
			body->ApplyForceToCenter(b2Vec2(0.0f, -statistics.deltav), true);
	}
	else if(pos.y < target->y)
	{
		if(vel.y < statistics.maxv)
			body->ApplyForceToCenter(b2Vec2(0.0f, statistics.deltav), true);
	}


	if(statistics.currenthealth <= 0)
	{
		Destroy();
		sounds->AddSoundWave("zombie_die", body->GetWorldCenter());
	}

	if(playercollide && playerptr != nullptr && collidetimer.Passed())
	{
		playerptr->DecreaseHealth(statistics.damage);
		std::cout<<"health: " << playerptr->GetHealth() << "\n";

		collidetimer.AddDelta(1000);
	}
	else if(obstaclecollide && obstacleptr != nullptr && collidetimer.Passed())
	{
		obstacleptr->OnHit(statistics.damage);

		collidetimer.AddDelta(1000);
	}

	if(regentimer.Passed())
	{
		IncreaseHealth(statistics.hpregen);
		regentimer.AddDelta(1000);
	}
}

Unit::~Unit()
{

}



//Obstacles
Obstacle::Obstacle(const PhysicObjectData& physicdata, b2Vec2 pos, SoundWaveManager* soundsptr,
				   float angle, b2Vec2 scale, const ObstacleData& data)
	: GameplayObject(physicdata)
{
	//sounds = soundsptr;

	statistics = data;

	Image* newimg = new Image(data.graphicname, IsGui::NONE, 0,0 , 0,0 , statistics.framewidth,statistics.frameheight);
	SetImage(newimg);

	CreateBody(BodyType::STATIC, pos.x, pos.y);

	b2PolygonShape box;
	box.SetAsBox(static_cast<float>(statistics.framewidth) / objdata.boxunit / 2,
				 static_cast<float>(statistics.frameheight) / objdata.boxunit / 2);

	b2FixtureDef fixture;
	fixture.shape = &box;
	fixture.density = 1.0f;
	fixture.friction = 0.5f;
	fixture.restitution= 0.2f;

	fixture.filter.categoryBits = Collide::DESTRUCTIBLE;

	//fixture.filter.maskBits = Collide::UNIT | Collide::BULLET | Collide::PLAYER;

	AddFixture(fixture);

	body->SetTransform( body->GetPosition(), angle);

	body->SetLinearDamping(5.0f);
	body->SetAngularDamping(5.0f);

	body->SetActive(statistics.collide);

	type = MapObjectType::DESTRUCTIBLE;

	if(statistics.lifetime)
		lifetimer.AddDelta(statistics.lifetime);
	//else dmg per second

	Scale(scale.x, scale.y);
}

void Obstacle::OnHit(const Damage& damage)
{
	//sounds->AddSoundWave("fencehit", body->GetWorldCenter());
	float hit = 0;

	//probalby need a global function that will coint this:
	//counthit(dmgtype , armortype) returning int

	/*hit += damage.normal * statistics.armortype->normal;
	hit += damage.pierce * statistics.armortype->pierce;
	hit += damage.siege * statistics.armortype->siege;
	hit += damage.barefist * statistics.armortype->barefist;
	hit += damage.bite * statistics.armortype->bite;
	hit += damage.magic * statistics.armortype->magic;
	hit += damage.chaos * statistics.armortype->chaos;
	*/
	hit = 50;
	//armor stat should decrease dmg somehow by itself
	//hit = hit * statistics.armor / 10;

	statistics.currentendurance -= hit;

	if(statistics.currentendurance > 0)
	{
		float percent = (float)statistics.currentendurance / (float)statistics.maxendurance;
		float step = 1.0f / (statistics.frames);
		int frame;

		if(percent == 1)
			frame = statistics.frames - 1;
		else if(percent > step)
			frame = percent / step ;
		else
			frame = 0;

		int frame_actual = (statistics.frames-1) - frame;
		img->ChangeSource(frame_actual * statistics.framewidth, 0);

		//Wojtek version
		/*
		for (int i = statistics.frames; i >= 1 ; i--)
		{
		    if (statistics.currentendurance <= (statistics.maxendurance/(float)statistics.frames)*i)
		        img->ChangeSource(statistics.framewidth * i, 0);
		}
		*/
	}

	if(statistics.currentendurance <= 0)
	{
		//Destroy() //sometimes crash - not finding virtual function
		Obstacle::Destroy();
	}
}

void Obstacle::OnHit(int value)
{
	statistics.currentendurance -= value;

	if(statistics.currentendurance > 0)
	{
		float percent = statistics.currentendurance / statistics.maxendurance;
		float step = 1.0f / statistics.frames;
		int frame;

		if(percent == 1)
			frame = statistics.frames - 1;
		else if(percent > step)
			frame = percent / step ;
		else
			frame = 0;

		int frame_actual = (statistics.frames-1) - frame;
		img->ChangeSource(frame_actual * statistics.framewidth, 0);

		//Wojtek version
		/*
		for (int i = statistics.frames; i >= 1 ; i--)
		{
		    if (statistics.currentendurance <= (statistics.maxendurance/(float)statistics.frames)*i)
		        img->ChangeSource(statistics.framewidth * i, 0);
		}
		*/
	}

	if(statistics.currentendurance <= 0)
	{
		Obstacle::Destroy();
	}
}

void Obstacle::Destroy()
{
	todestroy = true;
	//sounds->AddSoundWave("fencedestruct", body->GetWorldCenter());
	for(int i = 0; i < statistics.objects_after_destruct; i++)
	{
		//spawn lesser material parts
	}
}

void Obstacle::Update()
{
	GameplayObject::Update();

	//moving and rotating in case if moveable

	if(statistics.lifetime != 0)
	{
		if (lifetimer.Passed())
			Destroy();
	}

	if(statistics.dmgpersec > 0 && lifetimer.Passed())
	{
		OnHit(statistics.dmgpersec);
		lifetimer.AddDelta(1000);
	}
}

SoundEmitter::SoundEmitter(const PhysicObjectData& physicdata, SoundWaveManager* soundmanager,
						   const std::string& name, int delta, int shift)
	: GameplayObject(physicdata)
{
	type = MapObjectType::SOUNDEMITTER;

	sounds = soundmanager;
	soundname = name;
	deltatime = delta;

	timer = SDL_GetTicks() + shift;
	enablesounds = true;

	destroyable = false;
}

void SoundEmitter::EnableSounds(bool value)
{
	enablesounds = value;
}


void SoundEmitter::Update()
{
	if(enablesounds)
	{
		GameplayObject::Update();
		if( timer < SDL_GetTicks())
		{
			sounds->AddSoundWave(soundname, GetCenterPhysicPos());
			timer = SDL_GetTicks() + deltatime;
		}
	}
}








