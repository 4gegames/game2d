#ifndef SETTINGS_HPP_INCLUDED
#define SETTINGS_HPP_INCLUDED

#include<string>
#include<vector>
#include<fstream>
#include<ctime>

#include<GL/glew.h>
#include<GL/glu.h>

#include<SDL2/SDL.h>
#include<SDL2/SDL_mixer.h>
#include<SDL2/SDL_opengl.h>
#include<SDL2/SDL_ttf.h>
#include<SDL2/SDL_image.h>
#include<SDL2/SDL_net.h>
#include<SDL2/SDL_revision.h>

#include "Utils/Log.hpp"


enum class ScreenRatio
{
	ratio5x4,
	ratio16x10,
	ratio4x3,
	ratio16x9
};

enum class RendererType
{
	OGL2,
	OGL2Shader,
	OGL3,
	OGLES
};

struct Options
{
	float default_resolution_x, default_resolution_y;
	float resolution_x, resolution_y;
	SDL_Rect screenrect;

	double ratio_x, ratio_y;
	short int shift;
	ScreenRatio resRatio;

	bool vsync;
	bool first_run;

	bool fullscreen;
	short int multisampling;

	short int volumeMusic;
	short int volumeSound;

	std::string language;
	std::string userPath;

	RendererType renderer;

	int velocityIterations;
	int positionIterations;
};


class Settings
{
		SDL_Surface* mini;
		void LoadSettings();
		void ComputeResolution();
		void SaveRes(); // helper to ComputeResolution()
		void InitSDL();
		void WindowCreate();
		void InitOpenGL();

	public:
		Options options;
		std::vector<std::string> sets;

		SDL_Window* window;

		Settings();
		~Settings();

		void SaveSettings();
};

#endif // SETTINGS_HPP_INCLUDED
