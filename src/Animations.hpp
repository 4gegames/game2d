#ifndef ANIMATIONS_HPP_INCLUDED
#define ANIMATIONS_HPP_INCLUDED

#include "Images.hpp"
#include "GameTimer.hpp"

class AnimationSystem
{
		int base; //base animation, default first line with index 0

		unsigned int active_number; // number of active animation line
		bool active;
		unsigned int actualframe;

		bool looped;
		unsigned int startframe;
		unsigned int numberframes;

		Image* img;
		GameTimer timer;
		unsigned int width, height;
		unsigned int delay; //time to next frame

		std::vector<std::pair<int, int> > animlines;
		unsigned int maxframe;
	public:
		void Play(unsigned int number, bool looped, int pstartframe=0, int pnumberframes =0, bool forced=false);
		void SetBaseAnimation(int number); //default looped animation
		void SetNumberFrames(int line, int number);
		void Update();
		void PauseAnimations();
		int GetActiveAnimation();

		AnimationSystem(Image* img, int pdelay, int frame_w, int frame_h, int graphic_w, int graphic_h);
		~AnimationSystem() {}
};


#endif
