#ifndef CONTROLLERS_HPP_INCLUDED
#define CONTROLLERS_HPP_INCLUDED

#include<fstream>
#include<vector>
#include<map>
#include<cassert>

#include<SDL2/SDL.h>

#include "Utils/Log.hpp"

enum class KeyType
{
	MOUSE,
	KEYBOARD,
	MOUSEWHEEL,
	JOYSTICK
};

enum class MouseState
{
	NOTPRESSED,
	BUTTONDOWN,
	BUTTONUP
};

struct Key
{
	KeyType type;
	SDL_Scancode code;
	int mousebutton; //
	int mousewheel;
	Key(KeyType ptype, int value);
};

class Controllers
{
		int key_size;

		void LoadKeyboardControls();

	public:
		std::map<std::string, Key*> keys;

		std::vector<std::pair<std::string, int> > keynames;

		Uint8* keystate;
		bool mousekey[8];
		// Left mouse key - 0, middle - 1, right - 2
		int mouse_x, mouse_y;
		MouseState lmb_state; // maybe should add state variable to other mouse buttons,
		// only if they will need to buttons
		int click_x, click_y;

		Controllers();
		~Controllers();

		bool wheelstate[2];
		unsigned int wheel_timer;

		void Reset();
		void Update();
		void HandleEvent(SDL_Event& event);

		Key* GetKey(const std::string& name);

		bool Check(Key* k);

		bool Check(const std::string& name);
		bool Check(const std::string& name, const std::string& name2);
		//to debug, it's can log all different calls in container
		bool Check(const std::string& name, const std::string& name2,
				   const std::string& name3);

		bool TestClickInArea(SDL_Rect area); //area must be in screen coordinates
};

#endif // CONTROLLERS_HPP_INCLUDED
