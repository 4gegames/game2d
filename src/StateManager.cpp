#include "StateManager.hpp"

StateManager::StateManager()
{
	exit=false;
	restart=false;
	InitLog();

	Log("\n___________________________________GAME LOG_____________________________________\n");
	Log("Platform: ",SDL_GetPlatform(),"\n");
	Log("Processor parameters:\n");
	Log("Logical cores: ",SDL_GetCPUCount(),"\n");
	Log("L1 CPU cache: ",SDL_GetCPUCacheLineSize(),"\n");

	Log("CPU extensions: ", SDL_Has3DNow, " ", SDL_HasAltiVec, " ",
		SDL_HasMMX, " ", SDL_HasRDTSC, " ", SDL_HasSSE, " ", SDL_HasSSE2,
		" ", SDL_HasSSE3, " ", SDL_HasSSE41, " ", SDL_HasSSE42,"\n");

	Init();
}

void StateManager::Init()
{
	Game::Init();

	PushState(States::INITSTATE);
	PushState(States::INTRO);
	exit = false;
	restart = false;
}

void StateManager::PushState(States newstate)
{
	State* tempState;
	switch(newstate)
	{
		case States::INITSTATE:
			tempState=new InitState;
			break;
		case States::INTRO:
			tempState=new Intro;
			break;
		case States::MAINMENU:
			tempState=new MainMenu;
			break;
		case States::SINGLEPLAYERMENU:
			tempState=new SinglePlayerMenu;
			break;
		case States::MULTIPLAYER:
			tempState=new MultiPlayer;
			break;
		case States::SETTINGSMENU:
			tempState=new SettingsMenu;
			break;
		case States::SETTINGSGRAPHICS:
			tempState=new SettingsGraphics;
			break;
		case States::SETTINGSSOUNDS:
			tempState=new SettingsSounds;
			break;
		case States::SETTINGSGAMEPLAY:
			tempState=new SettingsGameplay;
			break;
		case States::SETTINGSCONTROL:
			tempState=new SettingsControl;
			break;
		case States::SETTINGSLANGUAGE:
			tempState=new SettingsLanguage;
			break;
		case States::CREDITS:
			tempState=new Credits;
			break;
		case States::LOADSAVE:
			tempState=new LoadSave;
			break;
		case States::SAVEERROR:
			tempState=new SaveError;
			break;
		case States::DEFEATMENU:
			tempState=new DefeatMenu;
			break;
		case States::PAUSEMENU:
			tempState=new PauseMenu;
			break;
		case States::SPOOKYGAMEPLAY:
			tempState=new SpookyGameplay;
			break;
		case States::MINIMIZEPAUSE:
			tempState=new MinimizePause;
		case States::END:
			break;
		default:
			tempState=new Intro;
			Log("Bad PushState parameter\n");
	}
	stateStack.push_back(tempState);
}

void StateManager::PopState()
{
	delete stateStack.back();
	stateStack.pop_back();
}

void StateManager::ChangeState()
{
	States s1;
	switch(stateStack.back()->action)
	{
		case ChangeStates::RESTART:
			restart=true;
			break;
		case ChangeStates::PUSH:
			PushState(stateStack.back()->nextstate);
			break;
		case ChangeStates::POP:
			PopState();
			break;
		case ChangeStates::POPUNTIL:
			s1= stateStack.back()->nextstate;

			while(!stateStack.empty() && s1!= stateStack.back()->currentstate)
			{
				PopState();
			}
			break;
		case ChangeStates::POPANDPUSH:
			s1= stateStack.back()->nextstate;
			PopState();
			PushState(s1);
			break;
		case ChangeStates::NONE:
		default:
			break;
	}
	if(!stateStack.empty())
		stateStack.back()->action=ChangeStates::NONE;
	else
		exit=true;
}

void StateManager::InputState()
{
	if((Game::ctrl->keystate[SDL_SCANCODE_F4]&&Game::ctrl->keystate[SDL_SCANCODE_LALT])
			|| Game::ctrl->Check("forceexit"))
	{
		stateStack.back()->action = ChangeStates::POPUNTIL;
		stateStack.back()->nextstate = States::END;
		exit=true;
	}

	static unsigned int timer = SDL_GetTicks();
	if(Game::ctrl->keystate[SDL_SCANCODE_F11] && timer<SDL_GetTicks())
	{
		Game::resources->LogTextures();
		timer = SDL_GetTicks()+2000;
	}

	if(Game::ctrl->keystate[SDL_SCANCODE_BACKSPACE])
	{
		stateStack.back()->action = ChangeStates::POP;
	}

	while(SDL_PollEvent(&event)) // event loop
	{
		if(event.type == SDL_QUIT)
		{
			stateStack.back()->action = ChangeStates::POPUNTIL;
			stateStack.back()->nextstate = States::END;
			exit = true;
		}
		else if(event.type == SDL_WINDOWEVENT)
		{
			switch(event.window.event)
			{
				case SDL_WINDOWEVENT_MINIMIZED:
					stateStack.back()->action = ChangeStates::PUSH;
					stateStack.back()->nextstate = States::MINIMIZEPAUSE;
					break;

				case SDL_WINDOWEVENT_RESTORED:
					//Window restored
					break;

				case SDL_WINDOWEVENT_RESIZED:
					//Window resized
					break;
			}
		}

		Game::ctrl->HandleEvent(event);
	}

	Game::ctrl->Update();
}

void StateManager::RenderState()
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if(stateStack.back()->translucent == true)
	{
		if(stateStack.size() >= 2)
		{
			stateStack[stateStack.size()-2]->Render();
		}
	}

	stateStack.back()->Render();

	SDL_GL_SwapWindow(Game::settings->window);
}

void StateManager::Update()
{
	InputState();
	while(Game::fps->accumulator > Game::fps->TIME_STEP)
	{
		stateStack.back()->Update(Game::fps->TIME_STEP);
		Game::fps->accumulator -= Game::fps->TIME_STEP;
	}

	if(Game::fps->accumulator <= 0)
	{
		Game::fps->accumulator = 0.0f;
	}
	RenderState();

	if(stateStack.back()->action != ChangeStates::NONE)
	{
		ChangeState(); // if loop get lagged, then execute some logical loop iterations, but it's not a problem
	}

	Game::fps->CountFPS();

	if(restart)
	{
		while(!stateStack.empty())
		{
			PopState();
		}
		Log("stack size: ", stateStack.size(),"\n");
		Log("************************RESTART************************\n");
		Game::Clean();

		Init();
		PushState(States::MAINMENU);
		PushState(States::SETTINGSMENU);
	}
}

StateManager::~StateManager()
{
	Log("**********FINAL CLEANUP***********\n");
	Game::Clean();
	CloseLog();
}
