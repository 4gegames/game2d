#ifndef STATEMANAGER_HPP_INCLUDED
#define STATEMANAGER_HPP_INCLUDED

#include "States/State.hpp"
#include "States/MainStates.hpp"
#include "States/SinglePlayer.hpp"
#include "States/MultiPlayer.hpp"
#include "Spooky/Spooky.hpp"


class StateManager
{
		SDL_Event event;
		std::deque <State*> stateStack;
		void InputState();
		void RenderState();
		void PushState(States newstate); //add
		void PopState(); //remove
		void ChangeState();
	public:
		bool exit, restart;
		StateManager();
		~StateManager();
		void Init();
		void Update();
};

#endif // STATEMANAGER_HPP_INCLUDED
