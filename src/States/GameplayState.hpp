#ifndef GameplayState_HPP_INCLUDED
#define GameplayState_HPP_INCLUDED

#include <list>
#include<Box2D/Box2D.h>

#include "State.hpp"
#include "../GameplayObjects/GameplayObject.hpp"


class GameplayState : public State
{
	protected:
		std::vector<Panel*> panels;

		std::list<GameplayObject*> gameplayobjects;
		Camera* cam_gameplay; //use to gameplay

		b2World* world;
		b2Vec2* gravity;

		PhysicObjectData objdata;

		bool testingrender;


		void SetGameplayCameraType(Perspective type);

		void AddObject(GameplayObject* obj);

		void InitPhysicWorld(float xgravity, float ygravity, float boxunit);
		void DeletePhysicWorld();

		GameplayObject* AddTestBody(float posx, float posy);
		GameplayObject* AddTestCircle(float posx, float posy);
		GameplayObject* AddStaticRect(float posx, float posy, float width, float height); //left-up corner position

		//Get mouse position in different scales
		glm::vec2 GetMouseGlPos();
		b2Vec2 GetMousePhysicPos();

		b2Vec2 ConvertToPhysicPos(const glm::vec2& v);
		glm::vec2 ConvertToGlPos(const b2Vec2& v);
		glm::vec2 ConvertToScreenPos(const b2Vec2& v);

		glm::vec2 ConvertScreenToGlPos(const glm::vec2& v);
		b2Vec2 ConvertScreenToPhysicPos(const glm::vec2& v);

		void DeleteAllObjects();

	public:
		GameplayState();
		GameplayState(float xgravity, float ygravity, float boxunit=50.0f);
		virtual void DebugRender();

		void Render();
		virtual void Update(float time_step);
		virtual ~GameplayState();
};




#endif // GameplayState_HPP_INCLUDED
