#include "MainStates.hpp"

InitState::InitState()
{
	currentstate = States::INITSTATE;

	Log("\nAdd Game\n");

	SDL_SetWindowTitle(Game::settings->window, Game::resources->GetTextLine("main_title").c_str() );

	SDL_WarpMouseInWindow(Game::settings->window, Game::settings->options.resolution_x / 2,
						  Game::settings->options.resolution_y / 2);
}

InitState::~InitState()
{
	Log("  ~Game()\n");
}

///////////////////////////////////////////////////////////////////////////////

Intro::Intro()
{
	currentstate= States::INTRO;
	Log("\nAdd Intro\n");
	CreateBackground("b_intro", "Data/Images/b_intro.png");

}

void Intro::GoToNextState()
{
	action=ChangeStates::POPANDPUSH;
	nextstate= States::MAINMENU;
}

void Intro::Update(float time_step)
{
	State::Update(time_step);
	if(Game::ctrl->keystate[SDL_SCANCODE_SPACE] || Game::ctrl->Check("exit")
			|| Game::ctrl->keystate[SDL_SCANCODE_RETURN] || Game::ctrl->mousekey[0])
	{
		GoToNextState();
	}

	static unsigned int timer = SDL_GetTicks() + 3000;

	if(timer < SDL_GetTicks())
		GoToNextState();
}

Intro::~Intro()
{
	Log("  ~Intro()\n");
}

///////////////////////////////////////////////////////////////////////////////

MainMenu::MainMenu()
{
	currentstate = States::MAINMENU;
	CreateCursor("cursor");

	Log("Add MainMenu()\n");

	Game::audio->AddMusic("muzyka1","Data/Music/muzyka1.ogg");
	Game::audio->AddMusic("muzyka2","Data/Music/muzyka2.ogg");

	if(Game::settings->options.volumeMusic)
		Game::audio->PlayMusic("muzyka2");

	CreateBackground("b_menu", "Data/Images/b_menu.png");


	LoadText("main_title", "title", Game::resources->colors.red);
	GuiObject* obj = new GuiObject("main_title", 0, 50);
	obj->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(obj);


	LoadText("main_single_player", "normal", Game::resources->colors.black);
	Button* b = new Button("mainmenu", "main_single_player", "menu1", 0, 425);
	b->Align(Game::screenrect, AlignmentX::CENTER);
	b->click = std::bind(&MainMenu::GoSinglePlayer, this);
	AddGuiObject(b);


	LoadText("main_multi_player", "normal", Game::resources->colors.black);
	b = new Button("mainmenu", "main_multi_player", "menu2", 0, 550);
	b->Align(Game::screenrect, AlignmentX::CENTER);
	b->click = std::bind(&MainMenu::GoMultiPlayer, this);
	AddGuiObject(b);

	LoadText("main_settings", "normal", Game::resources->colors.black);
	b = new Button("mainmenu", "main_settings", "menu3", 0, 675);
	b->Align(Game::screenrect, AlignmentX::CENTER);
	b->click = std::bind(&MainMenu::GoSettings, this);
	AddGuiObject(b);


	LoadText("main_credits", "normal", Game::resources->colors.black);
	b = new Button("mainmenu", "main_credits", "menu4", 0, 800);
	b->Align(Game::screenrect, AlignmentX::CENTER);
	b->click = std::bind(&MainMenu::GoCredits, this);
	AddGuiObject(b);

	LoadText("main_exit", "normal", Game::resources->colors.black);
	b = new Button("mainmenu", "main_exit", "menu5", 0, 925);
	b->Align(Game::screenrect, AlignmentX::CENTER);
	b->click = std::bind(&MainMenu::Exit, this);
	AddGuiObject(b);
}

void MainMenu::GoSinglePlayer()
{
	action = ChangeStates::PUSH;
	nextstate = States::SINGLEPLAYERMENU;
}

void MainMenu::GoMultiPlayer()
{
	/*	action=ChangeStatesPUSH;
	nextstate= StatesMULTIPLAYER; */
}

void MainMenu::GoSettings()
{
	action = ChangeStates::PUSH;
	nextstate = States::SETTINGSMENU;
}

void MainMenu::GoCredits()
{
	action = ChangeStates::PUSH;
	nextstate = States::CREDITS;
}

void MainMenu::GoSpooky()
{
	action = ChangeStates::PUSH;
	nextstate = States::SPOOKYGAMEPLAY;
}

void MainMenu::Exit()
{
	action = ChangeStates::POPUNTIL;
	nextstate = States::END;
}

void MainMenu::Update(float time_step)
{
	State::Update(time_step);

	if(Game::ctrl->Check("menu9"))
		GoSpooky();
}

MainMenu::~MainMenu()
{
	Log("\n~Mainmenu()");
}

///////////////////////////////////////////////////////////////////////////////

SettingsMenu::SettingsMenu()
{
	Menu();
}

void SettingsMenu::Menu()
{
	CreateCursor("cursor");
	CreateBackground("b_background", "Data/Images/b_background.png");

	LoadText("settings_header", "head", Game::resources->colors.black);
	GuiObject* obj = new GuiObject("settings_header", 0, 75);
	obj->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(obj);

	LoadText("settings_graphics", "normal", Game::resources->colors.black);
	Button* b = new Button("setbutton", "settings_graphics", "menu1", 10, 500);
	b->click = std::bind(&SettingsMenu::Graphics, this);
	AddGuiObject(b);

	LoadText("settings_sounds", "normal", Game::resources->colors.black);
	b = new Button("setbutton", "settings_sounds", "menu2", 10, 600);
	b->click = std::bind(&SettingsMenu::Sounds, this);
	AddGuiObject(b);

	LoadText("settings_gameplay", "normal", Game::resources->colors.black);
	b = new Button("setbutton", "settings_gameplay", "menu3", 10, 700);
	b->click = std::bind(&SettingsMenu::Gameplay, this);
	AddGuiObject(b);

	LoadText("settings_control", "normal", Game::resources->colors.black);
	b = new Button("setbutton", "settings_control", "menu4", 10, 800);
	b->click = std::bind(&SettingsMenu::Control, this);
	AddGuiObject(b);

	LoadText("settings_language", "normal", Game::resources->colors.black);
	b = new Button("setbutton", "settings_language", "menu5", 10, 900);
	b->click = std::bind(&SettingsMenu::Language, this);
	AddGuiObject(b);

	LoadText("cancel", "normal", Game::resources->colors.black);
	b = new Button("return1", "cancel", "exit", 1000, 900);
	b->click = std::bind(&SettingsMenu::Return, this);
	AddGuiObject(b);
}

void SettingsMenu::Graphics()
{
	action = ChangeStates::POPANDPUSH;
	nextstate = States::SETTINGSGRAPHICS;
}

void SettingsMenu::Sounds()
{
	action = ChangeStates::POPANDPUSH;
	nextstate = States::SETTINGSSOUNDS;
}

void SettingsMenu::Gameplay()
{
	action = ChangeStates::POPANDPUSH;
	nextstate = States::SETTINGSGAMEPLAY;
}

void SettingsMenu::Control()
{
	action = ChangeStates::POPANDPUSH;
	nextstate = States::SETTINGSCONTROL;
}

void SettingsMenu::Language()
{
	action = ChangeStates::POPANDPUSH;
	nextstate = States::SETTINGSLANGUAGE;
}

SettingsMenu::~SettingsMenu()
{

}

SettingsGraphics::SettingsGraphics()
{
	currentstate = States::SETTINGSGRAPHICS;

	int resx = Game::settings->options.resolution_x;
	int resy = Game::settings->options.resolution_y;

	std::fstream resfile;
	std::string x, y;
	resfile.open("Data/resolutions.csv", std::ios::in);
	while(resfile.good())
	{
		getline(resfile, x, ';');
		getline(resfile, y,'\n');
		if(x != "" && y != "")
		{
			std::pair<int, int> resolution = std::make_pair(std::stoi(x),
											 std::stoi(y));
			resolutions.push_back(resolution);
		}
	}
	resfile.close();

	res_amount = resolutions.size();

	//find id of actual resolution
	res_id = 0;
	for(; res_id < res_amount; res_id++)
	{
		if(resolutions[res_id].first == resx)
		{
			while(resolutions[res_id].second != resy)
			{
				res_id++;
			}
			break;
		}
	}

	//load all resolution text lines
	for(int i = 0; i < 15; i++)
	{
		std::string name = "res" + std::to_string(i);
		LoadText(name, "normal", Game::resources->colors.black);
	}

	selection = new GuiObject("setbuttonhov", 10, 500, GuiDepth::ABOVEBUTTON);
	AddGuiObject(selection);

	panel = new GuiObject("setpos", 0, 215, GuiDepth::PANEL);
	panel->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(panel);

	LoadText("settings_select_resolution", "normal", Game::resources->colors.black);
	GuiObject* txt = new GuiObject("settings_select_resolution");
	txt->Align(panel->GetPosition(), AlignmentX::CENTER, AlignmentY::UP);
	AddGuiObject(txt);

	std::string name = "res" + std::to_string(res_id);
	res = new GuiObject(name);
	res->Align(panel->GetPosition(), AlignmentX::CENTER, AlignmentY::DOWN);
	AddGuiObject(res);

	Button* arrowl = new Button("setarrowleft");
	arrowl->Align(panel->GetPosition(), AlignmentX::LEFT, AlignmentY::DOWN);
	arrowl->click = std::bind(&SettingsGraphics::Previous, this);
	AddGuiObject(arrowl);

	Button* arrowr = new Button("setarrowright");
	arrowr->Align(panel->GetPosition(), AlignmentX::RIGHT, AlignmentY::DOWN);
	arrowr->click = std::bind(&SettingsGraphics::Next, this);
	AddGuiObject(arrowr);

	LoadText("save", "normal", Game::resources->colors.black);
	Button* b = new Button("return1", "save", "", 1000, 780);
	b->click = std::bind(&SettingsGraphics::Restart, this);
	AddGuiObject(b);

///////////////////////////////////////////////////////////////////////////////

	fullscreen=Game::settings->options.fullscreen;

	GuiObject* panel2 = new GuiObject("setpos", 0, 390, GuiDepth::PANEL);
	panel2->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(panel2);

	LoadText("settings_fullscreen", "normal", Game::resources->colors.black);
	GuiObject* txt2 = new GuiObject("settings_fullscreen", TEXT);
	txt2->Align(panel2->GetPosition(), AlignmentX::CENTER, AlignmentY::CENTER);
	AddGuiObject(txt2);

	Button* bfullscreen = new Button("checkbox1", &fullscreen);
	bfullscreen->Align(panel2->GetPosition(), AlignmentX::LEFT, AlignmentY::CENTER);
	AddGuiObject(bfullscreen);

///////////////////////////////////////////////////////////////////////////////

	multisampling=Game::settings->options.multisampling;

	GuiObject* panel3 = new GuiObject("setpos", 0, 565, GuiDepth::PANEL);
	panel3->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(panel3);

	LoadText("settings_aliasing", "normal", Game::resources->colors.black);
	GuiObject* txt3 = new GuiObject("settings_aliasing");
	txt3->Align(panel3->GetPosition(), AlignmentX::CENTER, AlignmentY::UP);
	AddGuiObject(txt3);

	GuiObject* ms = new GuiObject("multisampling");
	ms->Align(panel3->GetPosition(), AlignmentX::CENTER, AlignmentY::DOWN);

	//TO DO: make button with multisampling values

///////////////////////////////////////////////////////////////////////////////

	vsync = Game::settings->options.vsync;

	GuiObject* panel4 = new GuiObject("setpos", 0, 740, GuiDepth::PANEL);
	panel4->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(panel4);

	LoadText("settings_vsync", "normal", Game::resources->colors.black);
	GuiObject* txt4 = new GuiObject("settings_vsync");
	txt4->Align(panel4->GetPosition(), AlignmentX::RIGHT, AlignmentY::CENTER);
	AddGuiObject(txt4);

	Button* bvsync = new Button("checkbox1", &vsync);
	bvsync->Align(panel4->GetPosition(), AlignmentX::LEFT, AlignmentY::CENTER);
	AddGuiObject(bvsync);

///////////////////////////////////////////////////////////////////////////////

	firstrun=Game::settings->options.first_run;

	GuiObject* panel5 = new GuiObject("setpos", 0, 915, GuiDepth::PANEL);
	panel5->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(panel5);

	LoadText("settings_reset", "normal", Game::resources->colors.black);
	GuiObject* txt5 = new GuiObject("settings_reset");
	txt5->Align(panel5->GetPosition(), AlignmentX::CENTER, AlignmentY::CENTER);
	AddGuiObject(txt5);

	Button* breset = new Button("checkbox1", &firstrun);
	breset->Align(panel5->GetPosition(), AlignmentX::LEFT, AlignmentY::CENTER);
	AddGuiObject(breset);
}


void SettingsGraphics::Previous()
{
	if(res_id > 0)
		res_id--;
	else
		res_id = res_amount - 1; //index from 0
	std::string name = "res" + std::to_string(res_id);

	res->Destroy();
	res = new GuiObject(name);
	res->Align(panel->GetPosition(), AlignmentX::CENTER, AlignmentY::DOWN);
	AddGuiObject(res);
}

void SettingsGraphics::Next()
{
	if(res_id < res_amount - 1)
		res_id++;
	else
		res_id = 0;

	std::string name = "res" + std::to_string(res_id);

	res->Destroy();
	res = new GuiObject(name);
	res->Align(panel->GetPosition(), AlignmentX::CENTER, AlignmentY::DOWN);
	AddGuiObject(res);
}

void SettingsGraphics::Restart()
{
	Game::settings->sets[10] = std::to_string(resolutions[res_id].first);
	Game::settings->sets[11] = std::to_string(resolutions[res_id].second);

	Game::settings->sets[13] = std::to_string(static_cast<int>(fullscreen));
	Game::settings->sets[15] = std::to_string(multisampling);
	Game::settings->sets[17] = std::to_string(vsync);
	Game::settings->sets[8]  = std::to_string(firstrun);

	Game::settings->SaveSettings();

	action = ChangeStates::RESTART;
}

SettingsGraphics::~SettingsGraphics()
{

}

SettingsSounds::SettingsSounds()
{
	currentstate = States::SETTINGSSOUNDS;

	selection = new GuiObject("setbuttonhov", 10, 600, GuiDepth::ABOVEBUTTON);
	AddGuiObject(selection);

	/*
	img=LoadGuiImage("setpos",1,1,0,600);
	img->Align(Game::screenrect, AlignmentX::CENTER);

	txt=LoadGuiImage("88",1,3);
	txt->Align(img->position, AlignmentX::CENTER, AlignmentY::UP);

	int i=0;
	AddSlider(&i, img, AlignmentX::CENTER, AlignmentY::DOWN);
	*/
}

SettingsSounds::~SettingsSounds()
{
	//if are changes should show dialog window witch options save/cancel
}

SettingsGameplay::SettingsGameplay()
{
	currentstate = States::SETTINGSGAMEPLAY;

	selection = new GuiObject("setbuttonhov", 10, 700, GuiDepth::ABOVEBUTTON);
	AddGuiObject(selection);

}

SettingsGameplay::~SettingsGameplay()
{

}

SettingsControl::SettingsControl()
{
	currentstate = States::SETTINGSCONTROL;

	selection = new GuiObject("setbuttonhov", 10, 800, GuiDepth::ABOVEBUTTON);
	AddGuiObject(selection);


}

SettingsControl::~SettingsControl()
{

}

SettingsLanguage::SettingsLanguage()
{
	currentstate = States::SETTINGSLANGUAGE;

	selection = new GuiObject("setbuttonhov", 10, 900, GuiDepth::ABOVEBUTTON);
	AddGuiObject(selection);

	language = Game::settings->options.language;


	LoadText("settings_language_head", "smallhead", Game::resources->colors.black);
	GuiObject* txt = new GuiObject("settings_language_head", 0, 300);
	txt->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(txt);

	GuiObject* panel = new GuiObject("setpos", 0, 450, GuiDepth::PANEL);
	panel->GetImage()->SetDisplay(false);
	panel->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(panel);

	b1 = new Button("languageEN");
	b1->Align(panel->GetPosition(), AlignmentX::LEFT, AlignmentY::CENTER);
	b1->click = std::bind(&SettingsLanguage::SetEN, this);
	AddGuiObject(b1);

	b2 = new Button("languagePL");
	b2->Align(panel->GetPosition(), AlignmentX::CENTER, AlignmentY::CENTER);
	b2->click = std::bind(&SettingsLanguage::SetPL, this);
	AddGuiObject(b2);

	b3 = new Button("languageRU");
	b3->Align(panel->GetPosition(), AlignmentX::RIGHT, AlignmentY::CENTER);
	b3->click = std::bind(&SettingsLanguage::SetRU, this);
	AddGuiObject(b3);


	LoadText("save", "normal", Game::resources->colors.black);
	Button* b = new Button("return1", "save", "", 1000, 780);
	b->click = std::bind(&SettingsLanguage::Restart, this);
	AddGuiObject(b);

	selected = new GuiObject("languageset");
	AddGuiObject(selected);

	if(language == "PL")
		SetPL();
	else if(language == "RU")
		SetRU();
	else
		SetEN();
}

void SettingsLanguage::Restart()
{
	Game::settings->sets[23] = language.c_str();
	Game::settings->options.language=language;
	Game::settings->SaveSettings();

	action = ChangeStates::RESTART;
}

void SettingsLanguage::SetEN()
{
	language = "EN";
	selected->GetImage()->position = b1->GetHoverImage()->position;
}

void SettingsLanguage::SetPL()
{
	language = "PL";
	selected->GetImage()->position = b2->GetHoverImage()->position;
}

void SettingsLanguage::SetRU()
{
	language = "RU";
	selected->GetImage()->position = b3->GetHoverImage()->position;
}

SettingsLanguage::~SettingsLanguage()
{

}

Credits::Credits()
{
	currentstate = States::CREDITS;
	CreateBackground("b_credits", "Data/Images/b_credits.png");
	CreateCursor("cursor");


	GuiObject* logo = new GuiObject("logo", 0, 50);
	logo->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(logo);

	LoadText("author1", "normal", Game::resources->colors.black);
	GuiObject* obj = new GuiObject("author1", 0, 325);
	obj->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(obj);

	LoadText("author2", "normal", Game::resources->colors.black);
	obj = new GuiObject("author2", 0, 400);
	obj->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(obj);

	LoadText("author3", "normal", Game::resources->colors.black);
	obj = new GuiObject("author3", 0, 475);
	obj->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(obj);

	LoadText("return","normal",Game::resources->colors.black);
	Button* b = new Button("return1", "return", "exit", 1000, 900);
	b->click = std::bind(&Credits::Return, this);
	AddGuiObject(b);
}

Credits::~Credits()
{

}

MinimizePause::MinimizePause()
{
	currentstate = States::MINIMIZEPAUSE;
	CreateBackground("b_minimizepause", "Data/Images/b_minimizepause.png");
	translucent = true;

	LoadText("minimize_pause", "normal", Game::resources->colors.red);
	GuiObject* obj = new GuiObject("minimize_pause");
	obj->Align(Game::screenrect, AlignmentX::CENTER, AlignmentY::CENTER);
	AddGuiObject(obj);
}

void MinimizePause::Update(float time_step)
{
	State::Update(time_step);
	if(Game::ctrl->keystate[SDL_SCANCODE_SPACE] || Game::ctrl->Check("exit")
			|| Game::ctrl->keystate[SDL_SCANCODE_RETURN] || Game::ctrl->mousekey[0])
	{
		action = ChangeStates::POP;
	}
}


MinimizePause::~MinimizePause()
{

}
