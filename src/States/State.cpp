#include "State.hpp"


State::State()
{
	translucent = false;
	background = nullptr;
	cursor = nullptr;

	currentstate = States::END;
	action = ChangeStates::NONE;
	nextstate = States::END;

	cam_gui = new Camera(Game::settings->options.resolution_x, Game::settings->options.resolution_y);

	Game::ctrl->Reset();
}

void State::CreateBackground(const std::string& name, const char* path1, const char* path2)
{
	int a, b, c;

	if(path2 != nullptr)
	{
		if(Game::settings->options.resolution_x / Game::settings->options.resolution_y >= 1.2
			&&Game::settings->options.resolution_x / Game::settings->options.resolution_y <= 1.4)  //4:3 i 5:4
		{
			Game::resources->LoadTexture(name, path1);
		}
		else if(Game::settings->options.resolution_x / Game::settings->options.resolution_y >= 1.55) // 16:9 i 16:10
		{
			Game::resources->LoadTexture(name, path2);
		}

		a = 43;
		b = 93;
		c = 0;
	}
	else
	{
		Game::resources->LoadTexture(name, path1);
		a = 277;
		b = 93;
		c = 233;
	}

	SDL_Rect src;
	switch (Game::settings->options.resRatio)
	{
		case ScreenRatio::ratio5x4: //5:4
			src.x = a;
			break;
		case ScreenRatio::ratio16x10: //16:10
			src.x = b;
			break;
		case ScreenRatio::ratio4x3: //4:3
			src.x = c;
			break;
		case ScreenRatio::ratio16x9: // 16:9
			src.x = 0;
			break;
	}
	src.y = 0;
	src.w = Game::settings->options.resolution_x / Game::settings->options.ratio_x;
	src.h = Game::settings->options.resolution_y / Game::settings->options.ratio_x;

	Image* img = new Image(name, IsGui::GUI, 0, 0, src.x, src.y, src.w, src.h);

	background = new GuiObject(img);
}

void State::CreateCursor(const std::string& name)
{
	if(cursor != nullptr)
		cursor->Destroy();

	cursor = new Cursor(name);
	AddGuiObject(cursor);
}


std::string State::LoadText(const std::string& nameid, const char* font, SDL_Color color)
{
	Game::resources->LoadTextTexture(nameid, font, color);
	return nameid;
}

void State::AddGuiObject(GuiObject* obj)
{
	if(!guiobjects.empty())
	{
		int depth = obj->depth;
		auto it = guiobjects.begin();
		while(it != guiobjects.end() && ( (*it)->depth) < depth)
		{
			++it;
		}
		guiobjects.insert(it, obj);
	}
	else
		guiobjects.push_back(obj);
}


void State::BackgroundRender()
{
	if(background != nullptr)
	{
		cam_gui->Active();
		background->Render(cam_gui);
	}
}

//public methods
void State::Update(float time_step)
{
	for(auto it = guiobjects.begin(); it != guiobjects.end(); ++it)
	{
		(*it)->Update();

		if( (*it)->todestroy)
		{
			delete (*it);
			it = guiobjects.erase(it);
		}
	}
}

void State::Render()
{
	BackgroundRender();

	cam_gui->Active();
	for(auto it = guiobjects.begin(); it != guiobjects.end(); ++it)
	{
		(*it)->Render(cam_gui);
	}
}

void State::Return()
{
	action=ChangeStates::POP;
}

State::~State()
{
	Game::resources->CleanTextureCache();

	for(auto it = guiobjects.begin(); it != guiobjects.end(); ++it)
	{
		delete (*it);
	}

	guiobjects.clear();

	Game::ctrl->Reset();
}

