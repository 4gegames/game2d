#include "SinglePlayer.hpp"


SinglePlayerMenu::SinglePlayerMenu()
{
	currentstate = States::SINGLEPLAYERMENU;
	CreateCursor("cursor");

	CreateBackground("b_menu", "Data/Images/b_menu.png");

	GuiObject* title = new GuiObject("main_title", 0, 50);
	title->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(title);


	LoadText("singlemenu_new_game", "normal", Game::resources->colors.black);
	Button* b = new Button("mainmenu", "singlemenu_new_game", "menu1", 0, 400);
	b->Align(Game::screenrect, AlignmentX::CENTER);
	b->click = std::bind(&SinglePlayerMenu::GoNewGame, this);
	AddGuiObject(b);

	LoadText("singlemenu_continue", "normal", Game::resources->colors.black);
	b = new Button("mainmenu", "singlemenu_continue", "menu2", 0, 550);
	b->Align(Game::screenrect, AlignmentX::CENTER);
	b->click = std::bind(&SinglePlayerMenu::GoLastSave, this);
	AddGuiObject(b);

	LoadText("singlemenu_load", "normal", Game::resources->colors.black);
	b = new Button("mainmenu", "singlemenu_load", "menu3", 0, 700);
	b->Align(Game::screenrect, AlignmentX::CENTER);
	b->click = std::bind(&SinglePlayerMenu::GoLoadSave, this);
	AddGuiObject(b);

	LoadText("return", "normal", Game::resources->colors.black);
	b = new Button("return1", "return", "exit", 1000, 900);
	b->click = std::bind(&SinglePlayerMenu::Return, this);
	AddGuiObject(b);
}

void SinglePlayerMenu::GoNewGame()
{
	action = ChangeStates::PUSH;
	nextstate = States::SPOOKYGAMEPLAY;
}

void SinglePlayerMenu::GoLastSave()
{
	if(false)
	{
		/*
		action = ChangeStates::PUSH;
		nextstate = States:: ;
		*/
	}
	else
	{
		action = ChangeStates::PUSH;
		nextstate = States::SAVEERROR;
	}
}

void SinglePlayerMenu::GoLoadSave()
{
	if(false) //found some save files
	{
		action = ChangeStates::PUSH;
		nextstate = States::LOADSAVE;
	}
	else
	{
		action = ChangeStates::PUSH;
		nextstate = States::SAVEERROR;
	}
}

SinglePlayerMenu::~SinglePlayerMenu()
{

}


LoadSave::LoadSave()
{
	currentstate = States::LOADSAVE;
	CreateCursor("cursor");
	Log("Load save state, temporary empty\n");
	action = ChangeStates::POP;//andpush;
	//nextstate= saveerror;
}

void LoadSave::Load(std::string name)
{

}

LoadSave::~LoadSave()
{

}

SaveError::SaveError()
{
	currentstate = States::SAVEERROR;
	Log("\nSave Error\n");
	CreateCursor("cursor");
	translucent = true;

	GuiObject* obj = new GuiObject("dialog_small", 0, 200);
	obj->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(obj);

	LoadText("save_error_text", "normal", Game::resources->colors.black);
	GuiObject* txt = new GuiObject("save_error_text", 0,0, GuiDepth::TEXT);
	txt->Align(obj->GetPosition(), AlignmentX::CENTER, AlignmentY::UP);
	txt->Translate(0, 20);
	AddGuiObject(txt);

	LoadText("return", "normal", Game::resources->colors.black);
	Button* b = new Button("mainmenu", "return", "confirm", 0, 500);
	b->Align(obj->GetPosition(), AlignmentX::CENTER, AlignmentY::DOWN);
	b->Translate(0, -20);
	b->click = std::bind(&SaveError::Exit, this);
	AddGuiObject(b);
}

void SaveError::Exit()
{
	action = ChangeStates::POP;
}

SaveError::~SaveError()
{

}
///////////////////////////////////////////////////////////////////////////////
void PauseMenu::Exit()
{
	action = ChangeStates::POPUNTIL;
	nextstate = States::MAINMENU;
}

void PauseMenu::Settings()
{
	action = ChangeStates::PUSH;
	nextstate = States::SETTINGSMENU;
}

PauseMenu::PauseMenu()
{
	currentstate = States::PAUSEMENU;
	translucent = true;

	CreateCursor("cursor");

	GuiObject* obj = new GuiObject("pausemenu", 0, 200);
	obj->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(obj);

	LoadText("pausemenu_head", "head", Game::resources->colors.black);
	GuiObject* txt = new GuiObject("pausemenu_head", 0, 250, GuiDepth::TEXT);
	txt->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(txt);

	LoadText("pausemenu_exit", "normal", Game::resources->colors.black);
	Button* b = new Button("pausemenu", "pausemenu_exit", "menu1", 0, 500);
	b->Align(Game::screenrect, AlignmentX::CENTER);
	b->click = std::bind(&PauseMenu::Exit, this);
	AddGuiObject(b);

	LoadText("pausemenu_settings", "normal", Game::resources->colors.black);
	b = new Button("pausemenu", "pausemenu_settings", "menu2", 0, 625);
	b->Align(Game::screenrect, AlignmentX::CENTER);
	b->click = std::bind(&PauseMenu::Settings, this);
	AddGuiObject(b);

	LoadText("pausemenu_return", "normal", Game::resources->colors.black);
	b = new Button("pausemenu", "pausemenu_return", "menu3", 0, 750);
	b->Align(Game::screenrect, AlignmentX::CENTER);
	b->click = std::bind(&PauseMenu::Return, this);
	AddGuiObject(b);
}

void PauseMenu::Update(float time_step)
{
	State::Update(time_step);
	if(Game::ctrl->Check("exit"))
	{
		Return();
	}
}

PauseMenu::~PauseMenu()
{
	//Game::Audio->PlayMusic("muzyka1");
}

////////////////////////////////////////////////////////////////////////

DefeatMenu::DefeatMenu()
{
	currentstate = States::DEFEATMENU;
	Log("\nDefeat!\n");
	CreateCursor("cursor");
	translucent=true;

	GuiObject* obj = new GuiObject("dialog_small", 0, 200);
	obj->Align(Game::screenrect, AlignmentX::CENTER);
	AddGuiObject(obj);

	LoadText("defeat_text", "normal", Game::resources->colors.black);
	GuiObject* txt = new GuiObject("defeat_text", 0,0, GuiDepth::TEXT);
	txt->Align(obj->GetPosition(), AlignmentX::CENTER, AlignmentY::UP);
	txt->Translate(0, 20);
	AddGuiObject(txt);

	LoadText("return", "normal", Game::resources->colors.black);
	Button* b = new Button("mainmenu", "return", "confirm", 0, 500);
	b->Align(obj->GetPosition(), AlignmentX::CENTER, AlignmentY::DOWN);
	b->Translate(0, -20);
	b->click = std::bind(&DefeatMenu::Exit, this);
	AddGuiObject(b);
}

DefeatMenu::~DefeatMenu()
{

}

void DefeatMenu::Exit()
{
	action = ChangeStates::POPUNTIL;
	nextstate = States::MAINMENU;
}
