#ifndef SINGLEPLAYER_HPP_INCLUDED
#define SINGLEPLAYER_HPP_INCLUDED

#include "MainStates.hpp"


class SinglePlayerMenu : public State
{
		void GoNewGame();
		void GoLastSave();
		void GoLoadSave();
	public:
		SinglePlayerMenu();
		~SinglePlayerMenu();
};

class LoadSave : public State
{
		void Load(std::string name);
	public:
		LoadSave();
		~LoadSave();
};

class SaveError : public State
{
		void Exit();
	public:
		SaveError();
		~SaveError();
};

class PauseMenu : public State
{
		void Exit();
		void Settings();
	public:
		PauseMenu();
		void Update(float time_step);
		~PauseMenu();
};

class DefeatMenu : public State
{
		void Exit();
	public:
		DefeatMenu();
		~DefeatMenu();
};

#endif // SINGLEPLAYER_HPP_INCLUDED
