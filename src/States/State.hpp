#ifndef STATE_HPP_INCLUDED
#define STATE_HPP_INCLUDED

#include <list>

#include "../GuiObjects/Cursor.hpp"
#include "../GuiObjects/GuiObject.hpp"
#include "../GuiObjects/Button.hpp"
#include "../GuiObjects/Panel.hpp"

#include "../GameTimer.hpp"

#define PI 3.14159265

enum class States  // must add States also in StateManager ::PushState()
{
	INITSTATE,
	INTRO,
	MAINMENU,
	SINGLEPLAYERMENU,
	SETTINGSMENU,
	SETTINGSGRAPHICS,
	SETTINGSSOUNDS,
	SETTINGSGAMEPLAY,
	SETTINGSCONTROL,
	SETTINGSLANGUAGE,
	CREDITS,
	MULTIPLAYER,
	LOADSAVE,
	SAVEERROR,
	DEFEATMENU,
	PAUSEMENU,
	SPOOKYGAMEPLAY,
	MINIMIZEPAUSE,
	END
};

enum class ChangeStates
{
	NONE,
	RESTART,
	PUSH,
	POPANDPUSH,
	POP,
	POPUNTIL // in this case must select target state, in variable nextstate
	// if we want pop all states, nextstate=end
};


class State
{
	protected:
		GuiObject* background;
		GuiObject* cursor;

		std::list<GuiObject*> guiobjects;

		Camera* cam_gui;

		//path1 - texture in 4:3, path2 - texture in 16:9
		void CreateBackground(const std::string& name, const char* path1, const char* path2=nullptr);
		void CreateCursor(const std::string& name);

		std::string LoadText(const std::string& nameid, const char* font, SDL_Color color);

		void AddGuiObject(GuiObject* obj);

		virtual void BackgroundRender(); //for example for tiled maps
		//default render background, if exist

		virtual void DebugRender() { }
	public:

		States currentstate;
		ChangeStates action;
		States nextstate;
		bool translucent;

		State();
		virtual ~State();
		virtual void Update(float time_step);
		virtual void Render();

		void Return(); //pop actual instance of child class
};

#endif // STATE_HPP_INCLUDED
