#include "GameplayState.hpp"

GameplayState::GameplayState()
{
	float length_unit = 50.0f;
	gravity =new b2Vec2(0.0f, -10.0f);
	world=new b2World(*gravity);

	cam_gameplay = new Camera(Game::settings->options.resolution_x, Game::settings->options.resolution_y,
							  Perspective::FRUSTUM, Game::settings->options.renderer != RendererType::OGL3);

	testingrender = false;

	objdata.boxunit = length_unit;
	objdata.world = world;
	objdata.addobjfunc = std::bind(&GameplayState::AddObject, this, std::placeholders::_1);
	objdata.boxallocator = new b2BlockAllocator;
}

GameplayState::GameplayState(float xgravity, float ygravity, float boxunit)
{
	InitPhysicWorld(xgravity, ygravity, boxunit);

	cam_gameplay = new Camera(Game::settings->options.resolution_x, Game::settings->options.resolution_y);
}

void GameplayState::SetGameplayCameraType(Perspective type)
{
	cam_gameplay->Init(type);
}

void GameplayState::InitPhysicWorld(float xgravity, float ygravity, float boxunit)
{
	gravity = new b2Vec2(xgravity, ygravity);
	world = new b2World( *gravity);

	objdata.boxunit = boxunit;
	objdata.world = world;
	objdata.addobjfunc = std::bind(&GameplayState::AddObject, this, std::placeholders::_1);
	objdata.boxallocator = new b2BlockAllocator;

	testingrender = false;
}

void GameplayState::DeletePhysicWorld()
{
	DeleteAllObjects();

	delete world;
	delete gravity;
}

void GameplayState::AddObject(GameplayObject* obj)
{
	if(!gameplayobjects.empty())
	{
		int depth = obj->depth;
		auto it=gameplayobjects.begin();
		while(it!=gameplayobjects.end()&&((*it)->depth) < depth)
		{
			++it;
		}
		gameplayobjects.insert(it, obj);
	}
	else
		gameplayobjects.push_back(obj);
}


void GameplayState::DebugRender()
{
	if(testingrender)
	{
		for(auto it = gameplayobjects.begin(); it != gameplayobjects.end(); ++it)
		{
			(*it)->DebugRender(cam_gameplay);
		}
	}
}


GameplayObject* GameplayState::AddTestBody(float posx, float posy)
{
	Image* img = new Image("boxanimated", IsGui::NONE, 0,0, 0,0,50,50);

	GameplayObject* obj = new GameplayObject(objdata, img);
	obj->CreateBody(BodyType::DYNAMIC, posx, posy);

	b2PolygonShape dynamicBox;
	dynamicBox.SetAsBox(50.0f / objdata.boxunit / 2, 50.0f / objdata.boxunit / 2);
	//half of width and half of height
	obj->CreateFixture(dynamicBox, 1.0f, 0.5f, 0.2f);

	obj->body->SetLinearDamping(5.0f);
	obj->body->SetAngularDamping(5.0f);


	/*
	MODIFY MASS OF OBJECT
	b2MassData* mass = new b2MassData;
	obj->body->GetMassData(mass);
	mass->mass *= 0.5;
	obj->body->SetMassData(mass);
	delete mass;
	*/

	return obj;
}

GameplayObject* GameplayState::AddTestCircle(float posx, float posy)
{
	Image* img = new Image("circleanimated", IsGui::NONE, 0,0, 0,0,50,50);

	GameplayObject* obj = new GameplayObject(objdata, img);
	obj->CreateBody(BodyType::DYNAMIC, posx, posy);

	b2FixtureDef fixture;
	b2CircleShape circle;
	circle.m_radius = 50.0f / objdata.boxunit / 2; //0.5f;

	fixture.shape=&circle;
	fixture.density = 1.0f;
	fixture.friction=0.5f;
	fixture.restitution=0.2f;

	obj->AddFixture(fixture);

	return obj;
}

GameplayObject* GameplayState::AddStaticRect(float posx, float posy, float width, float height)
{
	GameplayObject* body = new GameplayObject(objdata);
	body->CreateBody(BodyType::STATIC, posx, posy);

	b2PolygonShape shape;
	shape.SetAsBox(width/2, height/2, b2Vec2(width/2, -height/2), 0);
	body->CreateFixture(shape);
	return body;
}

b2Vec2 GameplayState::ConvertToPhysicPos(const glm::vec2& v)
{
	b2Vec2 pos;
	pos.x =v.x/objdata.boxunit;// / Game::settings->options.ratio_x;
	pos.y =(- v.y)/objdata.boxunit;// / Game::settings->options.ratio_x;

	return pos;
}

glm::vec2 GameplayState::ConvertToGlPos(const b2Vec2& v)
{
	glm::vec2 pos;
	pos.x =v.x * objdata.boxunit;
	pos.y =(- v.y) * objdata.boxunit;

	return pos;
}

glm::vec2 GameplayState::ConvertToScreenPos(const b2Vec2& v) //MAYBE NEED - cam_gameplay->GetViewport().x;
{
	float x, y, z;
	x = -cam_gameplay->GetTranslation().x;
	y = -cam_gameplay->GetTranslation().y;
	z = -cam_gameplay->GetTranslation().z;

	glm::vec2 pos = ConvertToGlPos(v);
	pos.x = (pos.x - x)/ z;
	pos.y = (pos.y - y)/ z;
	return pos;
}


glm::vec2 GameplayState::GetMouseGlPos()
{
	float x, y;
	float z = -cam_gameplay->GetTranslation().z;
	x = Game::ctrl->mouse_x * z
		- cam_gameplay->GetTranslation().x - cam_gameplay->GetViewport().x * z;
	y = Game::ctrl->mouse_y * z
		- cam_gameplay->GetTranslation().y - cam_gameplay->GetViewport().y * z;

	return glm::vec2(x,y);
}

b2Vec2 GameplayState::GetMousePhysicPos()
{
	return ConvertToPhysicPos(GetMouseGlPos());
}

glm::vec2 GameplayState::ConvertScreenToGlPos(const glm::vec2& v)
{
	float x, y;
	float z = -cam_gameplay->GetTranslation().z;
	x = v.x * z - cam_gameplay->GetTranslation().x - cam_gameplay->GetViewport().x * z;
	y = v.y * z - cam_gameplay->GetTranslation().y - cam_gameplay->GetViewport().y * z;
	return glm::vec2(x,y);
}

b2Vec2 GameplayState::ConvertScreenToPhysicPos(const glm::vec2& v)
{
	return ConvertToPhysicPos(ConvertScreenToGlPos(v));
}

void GameplayState::DeleteAllObjects()
{
	Log("Gameplay objects amount: ",gameplayobjects.size(),"\n");

	for(auto it = gameplayobjects.begin(); it != gameplayobjects.end(); ++it)
	{
		delete (*it);
	}

	gameplayobjects.clear();
}


void GameplayState::Render()
{
	BackgroundRender();

	cam_gameplay->Active();
	for(auto it = gameplayobjects.begin(); it != gameplayobjects.end(); ++it)
	{
		(*it)->Render(cam_gameplay);
	}

	DebugRender();

	cam_gui->Active();
	for(auto it = guiobjects.begin(); it != guiobjects.end(); ++it)
	{
		(*it)->Render(cam_gui);
	}
}

void GameplayState::Update(float time_step)
{
	State::Update(time_step);

	world->Step(time_step, Game::settings->options.velocityIterations,
				Game::settings->options.positionIterations);
	Game::fps->UpdateTime(time_step * 1000);

	for(auto it = gameplayobjects.begin(); it != gameplayobjects.end(); ++it)
	{
		(*it)->Update();

		if( (*it)->todestroy)
		{
			if( (*it)->body != nullptr)
				world->DestroyBody( (*it)->body);
			delete (*it);
			it = gameplayobjects.erase(it);
		}
	}

	//debug
	static unsigned int timer = SDL_GetTicks();
	if(Game::ctrl->keystate[SDL_SCANCODE_F10] && (timer < SDL_GetTicks()) )
	{
		testingrender = !testingrender;
		timer = SDL_GetTicks() + 200;
	}
}

GameplayState::~GameplayState()
{

	DeletePhysicWorld();

	for(auto it = gameplayobjects.begin(); it != gameplayobjects.end(); ++it)
	{
		delete (*it);
	}

	gameplayobjects.clear();

	delete objdata.boxallocator;
}


