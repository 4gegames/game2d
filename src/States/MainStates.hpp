#ifndef MAINSTATES_HPP_INCLUDED
#define MAINSTATES_HPP_INCLUDED

#include "State.hpp"

class InitState : public State
{
	public:
		InitState();
		~InitState();
};

class Intro : public State
{
		void GoToNextState();
	public:
		Intro();
		void Update(float time_step);
		~Intro();
};

class MainMenu : public State
{
		void GoSinglePlayer();
		void GoMultiPlayer();
		void GoSettings();
		void GoCredits();
		void GoSpooky();
		void Exit();
		void RenderGui();
	public:
		MainMenu();
		void Update(float time_step);
		~MainMenu();
};

class SettingsMenu : public State
{
	protected:
		void Graphics();
		void Sounds();
		void Gameplay();
		void Control();
		void Language();
		void Menu();

		GuiObject* selection;
	public:
		SettingsMenu();
		~SettingsMenu();
};

class SettingsGraphics : public SettingsMenu
{
		int res_id;
		int res_amount;

		GuiObject* panel;
		GuiObject* res;

		int multisampling;
		bool vsync, fullscreen, firstrun;

		std::vector <std::pair<int, int> > resolutions;

		void Previous();
		void Next();
		void Restart();
	public:
		SettingsGraphics();
		~SettingsGraphics();
};

class SettingsSounds : public SettingsMenu
{
	public:
		SettingsSounds();
		~SettingsSounds();
};

class SettingsGameplay : public SettingsMenu
{
	public:
		SettingsGameplay();
		~SettingsGameplay();
};

class SettingsControl: public SettingsMenu
{
	public:
		SettingsControl();
		~SettingsControl();
};

class SettingsLanguage : public SettingsMenu
{
		std::string language;
		Button* b1;
		Button* b2;
		Button* b3;
		GuiObject* selected;

		void Restart();
		void SetEN();
		void SetPL();
		void SetRU();
	public:
		SettingsLanguage();
		~SettingsLanguage();
};

class Credits : public State
{
	public:
		Credits();
		~Credits();
};

class MinimizePause: public State
{
	public:
		MinimizePause();
		void Update(float time_step);
		~MinimizePause();
};

#endif // MAINSTATES_HPP_INCLUDED
