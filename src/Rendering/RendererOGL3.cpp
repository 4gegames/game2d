#include <algorithm>

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include "../Utils/FileUtils.hpp"
#include "RendererOGL3.hpp"

GLuint VAO::bound = 0;

Shader::Shader()
	: vs(0), fs(0), prog(0)
{
}

Shader::~Shader()
{
	glUseProgram(0);
	if (prog) glDeleteProgram(prog);
	if (vs) glDeleteShader(vs);
	if (fs) glDeleteShader(fs);
}

void Shader::Load(const std::string& vssrc, const std::string& fssrc)
{
	if (prog)
		return;

	const char* vsptr = vssrc.c_str();
	const char* fsptr = fssrc.c_str();

	glUseProgram(0);
	vs = glCreateShader(GL_VERTEX_SHADER);
	fs = glCreateShader(GL_FRAGMENT_SHADER);
	prog = glCreateProgram();

	glShaderSource(vs, 1, &vsptr, nullptr);
	glShaderSource(fs, 1, &fsptr, nullptr);
	glCompileShader(vs);
	glCompileShader(fs);
	glAttachShader(prog, vs);
	glAttachShader(prog, fs);
	glLinkProgram(prog);
}

VAO::VAO()
{
	glGenVertexArrays(1, &id);
}

VAO::~VAO()
{
	for (auto it = vbos.begin(), end = vbos.end(); it != end; ++it)
		glDeleteBuffers(1, &(it->second));
	glDeleteVertexArrays(1, &id);
}

void VAO::SetVBO(GLuint index, GLenum target, GLuint attrib_count, GLenum type, GLsizeiptr size, const GLvoid* data, GLenum usage)
{
	Bind();

	GLuint vbo = vbos[index];
	if (!vbo)
	{
		glGenBuffers(1, &vbo);
		vbos[index] = vbo;
		glEnableVertexAttribArray(index);
	}

	glBindBuffer(target, vbo);
	glBufferData(target, size, data, usage);
	glVertexAttribPointer(index, attrib_count, type, GL_FALSE, 0, 0);
}

void VAO::Bind()
{
	if (bound != id)
	{
		glBindVertexArray(id);
		bound = id;
	}
}

void VAO::Unbind()
{
	glBindVertexArray(0);
	bound = 0;
}

RendererOGL3::RendererOGL3(Settings* settings_param)
	: Renderer(settings_param),
	  is_init(false),
	  mv(1.0f), proj(1.0f),
	  quad(nullptr), line(nullptr)
{
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	context = SDL_GL_CreateContext(settingsptr->window);
	SDL_GL_MakeCurrent(settingsptr->window, context);
	SDL_GL_SetSwapInterval(settingsptr->options.vsync);

	glewExperimental = true;
	glewInit();

	int vmaj, vmin;
	glGetIntegerv(GL_MAJOR_VERSION, &vmaj);
	glGetIntegerv(GL_MINOR_VERSION, &vmin);
	Log(" - RendererOGL3\n", "GL context version: ", vmaj, ".", vmin, "\n");
}

RendererOGL3::~RendererOGL3()
{
	VAO::Unbind();
	// Need to delete them manually before deleting context. Any better ideas?
	quad.reset();
	line.reset();

	SDL_GL_DeleteContext(context);
}

void RendererOGL3::Init()
{
	if (is_init)
		return;

	glFrontFace(GL_CW);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	InitShaders();
	InitVAOs();

	is_init = true;

	glClearColor(0, 0, 0, 0);
	glViewport(0, 0, settingsptr->options.resolution_x, settingsptr->options.resolution_y);
	glClear(GL_COLOR_BUFFER_BIT);
	SDL_GL_SwapWindow(settingsptr->window);
}

void RendererOGL3::InitShaders()
{
	std::string vsbuf, fsbuf;

	ReadFile("Data/Shaders/texture.vert", vsbuf);
	ReadFile("Data/Shaders/texture.frag", fsbuf);
	shader_tex.Load(vsbuf, fsbuf);

	ReadFile("Data/Shaders/line.vert", vsbuf);
	ReadFile("Data/Shaders/line.frag", fsbuf);
	shader_line.Load(vsbuf, fsbuf);

	uniforms["texture_MVP"] = glGetUniformLocation(shader_tex.prog, "MVP");
	uniforms["texture_tex"] = glGetUniformLocation(shader_tex.prog, "tex");
	uniforms["color_MVP"] = glGetUniformLocation(shader_line.prog, "MVP");
	uniforms["color_RGBA"] = glGetUniformLocation(shader_line.prog, "RGBA");

	glUseProgram(shader_tex.prog);
	glUniformMatrix4fv(uniforms["texture_MVP"], 1, GL_FALSE, glm::value_ptr(mv));
	glUniform1i(uniforms["texture_tex"], 0);

	glUseProgram(shader_line.prog);
	glUniformMatrix4fv(uniforms["color_MVP"], 1, GL_FALSE, glm::value_ptr(mv));
	glUniform4f(uniforms["color_RGBA"], 1.0f, 1.0f, 1.0f, 1.0f);
}

void RendererOGL3::InitVAOs()
{
	quad = std::unique_ptr<VAO>(new VAO());
	line = std::unique_ptr<VAO>(new VAO());

	float quad_verts[18] = { 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0 };
	quad->SetVBO(0, GL_ARRAY_BUFFER, 3, GL_FLOAT, 18 * sizeof(float), quad_verts, GL_STREAM_DRAW);
}

void RendererOGL3::LoadTexture(SDL_Surface* surface, TextureData* texturedata)
{
	SDL_SetSurfaceAlphaMod(surface, 255);

	glGenTextures(1, &texturedata->id);
	glBindTexture(GL_TEXTURE_2D, texturedata->id);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, surface->w, surface->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void RendererOGL3::Render(GLuint textureid, SDL_Rect position, TextureSrc src, Camera* camera, int angle)
{
	if (camera->GetKind() == Perspective::PERSPECTIVE)
	{
		glm::vec3 center(camera->GetTranslation().x, camera->GetTranslation().y, 0);

		mv = glm::lookAt(camera->GetTranslation(), center, glm::vec3(0, -1, 0));
		proj = glm::perspective(65.0f, camera->GetWidth() / static_cast<float>(camera->GetHeight()), 0.5f, 2000.0f);
	}
	else
	{
		mv = glm::translate(camera->GetTranslation());
		proj = glm::frustum(0.0f, static_cast<float>(camera->GetWidth()), static_cast<float>(camera->GetHeight()), 0.0f, 1.0f, 25.0f);
	}

	mv = glm::translate(mv, glm::vec3(position.x, position.y, 0));

	if (angle)
	{
		mv = glm::translate(mv, glm::vec3(position.w / 2.0f, position.h / 2.0f, 0));
		mv = glm::rotate(mv, static_cast<float>(angle), glm::vec3(0, 0, 1));
		mv = glm::translate(mv, glm::vec3(-position.w / 2.0f, -position.h / 2.0f, 0));
	}

	mv = glm::scale(mv, static_cast<float>(position.w), static_cast<float>(position.h), 1.0f);

	glUseProgram(shader_tex.prog);
	glBindTexture(GL_TEXTURE_2D, textureid);

	glm::mat4 mvp = proj * mv;
	glUniformMatrix4fv(uniforms["texture_MVP"], 1, GL_FALSE, glm::value_ptr(mvp));

	float texcoords[12] =
	{
		src.left, src.top, src.right, src.top, src.left, src.bottom,
		src.left, src.bottom, src.right, src.top, src.right, src.bottom
	};

	quad->Bind();
	quad->SetVBO(1, GL_ARRAY_BUFFER, 2, GL_FLOAT, 12 * sizeof(float), texcoords, GL_STREAM_DRAW);

	glDrawArrays(GL_TRIANGLES, 0, 6);
}

void RendererOGL3::RenderEdges(const std::vector<float>& v, Camera* camera, SDL_Color* color, GLenum mode)
{
	if (camera->GetKind() == Perspective::PERSPECTIVE)
	{
		glm::vec3 center(camera->GetTranslation().x, camera->GetTranslation().y, 0);

		mv = glm::lookAt(camera->GetTranslation(), center, glm::vec3(0, -1, 0));
		proj = glm::perspective(65.0f, camera->GetWidth() / static_cast<float>(camera->GetHeight()), 0.5f, 2000.0f);
	}
	else
	{
		mv = glm::translate(camera->GetTranslation());
		proj = glm::frustum(0.0f, static_cast<float>(camera->GetWidth()), static_cast<float>(camera->GetHeight()), 0.0f, 1.0f, 25.0f);
	}

	glUseProgram(shader_line.prog);
	glBindTexture(GL_TEXTURE_2D, 0);

	glm::vec4 gl_color(1.0f, 0.0f, 0.0f, 0.5f);
	if (color)
		gl_color = glm::vec4(color->r / 255.0f, color->g / 255.0f, color->b / 255.0f, color->a / 255.0f);

	glm::mat4 mvp = proj * mv;
	glUniformMatrix4fv(uniforms["color_MVP"], 1, GL_FALSE, glm::value_ptr(mvp));
	glUniform4fv(uniforms["color_RGBA"], 1, glm::value_ptr(gl_color));

	line->Bind();
	line->SetVBO(0, GL_ARRAY_BUFFER, 2, GL_FLOAT, v.size() * sizeof(float), v.data(), GL_STREAM_DRAW);

	glLineWidth(2);
	glDrawArrays(mode, 0, v.size() / 2);
}
