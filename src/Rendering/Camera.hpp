#ifndef CAMERA_HPP_INCLUDED
#define CAMERA_HPP_INCLUDED

#include<GL/glew.h>
#include<GL/glu.h>
#include <glm/glm.hpp>
#include<SDL2/SDL.h>

#include "../Utils/Log.hpp"



enum class Perspective
{
	ORTHO,
	FRUSTUM,
	PERSPECTIVE
};

class Camera
{
		/***********************************************************************************
		this version of Camera use modifying translate.z to Zoom in/out
		Suggest refactorization path:
		modifying Frustum. Increase res_x and res_y to unzoom, and
		Decrease res_x and res_y to zoom
		***********************************************************************************/

		Perspective kind;
		int res_x, res_y;

		glm::vec3 translate;

		//camera has inverse values in translate .x and .y because
		//this is done by moving everything in the opposite direction

		glm::vec3 minimum, maximum;

		float zoom; // only for perspective

		SDL_Rect viewport;

		bool ogl2;

	public:
		Perspective GetKind() {return kind;}

		glm::vec3 GetTranslation()
		{ return translate; }

		void SetTranslation(float x, float y)
		{ translate = glm::vec3(x, y, 0); };

		void SetTranslation(float x, float y, float z)
		{ translate = glm::vec3(x, y, z); }

		void SetZoom(float value);

		float GetZoom()
		{ return translate.z; }

		int GetWidth() { return viewport.w; }
		int GetHeight() { return viewport.h; }

		void SetArea(float left, float right, float up, float down);
		void SetZoomScope(float near, float far);

		SDL_Rect GetViewArea();

		void SetViewport(int x, int y, int w, int h);
		//include little fuckfix with panel hide from right side

		SDL_Rect GetViewport()
		{ return viewport; }

		void Center(float x, float y);

		void ZoomIn(float value);
		void ZoomIn(float value, float x, float y); //x, y - screen coordinates

		void ZoomOut(float value);

		void SetMinZoom();
		void SetMaxZoom();

		void Active();
		void ResetTranslate();

		Camera(int resolution_x, int resolution_y, Perspective type=Perspective::FRUSTUM, bool old_opengl = true);

		void Init(Perspective type);

		void MoveHorizontal(float value, bool force = false); // x
		void MoveVertical(float value, bool force = false); // y

		//only for PERSPECTIVE
		void PerspectiveLookAt();
};

#endif // CAMERA_HPP_INCLUDED
