#ifndef RENDEREROGL2_HPP_INCLUDED
#define RENDEREROGL2_HPP_INCLUDED

#include "Renderer.hpp"

class RendererOGL2 : public Renderer
{
		GLuint bindtexture;
	public:
		RendererOGL2(Settings* settings_param);
		~RendererOGL2();

		void LoadTexture(SDL_Surface* surface, TextureData* texturedata);

		void Render(GLuint textureid, SDL_Rect position, TextureSrc src,
					Camera* camera = nullptr, int angle = 0);
		void RenderEdges(const std::vector<float>& v, Camera* camera,
						 SDL_Color* color = nullptr, GLenum mode = GL_LINE_LOOP);
};


#endif // RENDEREROGL2_HPP_INCLUDED
