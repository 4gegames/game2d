#include "Camera.hpp"


Camera::Camera(int resolution_x, int resolution_y, Perspective type, bool old_opengl)
	: res_x(resolution_x),
	  res_y(resolution_y),
	  ogl2(old_opengl)
{
	translate.x = 0;
	translate.y = 0;
	translate.z = -1;

	minimum.x = -5000;
	minimum.y = -8000;
	minimum.z = -10;

	maximum.x =  0;
	maximum.y =  0;
	maximum.z = -1;

	viewport = {0,0, resolution_x, resolution_y};

	Init(type);
}

void Camera::SetZoom(float value)
{
	float diff = value - translate.z;
	diff = diff >= 0 ? diff : diff * (-1.0f);

	if(translate.z > value)
		ZoomOut(diff);
	else
		ZoomIn(diff);
}


void Camera::Init(Perspective type)
{
	kind = type;
	zoom = -10;

	Active();
}

void Camera::Active()
{
	if (!ogl2)
		return;

	switch(kind)
	{
		case Perspective::ORTHO:
			glMatrixMode( GL_PROJECTION );
			glLoadIdentity();
			glOrtho( 0, viewport.w, viewport.h, 0, -1, 1 );
			glMatrixMode( GL_MODELVIEW );
			glLoadIdentity();
			break;
		case Perspective::FRUSTUM:

			//to down left corner
			glViewport(viewport.x, res_y - (viewport.y + viewport.h),
					   viewport.w, viewport.h);

			glMatrixMode( GL_PROJECTION );
			glLoadIdentity();
			glFrustum ( 0, viewport.w, viewport.h, 0, 1, 25 );
			glMatrixMode( GL_MODELVIEW );
			glLoadIdentity();

			break;
		case Perspective::PERSPECTIVE:
		{
			glMatrixMode( GL_PROJECTION );
			glLoadIdentity();
			gluPerspective(65.0, viewport.w/(float)viewport.h, 0.5, 2000);
			glMatrixMode( GL_MODELVIEW );
			glLoadIdentity();

			break;
		}
		default:
			Log("Error, bad parameter to initialization of camera\n");
	}
}

void Camera::SetArea(float left, float right, float up, float down)
{
	maximum.x = -left;
	maximum.y = -up;
	minimum.x = -right;
	minimum.y = -down;
}

void Camera::SetZoomScope(float near, float far)
{
	minimum.z = far;
	maximum.z = near;
}

SDL_Rect Camera::GetViewArea()
{
	SDL_Rect area;

	area.x = -translate.x;
	area.y = -translate.y;
	area.w = viewport.w * (-translate.z);
	area.h = viewport.h * (-translate.z);

	return area;
}

void Camera::SetViewport(int x, int y, int w, int h)
{
	viewport = {x, y, w, h};

	Active();

	//little fuckfix
	if(x+h < res_x )
	{
		MoveHorizontal( - (res_x - (x + h)) );
	}
}

void Camera::PerspectiveLookAt()
{
	if (!ogl2)
		return;

	gluLookAt(translate.x, translate.y, zoom,
			  translate.x, translate.y, 0, 0, -1, 0);
	//Must change method of the translation - if have perspective camera, can't use translate,
	//must modify parameter of gluLookAt() (e.g. first and fourth argument for vertical translation)
}

void Camera::ResetTranslate()
{
	translate.x = 0.0f;
	translate.y = 0.0f;
	translate.z = -1.0f;
}

void Camera::Center(float x, float y)
{
	if(kind == Perspective::PERSPECTIVE)
	{

	}
	else
	{
		float ax, ay;

		ax = translate.x + x - (viewport.w/2) *  (-translate.z);
		MoveHorizontal(ax);
		ay = translate.y + y - (viewport.h/2) *  (-translate.z);;
		MoveVertical(ay);
	}
}

void Camera::ZoomIn(float value)
{
	if(kind == Perspective::PERSPECTIVE)
	{
		zoom += value;
	}
	else
	{
		if(translate.z + value <= maximum.z)
		{
			translate.z += value;

			//zoom in center of screen
			MoveVertical( (float)viewport.h * value / 2);
			MoveHorizontal( (float)viewport.w * value / 2);
		}
		else if(translate.z < maximum.z)
		{
			ZoomIn(value/2);
		}
	}

	/*
	//modifying Frustum version:
	viewport.w*=0.95;
	viewport.h*=0.95;
	*/
}

void Camera::ZoomIn(float value, float x, float y)
{
	if(kind == Perspective::PERSPECTIVE)
	{
		zoom += value;
	}
	else
	{
		if(translate.z + value <= maximum.z)
		{
			translate.z += value;

			if(x > static_cast<float>(viewport.w) / 2 * 1.1 )
			{
				MoveHorizontal( (float)viewport.w * value );
			}

			if(y > static_cast<float>(viewport.w) / 2 * 1.1 )
			{
				MoveVertical( (float)viewport.h * value );
			}
		}
		else if(translate.z < maximum.z)
		{
			std::cout<<"recursion\n";
			ZoomIn(value/2);
		}
	}
}

void Camera::ZoomOut(float value)
{
	if(kind == Perspective::PERSPECTIVE)
	{
		zoom -= value;
	}
	else
	{
		float zmod = - value;// / 100;

		if( ( -(viewport.w * (translate.z + zmod) ) < -(minimum.x - maximum.x) )
				&& ( -(viewport.h * (translate.z + zmod) ) < -(minimum.y - maximum.y) )
		  ) //width and height in screen < total width and height
		{
			if(translate.z + zmod >= minimum.z)
			{
				translate.z += zmod;

				float y = (float)viewport.h * zmod;

				if(translate.y - viewport.h * (-translate.z) <= minimum.y) // down
				{
					translate.y -= y;
				}

				float x = (float)viewport.w * zmod / 2;

				/*
				translate with zoom out:
				nothing : for left corner
				- x : for center of screen
				- 2x: for right corner
				*/

				if(translate.x - x <= maximum.x)
				{
					translate.x -= x;
				}

				if( translate.x + viewport.w * translate.z <= minimum.x)
				{
					translate.x -= x;

					if(translate.x > maximum.x)
						translate.x = maximum.x; //clipping
				}
			}
			else if(translate.z > minimum.z)
			{
				ZoomOut(value/2);
			}
		}
		else if(( -(viewport.w * translate.z ) < -(minimum.x - maximum.x) )
				&& ( -(viewport.h * translate.z ) < -(minimum.y - maximum.y) ))
		{
			ZoomOut(value/2);
		}
	}
}

void Camera::SetMinZoom()
{
	translate.z = minimum.z;
}

void Camera::SetMaxZoom()
{
	translate.z = maximum.z;
}


void Camera::MoveHorizontal(float value, bool force)
{
	if(kind == Perspective::PERSPECTIVE)
		translate.x += value;
	else
	{
		if(force)
		{
			//translate.x -= value;
			if( translate.x - value <= maximum.x) // left
			{
				translate.x -= value;
			}
			else if( translate.x <= maximum.x) // left
			{
				MoveHorizontal(value/2, true);
			}
		}
		else if( translate.x - value <= maximum.x // left
				 && translate.x + viewport.w * translate.z - value >= minimum.x) // right
		{
			//std::cout<<"translate.x -= "<<value<<"\n";
			translate.x -= value;
		}
		else if( translate.x <= maximum.x // left
				 && translate.x + viewport.w * translate.z >= minimum.x) // right
		{
			MoveHorizontal(value/2);
		}
	}
}

void Camera::MoveVertical(float value, bool force)
{
	if(kind == Perspective::PERSPECTIVE)
		translate.y += value;
	else
	{
		if(force)
		{
			if( translate.y - value <= maximum.y) // up
			{
				translate.y -= value;
			}
			else if( translate.y <= maximum.y) // up
			{
				MoveVertical(value/2, true);
			}
		}
		else if( translate.y - value <= maximum.y // up
				 && translate.y + viewport.h * translate.z - value >= minimum.y) // down
		{
			translate.y -= value;
		}
		else if( translate.y <= maximum.y // up
				 && translate.y + viewport.h * translate.z >= minimum.y) // down
		{
			MoveVertical(value/2);
		}
	}
}

