#ifndef RENDERER_HPP_INCLUDED
#define RENDERER_HPP_INCLUDED

#include<map>
#include<memory>

#include<GL/glew.h>
#include<GL/glu.h>
#include<SDL2/SDL_opengl.h>
#include<SDL2/SDL_image.h>


#include "../Settings.hpp"
#include "Camera.hpp"

enum class PrimitiveType
{
	LINE_LOOP,
	LINE_STRIP
};

struct TextureSrc
{
	float left, right, top, bottom;
	int w, h;
};

struct TextureData
{
	int w, h;
	GLuint id;
};


class Renderer //virtual class
{
	protected:
		Settings* settingsptr;

		SDL_GLContext context;
	public:

		Renderer(Settings* settings_param)
			: settingsptr(settings_param)
		{
			Log("Renderer");
		}
		virtual ~Renderer() { }

		virtual void LoadTexture(SDL_Surface* surface, TextureData* texturedata) = 0;

		virtual void Render(GLuint textureid, SDL_Rect position, TextureSrc src,
							Camera* camera = nullptr, int angle = 0) = 0;
		virtual void RenderEdges(const std::vector<float>& v, Camera* camera,
								 SDL_Color* color=nullptr, GLenum mode=GL_LINE_LOOP) = 0;
};


#endif // RENDERER_HPP_INCLUDED
