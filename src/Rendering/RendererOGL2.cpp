#include "RendererOGL2.hpp"

RendererOGL2::RendererOGL2(Settings* settings_param)
	: Renderer(settings_param)
{
	context = SDL_GL_CreateContext(settingsptr->window);

	glClearColor( 0, 0, 0, 0 );

	glFrontFace(GL_CW);
	glEnable(GL_CULL_FACE);

	glEnable( GL_TEXTURE_2D ); // Need this to display a texture
	glEnable( GL_BLEND );

	//glEnable(GL_DEPTH_TEST);
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glViewport( 0, 0, settingsptr->options.resolution_x, settingsptr->options.resolution_y );


	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();

	glMatrixMode( GL_MODELVIEW );
	glLoadIdentity();
	SDL_GL_SwapWindow(settingsptr->window);
	SDL_GL_SetSwapInterval(settingsptr->options.vsync);



	Log(" - RendererOGL2\n");
	bindtexture = 0;
}

RendererOGL2::~RendererOGL2()
{
	Log("OpenGL errors: ",glGetError(),"\n");

	//checking OpenGL errors in 2.0 style
	GLenum errCode;
	const GLubyte* errString;

	if ((errCode = glGetError()) != GL_NO_ERROR)
	{
		errString = gluErrorString(errCode);
		Log("OpenGL Error: ", errString,"\n");
	}

	SDL_GL_DeleteContext(context);
}


void RendererOGL2::LoadTexture(SDL_Surface* surface, TextureData* texturedata)
{
	SDL_SetSurfaceAlphaMod(surface, 255);

	glGenTextures( 1, &texturedata->id );
	glBindTexture( GL_TEXTURE_2D, texturedata->id );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

	glTexImage2D( GL_TEXTURE_2D, 0, 4, surface->w, surface->h, 0,
				  GL_RGBA, GL_UNSIGNED_BYTE, surface->pixels );
}


void RendererOGL2::Render(GLuint textureid, SDL_Rect position, TextureSrc src,
						  Camera* camera, int angle)
{
	glLoadIdentity();

	if(camera->GetKind()==Perspective::PERSPECTIVE)
	{
		camera->PerspectiveLookAt();
	}
	else
		glTranslatef(camera->GetTranslation().x, camera->GetTranslation().y, camera->GetTranslation().z);

	glTranslatef(position.x, position.y, 0);

	if(angle != 0)
	{
		glTranslatef(position.w/2.0f, position.h/2.0f, 0);
		glRotatef(angle, 0, 0, 1);
		glTranslatef( -position.w/2.0f, -position.h/2.0f, 0);
	}


	//optimization, exclude not necessary binding texture
	if(bindtexture != textureid)
	{
		bindtexture = textureid;
		glBindTexture( GL_TEXTURE_2D, bindtexture);
	}

	glBegin( GL_QUADS );
	{
		// Top-left vertex (corner)
		glTexCoord2d(src.left,src.top);
		glVertex2s(0, 0);
		// top-right vertex (corner)
		glTexCoord2d(src.right,src.top);
		glVertex2s(position.w,0);
		// Bottom-right vertex (corner)
		glTexCoord2d(src.right,src.bottom);
		glVertex2s(position.w, position.h);
		// bottom-left vertex (corner)
		glTexCoord2d(src.left,src.bottom);
		glVertex2s(0, position.h);
	}
	glEnd();
}

void RendererOGL2::RenderEdges(const std::vector<float>& v, Camera* camera, SDL_Color* color, GLenum mode)
{
	glLoadIdentity();

	if(bindtexture != 0)
	{
		bindtexture = 0;
		glBindTexture( GL_TEXTURE_2D, bindtexture);
	}

	if(camera != nullptr)
	{
		glTranslatef(camera->GetTranslation().x, camera->GetTranslation().y, camera->GetTranslation().z);

		if(camera->GetKind()==Perspective::PERSPECTIVE)
			camera->PerspectiveLookAt();
	}
	else
	{
		Log("Camera don't exist\n");
		exit(1);
	}

	if(color != nullptr)
	{
		glColor4f(color->r/255.0f, color->g/255.0f, color->b/255.0f, color->a/255.0f);
	}
	else
		glColor4f(1.0,0.0,0.0,0.5);

	glLineWidth(2);
	int halfsize = v.size() / 2;
	glBegin(mode);
	{
		for(int i = 0; i < halfsize; i++)
		{
			glVertex2f(v[i*2], v[i*2 + 1]);
		}
	}
	glEnd();
	glColor3f(1.0, 1.0, 1.0);
	glLoadIdentity();
}
