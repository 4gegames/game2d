#ifndef RENDEREROGL3_HPP_INCLUDED
#define RENDEREROGL3_HPP_INCLUDED

#include "Renderer.hpp"

struct Shader
{
	GLuint vs, fs, prog;

	Shader();
	~Shader();

	void Load(const std::string& vssrc, const std::string& fssrc);
};

class VAO
{
		GLuint id;
		std::map<GLuint, GLuint> vbos;

		static GLuint bound;

	public:
		VAO();
		~VAO();

		void SetVBO(GLuint index, GLenum target, GLuint attrib_count, GLenum type, GLsizeiptr size, const GLvoid* data, GLenum usage);

		void Bind();
		static void Unbind();
};

class RendererOGL3 : public Renderer
{
		bool is_init;

		glm::mat4 mv, proj; // modelview & projection matrices
		std::unique_ptr<VAO> quad, line;

		Shader shader_tex, shader_line;
		std::map<std::string, GLuint> uniforms;

		void InitShaders();
		void InitVAOs();

	public:
		RendererOGL3(Settings* settings_param);
		~RendererOGL3();

		void Init();

		void LoadTexture(SDL_Surface* surface, TextureData* texturedata);

		void Render(GLuint textureid, SDL_Rect position, TextureSrc src,
					Camera* camera = nullptr, int angle = 0);
		void RenderEdges(const std::vector<float>& v, Camera* camera,
						 SDL_Color* color = nullptr, GLenum mode = GL_LINE_LOOP);
};

#endif // RENDEREROGL3_HPP_INCLUDED
