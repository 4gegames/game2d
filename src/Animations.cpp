#include "Animations.hpp"


void AnimationSystem::Play(unsigned int number, bool plooped, int pstartframe, int pnumberframes, bool forced)
{
	if(number!=active_number || active == false || actualframe == (numberframes+startframe) || forced)
	{
		active=true;
		active_number=number;
		looped=plooped;
		actualframe= startframe=pstartframe;
		if(pnumberframes)
			numberframes =pnumberframes;
		else
		{
			numberframes =animlines[number].second;
		}


		if(numberframes+startframe>maxframe)
		{
			startframe=0;
			numberframes =maxframe;
		}
	}
}

void AnimationSystem::SetBaseAnimation(int number)
{
	base=number;
	Play(number, true);
}

void AnimationSystem::SetNumberFrames(int line, int number)
{
	animlines[line].second = number;
}

void AnimationSystem::Update()
{
	if(active && timer.Passed())
	{
		if(actualframe>=(numberframes+startframe))
		{
			if(looped)
				actualframe= startframe;
			else
			{
				active_number=base;
				numberframes =animlines[base].second;
				looped=true;
				actualframe=0;
			}
		}

		img->ChangeSource(width*actualframe, height*animlines[active_number].first);
		actualframe++;
		timer.AddDelta(delay);
	}
}

void AnimationSystem::PauseAnimations()
{
	active=false;
}

int AnimationSystem::GetActiveAnimation()
{
	return active_number;
}

AnimationSystem::AnimationSystem(Image* pimg, int pdelay, int frame_w, int frame_h, int graphic_w, int graphic_h)
{
	active = false;
	active_number = base = 0;
	actualframe= startframe = 0;
	numberframes = 1;
	delay = pdelay;
	img = pimg;
	width = frame_w;
	height = frame_h;
	looped = false;

	maxframe = graphic_w / frame_w;
	int lines = graphic_h / frame_h;

	for(int i = 0; i < lines; i++)
	{
		animlines.push_back( std::make_pair(i, maxframe));
	}
}
