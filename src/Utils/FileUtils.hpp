#ifndef FILEUTILS_HPP_INCLUDED
#define FILEUTILS_HPP_INCLUDED

#include <string>

void ReadFile(const char* filename, std::string& buffer);

#endif // FILEUTILS_HPP_INCLUDED
