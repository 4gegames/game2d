#ifndef LOG_HPP_INCLUDED
#define LOG_HPP_INCLUDED

#include <fstream>
#include <iostream>

extern std::fstream log_file;

template<typename T>
void Log(T arg)
{
	log_file<<arg;
	log_file.flush();
}

template<typename T1, typename ...Ts>
void Log(T1 arg, Ts... args)
{
	Log(arg);
	Log(args...);
}

void InitLog();
void CloseLog();


#endif // LOG_HPP_INCLUDED
