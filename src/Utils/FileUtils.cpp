#include <fstream>

#include "FileUtils.hpp"

void ReadFile(const char* filename, std::string& buffer)
{
	std::ifstream f(filename);
	if (!f.is_open())
		return;

	f.seekg(0, std::ios::end);
	buffer.reserve(f.tellg());
	f.seekg(0, std::ios::beg);

	buffer.assign( (std::istreambuf_iterator<char>(f)), (std::istreambuf_iterator<char>()) );

	f.close();
}
