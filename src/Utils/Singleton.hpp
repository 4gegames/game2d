#ifndef SINGLETON_HPP_INCLUDED
#define SINGLETON_HPP_INCLUDED

#include <assert.h>

template <typename T>
class Singleton
{
	protected:
		static T* singleton;

		Singleton()
		{
			assert( !singleton );
			singleton = static_cast< T* >( this );
		}

		virtual ~Singleton()
		{
			assert( singleton );
			singleton = nullptr;
		}

	public:
		static T& GetSingleton()
		{
			if (singleton == nullptr)
				new T();
			return ( *singleton );
		}

		static T* GetSingletonPtr()
		{
			return singleton;
		}
};

template <typename T>
T* Singleton<T>::singleton = nullptr;
#endif//__CSINGLETON_H__

