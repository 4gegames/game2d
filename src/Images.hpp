#ifndef IMAGES_HPP_INCLUDED
#define IMAGES_HPP_INCLUDED

#include "Game.hpp"


enum class AlignmentX
{
	LEFT,
	RIGHT,
	CENTER,
	NONE
};

enum class AlignmentY
{
	UP,
	DOWN,
	CENTER,
	NONE
};

enum class IsGui
{
	GUISHIFT, //with shift depend of resolution ratio
	GUI, //without shift
	NONE
};

class Image
{
		int id;
		TexturePTR texturedata;
		TextureSrc src;
		bool source;

		int angle;

		IsGui isgui;
		bool displayed;

		SDL_Rect givenpos; //position, which was given in constructor
		//it's need, if instance of image belong to GuiObject, which is a child of panel
		AlignmentX align_x;
		AlignmentY align_y;
	public:

		SDL_Rect position;

		Image(const std::string& IdRes, IsGui is_gui = IsGui::GUISHIFT,
			  int x = 0, int y = 0, int w = 0, int h = 0);
		Image(const std::string& IdRes, IsGui is_gui,
			  int x, int y, int srcx, int srcy, int srcw, int srch);
		void ChangeSource(int x, int y);

		void Scale(float xmultiplier, float ymultiplier);

		void SetAngle(int angleparam);

		void Translate(int x, int y);
		void Align(SDL_Rect pos, AlignmentX x = AlignmentX::NONE,
				   AlignmentY y = AlignmentY::NONE);
		void ApplyLayout(SDL_Rect destination);

		AlignmentX GetHorizontalAlignment();
		AlignmentY GetVerticalAlignment();

		void Render(Camera* camera);

		void SetDisplay(bool value);
		bool GetDisplay();

		~Image();
};

#endif // IMAGES_HPP_INCLUDED
