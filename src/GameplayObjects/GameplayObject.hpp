#ifndef GAMEPLAYOBJECT_HPP_INCLUDED
#define GAMEPLAYOBJECT_HPP_INCLUDED

#include <list>
#include <Box2D/Box2D.h>
#include "../Images.hpp"
#include "../Animations.hpp"
#include "../Spooky/tilemap.hpp"

#include "../GameTimer.hpp"


enum class BodyType
{
	DYNAMIC,
	KINEMATIC,
	STATIC
};

class GameplayObject;

typedef std::function<void(GameplayObject*)> addobjectdelegate;

struct PhysicObjectData
{
	int boxunit;
	b2World* world = nullptr;
	b2BlockAllocator* boxallocator = nullptr;
	addobjectdelegate addobjfunc;
};

class GameplayObject
{
	protected:
		Image* img;
		float length_unit;
		SDL_Color debug_color;

		bool active;

		PhysicObjectData objdata;

		std::vector<float> borderpoints;
		b2Vec2 lastposition = b2Vec2(0,0); // if change position, then must recalculate select border
		b2Vec2 center;

	public:
		MapObjectType type;

		//depends of object type
		bool destroyable = true;

		b2Body* body;
		AnimationSystem* anim;

		short int depth;

		bool todestroy;

		/***********************************
		Must pre-calculate coordinates for all vertex in all fixtures
		and put to container, which size should be equal number of fixtures
		in DebugRender() method must take values from this new container,
		and translate him with fixture coordinates
		***********************************/


		void ComputeBorderPoints();

		//used only in edit mode for rotating
		float startangle;
		float localangle;

		b2Vec2 scaleratio; // scale in x and y axis
		b2Vec2 initscaleratio;


		void SetActive(bool value);
		bool IsActive();


		void SetAnimation(Image* pimg, int pdelay, int frame_w, int frame_h, int graphic_w, int graphic_h);
		glm::vec2 GetCenterPos(); //center of object
		b2Vec2 GetCenterPhysicPos();

		void CreateBody(BodyType bodytype, float x, float y);

		void AddFixture(b2FixtureDef fixturedef, bool inituserdata = true);
		void CreateFixture(b2PolygonShape& shape, float density = 1.0f, float friction=0.2f, float restitution=0.0f);

		SDL_Color GetDebugColor();
		void SetDebugColor(SDL_Color* color, int alpha = 255);

		void SetImage(Image* imgparam);
		Image* GetImage();

		void Scale(float xmultiplier, float ymultiplier);


		void Render(Camera* cam);
		/****
		need iterate of all fixtures
		****/
		void DebugRender(Camera* cam);

		void RenderSelectBorder(Camera* cam); //if selected, render select border

		virtual void Update();

		virtual bool OnCollision(GameplayObject* colliding);
		virtual void OnEndCollision();


		virtual void Destroy();

		GameplayObject(const PhysicObjectData& physicdata, Image* imgparam = nullptr, b2Body* bodyparam = nullptr);

		virtual ~GameplayObject();
};


#endif // GAMEPLAYOBJECT_HPP_INCLUDED
