#include "GameplayObject.hpp"

GameplayObject::GameplayObject(const PhysicObjectData& physicdata, Image* imgparam, b2Body* bodyparam)
{
	objdata = physicdata;

	img = imgparam;
	body = bodyparam;
	anim = nullptr;

	debug_color = Game::resources->colors.red;

	todestroy = false;
	depth = 1;

	active = true;

	type = MapObjectType::NONE;

	if(body != nullptr)
	{
		body->SetUserData(this);
	}

	objdata.addobjfunc(this);

	scaleratio.x = 1.0f;
	scaleratio.y = 1.0f;
}

void GameplayObject::SetImage(Image* imgparam)
{
	img=imgparam;
}

void GameplayObject::ComputeBorderPoints()
{
	b2AABB aabb;
	aabb.lowerBound = b2Vec2(FLT_MAX,FLT_MAX);
	aabb.upperBound = b2Vec2(-FLT_MAX,-FLT_MAX);

	//little hack because to compute aabb, body must be active
	bool isactive = true;
	if( !body->IsActive())
	{
		body->SetActive(true);
		isactive = false;
	}

	for (b2Fixture* fixture = body->GetFixtureList(); fixture; fixture = fixture->GetNext())
	{
		if(fixture->GetType() == 3) //if chain
		{
			b2ChainShape* shape = (b2ChainShape*)fixture->GetShape();

			for(int i = 0; i < shape->m_count - 1; i++)
				aabb.Combine(aabb, fixture->GetAABB(i));
		}
		else
			aabb.Combine(aabb, fixture->GetAABB(0));
	}

	if( isactive == false)
		body->SetActive(false);

	borderpoints.clear();

	b2Vec2 v1 = aabb.lowerBound;
	v1.x *= objdata.boxunit;
	v1.y *= objdata.boxunit;
	b2Vec2 v2 = aabb.upperBound;
	v2.x *= objdata.boxunit;
	v2.y *= objdata.boxunit;

	borderpoints.push_back(v1.x);
	borderpoints.push_back(-v1.y);
	borderpoints.push_back(v2.x);
	borderpoints.push_back(-v1.y);
	borderpoints.push_back(v2.x);
	borderpoints.push_back(-v2.y);
	borderpoints.push_back(v1.x);
	borderpoints.push_back(-v2.y);

	center = aabb.lowerBound + aabb.upperBound;
	center.x = center.x /2;
	center.y = center.y /2;
}

void GameplayObject::SetActive(bool value)
{
	active = value;
	if(img != nullptr)
		img->SetDisplay(value);

	if(body != nullptr)
		body->SetActive(value);
}

bool GameplayObject::IsActive()
{
	return active;
}

void GameplayObject::SetAnimation(Image* pimg, int pdelay, int frame_w, int frame_h, int graphic_w, int graphic_h)
{
	anim=new AnimationSystem(pimg, pdelay, frame_w, frame_h, graphic_w, graphic_h);
}

glm::vec2 GameplayObject::GetCenterPos()
{
	int x = GetImage()->position.x + GetImage()->position.w/2;
	int y = GetImage()->position.y + GetImage()->position.h/2;

	return glm::vec2(x, y);
}

b2Vec2 GameplayObject::GetCenterPhysicPos()
{
	ComputeBorderPoints();

	return center;
}

void GameplayObject::CreateBody(BodyType bodytype, float x, float y)
{
	b2BodyDef bodydef;

	switch(bodytype)
	{
		case BodyType::DYNAMIC:
			bodydef.type=b2_dynamicBody;
			break;
		case BodyType::KINEMATIC:
			bodydef.type=b2_kinematicBody;
			break;
		case BodyType::STATIC:
			bodydef.type=b2_staticBody;
			break;
		default:
			Log("Creating body failed, incorrect type\n");
	}

	bodydef.position.Set(x,y);
	body = objdata.world->CreateBody(&bodydef);
	body->SetUserData(this);
}


void GameplayObject::AddFixture(b2FixtureDef fixturedef, bool inituserdata)
{
	b2Fixture* fixture = body->CreateFixture(&fixturedef);

	if(inituserdata)
	{
		b2Shape* s = fixture->GetShape()->Clone(objdata.boxallocator);
		fixture->SetUserData(s);
	}
}

void GameplayObject::CreateFixture(b2PolygonShape& shape, float density, float friction, float restitution)
{
	b2FixtureDef fixture;
	fixture.shape = &shape;
	fixture.density = density;
	fixture.friction = friction;
	fixture.restitution = restitution;

	AddFixture(fixture);
}

SDL_Color GameplayObject::GetDebugColor()
{
	return debug_color;
}

void GameplayObject::SetDebugColor(SDL_Color* color, int alpha)
{
	debug_color.r = color->r;
	debug_color.g = color->g;
	debug_color.b = color->b;
	debug_color.a = alpha;
}

Image* GameplayObject::GetImage()
{
	return img;
}


void GameplayObject::Scale(float xmultiplier, float ymultiplier)
{
	scaleratio.x = xmultiplier;
	scaleratio.y = ymultiplier;

	for(b2Fixture* fixture = body->GetFixtureList(); fixture; fixture = fixture->GetNext())
	{
		b2FixtureDef scaled;
		scaled.density = fixture->GetDensity();
		scaled.filter = fixture->GetFilterData();
		scaled.friction = fixture->GetFriction();
		scaled.isSensor = fixture->IsSensor();
		scaled.restitution = fixture->GetRestitution();

		scaled.userData = fixture->GetUserData();

		switch(fixture->GetType())
		{
			case 0: //circle
			{
				b2CircleShape* initshape = static_cast<b2CircleShape*>(scaled.userData);
				b2CircleShape shape;
				shape.m_radius = initshape->m_radius * xmultiplier;

				scaled.shape = &shape;
				AddFixture(scaled, false);
			}
			break;
			case 1: //edge //not tested
			{
				b2EdgeShape* initshape = static_cast<b2EdgeShape*>(scaled.userData);
				b2EdgeShape shape;
				shape.m_vertex1 = b2Vec2(initshape->m_vertex1.x * xmultiplier,
										 initshape->m_vertex1.y * ymultiplier);
				shape.m_vertex2 = b2Vec2(initshape->m_vertex2.x * xmultiplier,
										 initshape->m_vertex2.y * ymultiplier);

				scaled.shape = &shape;
				AddFixture(scaled, false);
			}
			break;
			case 2: //polygon
			{
				b2PolygonShape* initshape = static_cast<b2PolygonShape*>(scaled.userData);
				int vcount = initshape->GetVertexCount();

				b2Vec2* points = new b2Vec2[vcount];

				for(int i = 0; i < vcount; i++)
				{
					b2Vec2 p = initshape->GetVertex(i);
					points[i].x = p.x * xmultiplier;
					points[i].y = p.y * ymultiplier;
				}

				b2PolygonShape shape;
				shape.Set(points, vcount);
				delete points;

				scaled.shape = &shape;
				AddFixture(scaled, false);
			}
			break;
			case 3: //chain
			{
				b2ChainShape* initshape = static_cast<b2ChainShape*>(scaled.userData);
				int vcount = initshape->m_count;

				b2Vec2* points = new b2Vec2[vcount];

				for(int i = 0; i < vcount; i++)
				{
					b2Vec2 p = initshape->m_vertices[i];
					points[i].x = p.x * xmultiplier;
					points[i].y = p.y * ymultiplier;
				}

				b2ChainShape shape;
				shape.CreateChain(points, vcount);
				delete points;

				scaled.shape = &shape;
				AddFixture(scaled, false);
			}
			break;
			default:
				Log("Fixture without shape type!\n");
				break;
		}

		ComputeBorderPoints();

		if(img)
		{
			img->Scale(xmultiplier, ymultiplier);
		}

		body->DestroyFixture(fixture);
	}
}

void GameplayObject::DebugRender(Camera* cam)
{
	if(body != nullptr)
	{
		for (b2Fixture* fixture = body->GetFixtureList(); fixture; fixture = fixture->GetNext())
		{
			if(fixture->IsSensor())
				debug_color = Game::resources->colors.yellow;

			if( !body->IsActive())
				debug_color.a= 100;

			switch(fixture->GetType())
			{
				case 0: //circle
				{
					b2CircleShape* shape = (b2CircleShape*)fixture->GetShape();

					int vcount = 9; // or 18 for better quality // own number of verticles for testing render

					float radius = shape->m_radius;

					b2Vec2* vtab=new b2Vec2[vcount];

					int j=0;
					for (int a = 0; a < 360; a += 360 / vcount)
					{
						double heading = a * M_PI / 180;
						vtab[j].Set(cos(heading) * radius, sin(heading) * radius);
						j++;
					}

					std::vector<float> v;
					v.reserve(vcount);
					for (int i = 0; i < vcount; ++i)
					{
						b2Vec2 vertex = fixture->GetBody()->GetWorldPoint( vtab[i] );
						vertex.x += shape->m_p.x;
						vertex.y += shape->m_p.y;

						v.push_back(vertex.x * objdata.boxunit);
						v.push_back( -vertex.y * objdata.boxunit);
					}

					Game::renderer->RenderEdges(v, cam, &debug_color);
					delete []vtab;
					v.clear();
				}
				break;
				case 1: //edge
				{
					b2EdgeShape* shape = (b2EdgeShape*)fixture->GetShape();
					std::vector<float> v;

					b2Vec2 v1=fixture->GetBody()->GetWorldPoint(shape->m_vertex1);
					b2Vec2 v2=fixture->GetBody()->GetWorldPoint(shape->m_vertex2);

					v.push_back(v1.x*objdata.boxunit);
					v.push_back( -v1.y*objdata.boxunit);

					v.push_back(v2.x*objdata.boxunit);
					v.push_back( -v2.y*objdata.boxunit);

					Game::renderer->RenderEdges(v, cam, &debug_color);
				}
				break;
				case 2: //polygon
				{
					b2PolygonShape* polygonShape = (b2PolygonShape*)fixture->GetShape();
					int vertexCount = polygonShape->GetVertexCount();

					std::vector<float> v;
					for (int i = 0; i < vertexCount; ++i)
					{
						//get the vertex in world coordinates
						b2Vec2 vertex = fixture->GetBody()->GetWorldPoint( polygonShape->GetVertex(i) );
						v.push_back(vertex.x * objdata.boxunit);
						v.push_back( -vertex.y * objdata.boxunit);
					}

					Game::renderer->RenderEdges(v, cam, &debug_color);
				}
				break;
				case 3: //chain
				{
					b2ChainShape* shape = (b2ChainShape*)fixture->GetShape();
					int vcount = shape->m_count;

					std::vector<float> v;
					for (int i = 0; i < vcount; ++i)
					{
						b2Vec2 vertex = fixture->GetBody()->GetWorldPoint( shape->m_vertices[i] );
						v.push_back(vertex.x * objdata.boxunit);
						v.push_back( -vertex.y * objdata.boxunit);
					}

					Game::renderer->RenderEdges(v, cam, &debug_color, GL_LINE_STRIP);
				}
				break;
				default:
					Log("Fixture without shape type!\n");
					break;
			}
		}
	}
}

void GameplayObject::RenderSelectBorder(Camera* cam)
{
	lastposition = GetCenterPhysicPos();

	Game::renderer->RenderEdges(borderpoints, cam, &Game::resources->colors.white);
}

void GameplayObject::Render(Camera* cam)
{
	if(img != nullptr)
		img->Render(cam);
}

void GameplayObject::Update()
{
	if(body!=nullptr && img!=nullptr)
	{
		b2Vec2 pos = body->GetPosition(); //get position of physical object
		float rot = body->GetAngle(); //get angle of physical object

		if(img != nullptr)
		{
			img->position.x =(pos.x*objdata.boxunit) - img->position.w/2;
			img->position.y = - (pos.y*objdata.boxunit + img->position.h/2);
			img->SetAngle(-(180*rot/ M_PI));
		}
	}

	if(anim!=nullptr)
		anim->Update();
}

bool GameplayObject::OnCollision(GameplayObject*)
{
	return false;
}

void GameplayObject::OnEndCollision()
{

}


void GameplayObject::Destroy()
{
	todestroy = true;
}

GameplayObject::~GameplayObject()
{
	delete img;
	delete anim;
}
