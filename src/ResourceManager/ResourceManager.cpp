#include "ResourceManager.hpp"

void ResourceManager::InitButtonModels()
{
	std::string name, nimg, nhover, ntime, ndepth;
	std::fstream resfile( "Data/buttons.csv", std::ios::in );
	getline(resfile, name);
	while(resfile.good())
	{
		getline(resfile, name, ';');
		getline(resfile, nimg, ';');
		getline(resfile, ntime, ';');
		getline(resfile, nhover, ';');
		getline(resfile, ndepth, '\n');

		int n = std::stoi(ntime);

		bool under = std::stoi(ndepth);

		buttonmodels[name] = {nimg, nhover, n, under};
	}
}

void ResourceManager::InitFonts()
{
	std::fstream resfile;
	std::string name, file, fontsize;

	resfile.open("Data/Fonts/fonts.csv", std::ios::in);

	getline(resfile, name, '\n'); //headers
	while(resfile.good())
	{
		getline(resfile, name, ';');
		getline(resfile, file, ';');
		getline(resfile, fontsize,'\n');

		std::string filename = "Data/Fonts/" + file;
		int s = std::stoi(fontsize);

		fonts[name] = TTF_OpenFont(filename.c_str(), s);
	}
}


void ResourceManager::InitTexts(const std::string& language)
{
	std::fstream textfile;
	textfile.open("Data/texts.csv", std::ios::in);

	std::string textid, textline, temp;
	getline(textfile, temp, '\n'); // headline
	while(textfile.good())
	{
		if(language=="PL")
		{
			getline(textfile, textid, ';');
			getline(textfile, textline, ';');
			getline(textfile, temp, '\n');
		}
		else if(language=="RU")
		{
			getline(textfile, textid, ';');
			getline(textfile, temp, ';');
			getline(textfile, temp, ';');
			getline(textfile, textline, '\n');
		}
		else // if(language=="EN") //default English language
		{
			getline(textfile, textid, ';');
			getline(textfile, temp, ';');
			getline(textfile, textline, ';');
			getline(textfile, temp, '\n');
		}
		texts[textid] = textline;
	}
}

void ResourceManager::InitColors()
{
	colors.black.r = 0;
	colors.black.g = 0;
	colors.black.b = 0;
	colors.black.a = 255;
	colors.white.r = 255;
	colors.white.g = 255;
	colors.white.b = 255;
	colors.white.a = 255;
	colors.red.r = 255;
	colors.red.g = 0;
	colors.red.b = 0;
	colors.red.a = 255;
	colors.green.r = 0;
	colors.green.g = 255;
	colors.green.b = 0;
	colors.green.a = 255;
	colors.blue.r = 0;
	colors.blue.g = 0;
	colors.blue.b = 255;
	colors.blue.a = 255;
	colors.yellow.r = 255;
	colors.yellow.g = 255;
	colors.yellow.b = 100;
	colors.yellow.a = 255;
}

std::string ResourceManager::GetPathFromName(const std::string& name, const std::string& folder,
		const std::string& extension)
{
	std::string path;
	size_t slashpos = 0;
	while(slashpos < name.length())
	{
		if(name[slashpos] == '/')
			break;

		slashpos++;
	}

	if(slashpos == name.length())
		path = folder;
	//path="Data/Images/";

	path += name;
	size_t dotpos = 0;
	while(dotpos<name.length())
	{
		if(name[dotpos] == '.')
			break;

		dotpos++;
	}

	if(dotpos == name.length())
		path += extension;
	//path+=".png";

	return path;
}

std::string ResourceManager::GetNameFromPath(const std::string& path)
{
	//Data/Images/file.png

	int slashpos = path.rfind('/') + 1;
	int dotpos = path.find('.');

	return path.substr(slashpos, dotpos-slashpos);
}

ButtonModel ResourceManager::GetButtonData(const std::string& name)
{
	return buttonmodels.find(name)->second;
}


TTF_Font* ResourceManager::GetFont(const std::string& name)
{
	if(fonts.find(name) != fonts.end())
	{
		return fonts.find(name)->second;
	}
	else
	{
		Log("Error, font: ",name," not found\n");
		return nullptr;
	}
}


std::string ResourceManager::GetTextLine(const std::string& textid)
{
	if(texts.find(textid) != texts.end() )
	{
		return texts.find(textid)->second;
	}
	else
	{
		Log("Error, text line with id: ", textid, "not found\n");
		return "ERROR";
	}
}

TexturePTR ResourceManager::LoadTexture(const std::string& name, const std::string& path1)
{
	if(graphics.find(name) == graphics.end()) //texture not found in memory
	{
		std::string path;

		if(path1 != "")
		{
			path = path1;
		}
		else
		{
			path = GetPathFromName(name);
		}

		SDL_Surface* surface;
		surface = IMG_Load(path.c_str());
		if( !surface)
		{
			Log("Error, graphic with name: ",name," and path: ",path," is not found\n");
			SDL_FreeSurface(surface);
			surface = IMG_Load("Data/Images/error.png");
		}

		TextureData* texturedata = new TextureData;
		texturedata->w = surface->w;
		texturedata->h = surface->h;

		//need check texture exist
		renderer->LoadTexture(surface, texturedata);

		graphics[name] = TexturePTR(texturedata, TextureDeleter());

		SDL_FreeSurface(surface);

	}

	return graphics.find(name)->second;
}

TexturePTR ResourceManager::LoadTextTexture(const std::string& textid, const std::string& fontname, SDL_Color color)
{
	if(graphics.find(textid) == graphics.end()) //texture not found in memory
	{
		SDL_Surface* surface;
		const char* txt = GetTextLine(textid).c_str();

		TTF_Font* font = GetFont(fontname);

		if(font != nullptr)
		{
			//TTF create surface in BGR, so must swap B and R channel
			int r = color.b;
			color.b = color.r;
			color.r = r;

			surface = TTF_RenderUTF8_Blended(font, txt, color);
		}
		else
		{
			Log("ERROR, font: ",fontname, "is not found\n");
			surface = IMG_Load("Data/Images/error.png");
		}

		TextureData* texturedata = new TextureData;
		texturedata->w = surface->w;
		texturedata->h = surface->h;

		renderer->LoadTexture(surface, texturedata);

		graphics[textid] = TexturePTR(texturedata, TextureDeleter());

		SDL_FreeSurface(surface);
	}

	return graphics.find(textid)->second;
}

TexturePTR ResourceManager::LoadCustomTextTexture(const std::string& text, const std::string& name,
		const std::string& fontname, SDL_Color color)
{
	if(graphics.find(name) == graphics.end()) //texture not found in memory
	{
		SDL_Surface* surface;

		TTF_Font* font = GetFont(fontname);

		if(font != nullptr)
		{
			//TTF create surface in BGR, so must swap B and R channel
			int r = color.b;
			color.b = color.r;
			color.r = r;

			surface = TTF_RenderUTF8_Blended(font, text.c_str(), color);
		}
		else
		{
			Log("ERROR, font: ",fontname, "is not found\n");
			surface = IMG_Load("Data/Images/error.png");
		}

		TextureData* texturedata = new TextureData;
		texturedata->w = surface->w;
		texturedata->h = surface->h;

		renderer->LoadTexture(surface, texturedata);

		graphics[name] = TexturePTR(texturedata, TextureDeleter());

		SDL_FreeSurface(surface);
	}

	return graphics.find(name)->second;
}

TexturePTR ResourceManager::GetTextureData(const std::string& name)
{
	auto it = graphics.find(name);
	if(it == graphics.end())
	{
		return LoadTexture(name);
	}

	return (*it).second;
}

void ResourceManager::LogTextures()
{
	for(auto it = graphics.begin(); it != graphics.end(); ++it)
		Log(it->first, "\n" );
}

void ResourceManager::CleanTextureCache()
{
	for(auto it = graphics.begin(); it != graphics.end(); ++it)
	{
		if(it->second.unique() == 1)
		{
			it = graphics.erase(it);
		}
	}
}


ResourceManager::ResourceManager(Settings* settings, Renderer* rendererptr)
{
	sprite_id = 0;

	renderer = rendererptr;

	InitTexts(settings->options.language);
	InitButtonModels();
	InitColors();
	InitFonts();
}


ResourceManager::~ResourceManager()
{
	buttonmodels.clear();

	for(auto it = fonts.begin(); it != fonts.end(); ++it)
	{
		TTF_CloseFont(it->second);
	}
	fonts.clear();
}
