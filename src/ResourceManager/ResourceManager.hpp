#ifndef RESOURCEMANAGER_HPP_INCLUDED
#define RESOURCEMANAGER_HPP_INCLUDED

#include<map>
#include<string>
#include<fstream>
#include<vector>
#include<map>
#include<memory>
#include<cassert>

#include<SDL2/SDL_ttf.h>


#include "../Settings.hpp"
#include "../Rendering/Renderer.hpp"

struct ButtonModel
{
	std::string img;
	std::string hoverimg;
	int timestep;
	bool underbutton;
};


struct TextureDeleter
{
	void operator()(TextureData* textures)
	{
		glDeleteTextures (1, &textures->id);
		delete textures;
	}
};

typedef std::shared_ptr<TextureData> TexturePTR;


class ResourceManager
{
		std::map<std::string, ButtonModel> buttonmodels;
		void InitButtonModels();

		//Fonts are loaded from file
		std::map<std::string, TTF_Font*> fonts;
		void InitFonts();

		std::map<std::string, std::string> texts;
		void InitTexts(const std::string& language);

		void InitColors();

		Renderer* renderer;

		std::map<std::string, TexturePTR> graphics;

		int sprite_id;
	public:

		int GetImageID()
		{
			return sprite_id++;
		}

		std::string GetPathFromName(const std::string& name, const std::string& folder="Data/Images/",
									const std::string& extension=".png");
		std::string GetNameFromPath(const std::string& path);

		ButtonModel GetButtonData(const std::string& name);

		TTF_Font* GetFont(const std::string& name);

		std::string GetTextLine(const std::string& textid);

		TexturePTR LoadTexture(const std::string& name, const std::string& path1 = "");
		TexturePTR LoadTextTexture(const std::string& textid, const std::string& fontname,
								   SDL_Color color);
		TexturePTR LoadCustomTextTexture(const std::string& text, const std::string& name,
										 const std::string& fontname, SDL_Color color);
		TexturePTR GetTextureData(const std::string& name);

		void LogTextures();
		void CleanTextureCache();

		struct
		{
			SDL_Color black, white, blue, green, red, yellow;
		} colors;

		ResourceManager(Settings* settings, Renderer* rendererptr);
		~ResourceManager();
};

#endif // RESOURCEMANAGER_HPP_INCLUDED
