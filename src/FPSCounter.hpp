#ifndef FPSCOUNTER_HPP_INCLUDED
#define FPSCOUNTER_HPP_INCLUDED

#include<fstream>
#include<vector>
#include<map>
#include<cassert>

#include<SDL2/SDL.h>

#include "Utils/Log.hpp"

class FPSCounter
{
		float FPS_Time, DeltaTime, FPS_Number;
		int FPS_Frames;
		unsigned long DTTicks, DTNewTicks;
		const float MAX_ACCUMULATED_TIME;
		std::vector <int> fpscontainer;

		unsigned int gameplaytime;
	public:
		float accumulator;
		const float TIME_STEP;

		FPSCounter();
		~FPSCounter();

		void CountFPS();

		void UpdateTime(int delta);
		unsigned int GetGameplayTime();
};


#endif // FPSCOUNTER_HPP_INCLUDED
