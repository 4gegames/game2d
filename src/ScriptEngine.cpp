#include "ScriptEngine.hpp"


TiledMap* ScriptEngine::LoadMap(const std::string& name)
{
	using namespace luabind;

	std::string path="Data/Maps/"+name+".lua";

	Log("PATH: ",path,"\n");

	std::string script= "tab = dofile(\""+path+"\")";

	luaL_dostring(lua, script.c_str());

	//RunScript("Data/Maps/map.lua");

	luabind::object obj = luabind::globals(lua)["tab"];


	TiledMap* tmap= new TiledMap;

	tmap->mapversion= std::stof(object_cast<std::string>(globals(lua)["tab"]["version"] ));
	tmap->width=object_cast<int>(globals(lua)["tab"]["width"]);
	tmap->height=object_cast<int>(globals(lua)["tab"]["height"]);

	tmap->tilewidth=object_cast<int>(globals(lua)["tab"]["tilewidth"]);
	tmap->tileheight=object_cast<int>(globals(lua)["tab"]["tileheight"]);


	LoadLayers(tmap);
	LoadTilesets(tmap);

	return tmap;
}

void ScriptEngine::LoadTilesets(TiledMap* tmap)
{
	using namespace luabind;

	int counterset= 1;
	//tilesets loop
	for (luabind::iterator i(luabind::globals(lua)["tab"]["tilesets"]), end; i != end; ++i)
	{
		Tileset* tset= new Tileset;

		tset->name=object_cast<std::string>(globals(lua)["tab"]["tilesets"][counterset]["name"]);
		tset->imgname=object_cast<std::string>(globals(lua)["tab"]["tilesets"][counterset]["image"]);

		tset->imgwidth=object_cast<int>(globals(lua)["tab"]["tilesets"][counterset]["imagewidth"]);
		tset->imgheight=object_cast<int>(globals(lua)["tab"]["tilesets"][counterset]["imageheight"]);

		tset->spacing=object_cast<int>(globals(lua)["tab"]["tilesets"][counterset]["spacing"]);
		tset->margin=object_cast<int>(globals(lua)["tab"]["tilesets"][counterset]["margin"]);

		tset->firstid=object_cast<int>(globals(lua)["tab"]["tilesets"][counterset]["firstgid"]);

		tmap->tilesets.push_back(tset);
		counterset++;
	}
}

void ScriptEngine::LoadLayers(TiledMap* tmap)
{
	using namespace luabind;

	int counter= 1;
	//layers loop
	for (luabind::iterator i(luabind::globals(lua)["tab"]["layers"]), end; i != end; ++i)
	{
		Layer* tlayer;

		std::string type=object_cast<std::string>(globals(lua)["tab"]["layers"][counter]["type"]);
		if(type=="tilelayer")
		{
			tlayer= new TileLayer(tmap->width, tmap->height);
			//specific fields for TileLayer

		}
		else if(type=="objectgroup")
		{
			tlayer= new ObjectLayer;
		}
		else
		{
			tlayer= new ImageLayer;
		}

		tlayer->name=object_cast<std::string>(globals(lua)["tab"]["layers"][counter]["name"]);

		tlayer->visible=object_cast<bool>(globals(lua)["tab"]["layers"][counter]["visible"]);
		tlayer->opacity =object_cast<float>(globals(lua)["tab"]["layers"][counter]["opacity"]);

		for (luabind::iterator k(luabind::globals(lua)["tab"]["layers"][counter]["properties"]), end; k != end; ++k)
		{
			//important: saving additional properties, pair key-value
			k.key();
			*k;
		}

		Log("layer: ", tlayer->name, "\n");
		if(tlayer->type == LayerType::TILE)
		{
			tmap->tilelayer = dynamic_cast<TileLayer*>(tlayer);
			Log("Copy data to memory\n");

			for(int l=0; l < tmap->height; l++)
			{
				tmap->tilelayer->tiles.push_back( std::vector<Tile*>() );
			}

			int j=0;
			for (luabind::iterator iter(luabind::globals(lua)["tab"]["layers"][counter]["data"]), end;
					iter != end; ++iter)
			{

				Tile* t= new Tile(object_cast<int>( *iter) );

				int x = j / tmap->width;

				tmap->tilelayer->tiles[x].push_back(t);
				j++;
			}

		}
		else if(tlayer->type == LayerType::OBJECT)
		{
			ObjectLayer* objlayer = dynamic_cast<ObjectLayer*>(tlayer);


//            =object_cast<bool>(globals(lua)["tab"]["layers"][counter]["objects"]);

			if(objlayer->name == "collision")
			{
				tmap->collisionlayer = objlayer;
			}
			else if(objlayer->name == "action")
			{
				tmap->actionlayer = objlayer;
			}
			else
			{
				tmap->objectlayers.push_back(objlayer);
			}

			//need add spawnpoints, actionpoints, triggers

			int objcount= 1;
			for (luabind::iterator iter(luabind::globals(lua)["tab"]["layers"][counter]["objects"]), end;
					iter != end; ++iter)
			{
				MapObject* obj = new MapObject;
				obj->name = object_cast<std::string>
							(globals(lua)["tab"]["layers"][counter]["objects"][objcount]["name"]);

				std::string type = object_cast<std::string>
								   (globals(lua)["tab"]["layers"][counter]["objects"][objcount]["type"]);
				//maybe need more types
				if(type == "collision" || objlayer->name == "collision")
					obj->type = MapObjectType::COLLISION;
				else if(type == "destructible")
					obj->type = MapObjectType::DESTRUCTIBLE;
				else if(type == "spawnpoint")
					obj->type = MapObjectType::SPAWNPOINT;
				else if(type == "actionpoint")
					obj->type = MapObjectType::ACTIONPOINT;
				else if(type == "trigger")
					obj->type = MapObjectType::TRIGGER;
				else if(type == "particleemitter")
					obj->type = MapObjectType::PARTICLEEMITTER;
				else if(type == "soundemitter")
				{
					obj->type = MapObjectType::SOUNDEMITTER;
					for (luabind::iterator k(luabind::globals(lua)["tab"]["layers"][counter]
											 ["objects"][objcount]["properties"]), end; k != end; ++k)
					{
						if(k.key() == "deltatime")
						{
							obj->soundemitter.deltatime =
								std::stoi(object_cast<std::string>(*k) );
						}
						else if(k.key() == "soundname")
						{
							obj->soundemitter.soundname = object_cast<std::string>(*k);
						}
						else if(k.key() == "shift")
						{
							obj->soundemitter.shift =
								std::stoi(object_cast<std::string>(*k) );
						}
					}
				}
				else
				{
					Log("Map object from layer: ", objlayer->name," with name: ",obj->name,
						"has not a type!\n");
				}

				std::string shape = object_cast<std::string>
									(globals(lua)["tab"]["layers"][counter]["objects"][objcount]["shape"]);
				if(shape == "rectangle")
					obj->shapetype = Shape::RECTANGLE;
				else if(shape == "polygon")
					obj->shapetype = Shape::POLYGON;
				else if(shape == "ellipse")
					obj->shapetype = Shape::ELLIPSE;
				else //"polyline"
					obj->shapetype = Shape::POLYLINE;

				obj->x = object_cast<float>
						 (globals(lua)["tab"]["layers"][counter]["objects"][objcount]["x"]);
				obj->y = object_cast<float>
						 (globals(lua)["tab"]["layers"][counter]["objects"][objcount]["y"]);

				obj->width = object_cast<float>
							 (globals(lua)["tab"]["layers"][counter]["objects"][objcount]["width"]);
				obj->height = object_cast<float>
							  (globals(lua)["tab"]["layers"][counter]["objects"][objcount]["height"]);

				if(obj->shapetype == Shape::POLYGON)
				{
					int n = 0;
					for (luabind::iterator k(luabind::globals(lua)
											 ["tab"]["layers"][counter]["objects"][objcount]["polygon"]), end; k != end; ++k)
					{
						n++; //counting number of vertices
					}

					obj->vertexcount = n;
					obj->vertices = new b2Vec2[n];

					int m = 0;
					for (luabind::iterator k(luabind::globals(lua)
											 ["tab"]["layers"][counter]["objects"][objcount]["polygon"]), end; k != end; ++k)
					{
						obj->vertices[m].x = object_cast<float>
											 (globals(lua)["tab"]["layers"][counter]["objects"][objcount]["polygon"][k.key()]["x"]);
						obj->vertices[m].y = object_cast<float>
											 (globals(lua)["tab"]["layers"][counter]["objects"][objcount]["polygon"][k.key()]["y"]);
						m++;
					}
				}
				else if(obj->shapetype == Shape::POLYLINE)
				{
					int n = 0;
					for (luabind::iterator k(luabind::globals(lua)
											 ["tab"]["layers"][counter]["objects"][objcount]["polyline"]), end; k != end; ++k)
					{
						n++; //counting number of vertices
					}

					obj->vertexcount = n;
					obj->vertices = new b2Vec2[n];

					int m = 0;
					for (luabind::iterator k(luabind::globals(lua)
											 ["tab"]["layers"][counter]["objects"][objcount]["polyline"]), end; k != end; ++k)
					{
						obj->vertices[m].x = object_cast<int>
											 (globals(lua)["tab"]["layers"][counter]["objects"][objcount]["polyline"][k.key()]["x"]);
						obj->vertices[m].y = object_cast<int>
											 (globals(lua)["tab"]["layers"][counter]["objects"][objcount]["polyline"][k.key()]["y"]);
						m++;
					}
				}

				for (luabind::iterator k(luabind::globals(lua)
										 ["tab"]["layers"][counter]["objects"][objcount]["properties"]), end; k != end; ++k)
				{
					//important: saving additional properties, pair key-value
					k.key();
					*k;
				}

				objlayer->objects.push_back(obj);
				objcount++;
			}



		}

		//tmap->layers.push_back(tlayer);

		counter++;
	}
}

ScriptEngine::ScriptEngine()
{
	Log("Init Script Engine\n");
	lua=luaL_newstate();
	luabind::open(lua);
	luaL_openlibs(lua);

	/*
	luabind::module(lua) [
	    luabind::def("print_hello", print_hello)
	];

	int a=5;
	luabind::as_lua_integer(a);
	*/

	//LoadMap();
	//luaL_openlibs(lua);
}

ScriptEngine::~ScriptEngine()
{
	Log("Close Script Engine\n");
	lua_close(lua);
}


int ScriptEngine::RunScript(const std::string& name)
{
	int code=luaL_dofile(lua, name.c_str());

	if(code)
		Log("[LUA] Error code: ", code, "\n");
	return code;
}
