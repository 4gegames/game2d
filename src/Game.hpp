#ifndef GAME_HPP_INCLUDED
#define GAME_HPP_INCLUDED

#include "Settings.hpp"
#include "Rendering/Renderer.hpp"
#include "Rendering/RendererOGL2.hpp"
#include "Rendering/RendererOGL3.hpp"
#include "Controllers.hpp"
#include "FPSCounter.hpp"
#include "Audio/Audio.hpp"
#include "ResourceManager/ResourceManager.hpp"

class Game
{
	public:
		static FPSCounter* fps;
		static Controllers* ctrl;
		static Settings* settings;
		static Renderer* renderer;
		static Audio* audio;
		static ResourceManager* resources;

		static SDL_Rect screenrect;

		static void Init();
		static void Clean();
};

#endif // GAME_HPP_INCLUDED
