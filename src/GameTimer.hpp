#ifndef GAMETIMER_HPP_INCLUDED
#define GAMETIMER_HPP_INCLUDED

#include "Game.hpp"

class GameTimer
{
		unsigned int timer;
	public:

		GameTimer(unsigned int start_shift = 0)
		{
			timer = Game::fps->GetGameplayTime() + start_shift;
		}

		bool Passed()
		{
			if(timer < Game::fps->GetGameplayTime())
				return true;
			else
				return false;
		}

		void AddDelta(unsigned int value)
		{
			timer = Game::fps->GetGameplayTime() + value;
		}

		~GameTimer() { }
};


#endif // GAMETIMER_HPP_INCLUDED
