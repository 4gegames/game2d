#include "Audio.hpp"

Audio::Audio()
{
	Mix_Init(MIX_INIT_MP3 | MIX_INIT_OGG);
	Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096); // or 48000
	Log("Audio driver: ",SDL_GetCurrentAudioDriver(),"\n");
	Log("Allocate channels: ",Mix_AllocateChannels(-1),"\n");
	soundsvolume = 100;

	idmusic = "";
}

Audio::~Audio()
{
	Mix_HaltMusic();
	Mix_CloseAudio();
	Mix_Quit();
}

Mix_Music* Audio::GetIdMusic(const std::string& name)
{
	if(musics.find(name)!=musics.end())
	{
		return musics.find(name)->second;
	}
	else
	{
		Log("Error, music is not loaded\n");
		return NULL;
	}
}

void Audio::AddMusic(const std::string& name, const char* path)
{
	musics[name] =Mix_LoadMUS(path);
	if(musics[name] ==NULL)
		Log("Error, music is not loaded. Error code: ",Mix_GetError(),"\n");
}

void Audio::DeleteMusic(const std::string& name)
{
	Mix_FreeMusic(musics.find(name)->second);
	musics.erase(musics.find(name));
}

Mix_Chunk* Audio::GetIdSound(const std::string& name)
{
	if(sounds.find(name)!= sounds.end())
	{
		return sounds.find(name)->second;
	}
	else
	{
		return NULL;
	}
}

void Audio::AddSound(const std::string& name, const std::string& path)
{
	sounds[name] =Mix_LoadWAV(path.c_str());
	Mix_VolumeChunk(sounds[name], soundsvolume);//Game::settings->options.volumeSound);
	//need test validate of loading file
}

void Audio::DeleteSound(const std::string& name)
{
	Mix_FreeChunk(sounds.find(name)->second);
	sounds.erase(sounds.find(name));
}

int Audio::SoundsSize()
{
	return sounds.size();
}

int Audio::MusicsSize()
{
	return musics.size();
}

void Audio::PlayMusic(const std::string& name)
{
	//if(Settings::volumeMusic)
	if(name != idmusic)
		Mix_FadeInMusic(musics.find(name)->second, -1, 1000); //need add parameter looped

	idmusic = name;
}

void Audio::PlaySound(const std::string& name)
{
	Mix_PlayChannel(-1, GetIdSound(name), 0);
}
