#ifndef AUDIO_HPP_INCLUDED
#define AUDIO_HPP_INCLUDED

#include<string>
#include<map>
#include<queue>

#include<SDL2/SDL_mixer.h>

#include "../Utils/Log.hpp"

enum class MusicCommandType
{
	FADEIN,
	FADEOUT
};

struct MusicCommand
{
	MusicCommandType type;
	int fadetime;
	std::string nextname; // only for fade in effect
};

class Audio
{
		std::map<std::string, Mix_Music*> musics;
		std::map<std::string, Mix_Chunk*> sounds;
		int soundsvolume;

		std::queue<MusicCommand> commands;

		std::string idmusic;
	public:
		Audio();
		~Audio();

		Mix_Music* GetIdMusic(const std::string& name);
		void AddMusic(const std::string& name, const char* path);
		void DeleteMusic(const std::string& name);

		Mix_Chunk* GetIdSound(const std::string& name);
		void AddSound(const std::string& name, const std::string& path);
		void DeleteSound(const std::string& name);

		void SetVolumeMusic(int volume)
		{
			Mix_VolumeMusic(volume); //0..128
		}

		//for single chunk
		void SetVolumeChunk(const std::string& name, int volume)
		{
			Mix_VolumeChunk(GetIdSound(name), volume);
		}

		//for all sounds
		void SetVolumeSounds(int volume)
		{
			soundsvolume = volume;
		}
		int SoundsSize();
		int MusicsSize();

		void PlayMusic(const std::string& name);
		void PlaySound(const std::string& name);

		void StopMusic(int fadeout)
		{
			Mix_FadeOutMusic(fadeout);
		}
};

#endif // AUDIO_HPP_INCLUDED
