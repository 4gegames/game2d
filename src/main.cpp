#include "StateManager.hpp"

#ifdef WIN32
#undef main
#endif

int main(int argc, char* argv[])
{
	StateManager manager;
	while(manager.exit != true)
		manager.Update();

	return 0;
}
