#include "Panel.hpp"

Panel::Panel(const std::string& name, PanelLayout lay, bool cover, SDL_Rect source,
			 AlignmentX align, AlignmentY align2)
	: GuiObject(name, 0,0, GuiDepth::PANEL)
{
	layout = lay;
	img->Align(source, align, align2);
	covergameplay = cover;
}

Panel::Panel(Image* imgp)
{
	img = imgp;
	todestroy = false;
	depth = GuiDepth::PANEL;
	covergameplay = false;
	layout = PanelLayout::UNBOUND;
}

void Panel::SetCoverGameplay(bool value)
{
	covergameplay = value;
}

bool Panel::GetCoverGameplay()
{
	return covergameplay;
}

PanelLayout Panel::GetLayout()
{
	return layout;
}

AlignmentX Panel::GetHorizontalAlignment()
{
	return img->GetHorizontalAlignment();
}

AlignmentY Panel::GetVerticalAlignment()
{
	return img->GetVerticalAlignment();
}

void Panel::Align(SDL_Rect source, AlignmentX align, AlignmentY align2)
{
	img->Align(source, align, align2);

	for(auto it = relatives.begin(); it != relatives.end(); ++it)
	{
		(*it)->ApplyLayout(img->position);
	}
}

void Panel::SetDisplay(bool value)
{
	img->SetDisplay(value);

	//in loop all children
	for(auto it = relatives.begin(); it != relatives.end(); ++it)
	{
		(*it)->SetDisplay(value);
	}
}

void Panel::PanelEnable(bool value)
{
	img->SetDisplay(value);

	for(auto it = relatives.begin(); it != relatives.end(); ++it)
	{
		(*it)->SetDisplay(value);
	}
}

void Panel::AddToPanel(GuiObject* obj, AlignmentX align, AlignmentY align2)
{
	obj->Align(GetPosition(), align, align2);
	//obj->ApplyLayout(img->position);
	relatives.push_back(obj);
}

void Panel::Update()
{
	for(auto it = relatives.begin(); it != relatives.end(); ++it)
	{
		(*it)->Update();
	}
}

void Panel::Render(Camera* cam)
{
	img->Render(cam);

	for(auto it = relatives.begin(); it != relatives.end(); ++it)
	{
		(*it)->Render(cam);
	}
}

Panel::~Panel()
{
	relatives.clear();
}
