#ifndef BUTTON_HPP_INCLUDED
#define BUTTON_HPP_INCLUDED

#include <functional>
#include "../Game.hpp"
#include "GuiObject.hpp"

typedef std::function<void()> OnClickDelegate;

enum class ButtonType
{
	BUTTON,
	CHECKBOX,
	SLIDERLEFT,
	SLIDERRIGHT,
	SLIDERCENTER
};

class Button : public GuiObject
{
	protected:
		bool timeinit;
		bool permhovrender; //permanent rendering of hover image

		//ButtonType type;
		unsigned int timer;
		int timestep;

		/*int* valueslider; //to slider
		int maxslidervalue;*/
		Key* key;

		GuiDepth hoverdepth;

		//checkbox
		bool checkbox;
		bool* checkboxvalue;
		int imgwidth;

		bool CheckPosition(int x, int y);

		Image* hoverimg;
		Image* label;

		void AlignHoverImage();
		void AlignLabel();

	public:
		OnClickDelegate click = nullptr;
		OnClickDelegate hover = nullptr;

		Button(const std::string& model, const std::string& labelname = "",
			   const std::string& keyname = "", int x = 0, int y = 0);
		//load model with this name from buttons.csv
		Button(const std::string& model, bool* value,
			   const std::string& keyname = "", int x = 0, int y = 0);

		~Button();

		void SetDisplay(bool value);

		Image* GetHoverImage();
		Image* GetLabelImage();

		void ChangeLabel(const std::string& labelname);

		void OnClick();
		void OnHover();

		void ApplyLayout(SDL_Rect source);

		void Align(SDL_Rect source, AlignmentX value = AlignmentX::NONE,
				   AlignmentY value2 = AlignmentY::NONE);
		void Translate(int x, int y);

		void SetPermHovRender(bool param);

		void Update();
		void Render(Camera* cam);
};

#endif // BUTTON_HPP_INCLUDED
