#ifndef PANEL_HPP_INCLUDED
#define PANEL_HPP_INCLUDED

#include <list>

#include "../Images.hpp"
#include "GuiObject.hpp"

/**
if Layout is horizontal and cover == true, then viewport is reduced vertical
**/
enum class PanelLayout
{
	HORIZONTAL,
	VERTICAL,
	UNBOUND
};

class Panel : public GuiObject
{
		bool covergameplay;
		std::vector<GuiObject*> relatives;
		PanelLayout layout;

	public:
		void SetCoverGameplay(bool value); //if true, modify viewport
		bool GetCoverGameplay();

		PanelLayout GetLayout();

		AlignmentX GetHorizontalAlignment();
		AlignmentY GetVerticalAlignment();

		void Align(SDL_Rect source, AlignmentX align,
				   AlignmentY align2 = AlignmentY::NONE);

		void SetDisplay(bool value);
		void PanelEnable(bool value);

		void AddToPanel(GuiObject* obj, AlignmentX align,
						AlignmentY align2);

		void Update();
		void Render(Camera* cam);

		Panel(const std::string& name, PanelLayout lay, bool cover, SDL_Rect source,
			  AlignmentX align, AlignmentY align2 = AlignmentY::NONE);
		Panel(Image* imgp);
		~Panel();
};



#endif // PANEL_HPP_INCLUDED
