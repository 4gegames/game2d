#include "Button.hpp"


Button::Button(const std::string& modelname, const std::string& labelname, const std::string& keyname, int x, int y)
{
	checkbox = false;
	depth = GuiDepth::BUTTON;
	ButtonModel data = Game::resources->GetButtonData(modelname);
	img = new Image(data.img, IsGui::GUISHIFT, x, y);


	if(data.hoverimg != "")
	{
		if(data.underbutton)
			hoverdepth = GuiDepth::UNDERBUTTON;
		else
			hoverdepth = GuiDepth::ABOVEBUTTON;

		hoverimg = new Image(data.hoverimg, IsGui::GUISHIFT);
	}
	else
		hoverimg = nullptr;

	if(labelname != "")
		label = new Image(labelname, IsGui::GUISHIFT);
	else
		label = nullptr;

	//init position of hoverimg and label
	AlignHoverImage();
	AlignLabel();

	timestep = data.timestep;
	if(timestep > 0)
	{
		timeinit = true;
		timer = SDL_GetTicks();
	}
	else
		timeinit = false;

	permhovrender = false;

	if(keyname != "")
	{
		key = Game::ctrl->GetKey(keyname);
	}
	else
		key = nullptr;
}

Button::Button(const std::string& modelname, bool* value, const std::string& keyname, int x, int y)
{
	checkbox = true;
	depth = GuiDepth::BUTTON;
	ButtonModel data = Game::resources->GetButtonData(modelname);

	checkboxvalue = value;

	TextureData* texture = Game::resources->GetTextureData(data.img).get();

	imgwidth = texture->w / 2;
	img = new Image(data.img, IsGui::GUISHIFT, x, y, 0,0, imgwidth, texture->h);

	if(data.hoverimg != "")
	{
		if(data.underbutton)
			hoverdepth = GuiDepth::UNDERBUTTON;
		else
			hoverdepth = GuiDepth::ABOVEBUTTON;

		hoverimg = new Image(data.hoverimg, IsGui::GUISHIFT);
	}
	else
		hoverimg = nullptr;

	label = nullptr;

	//init position of hoverimg and label
	AlignHoverImage();

	timestep = data.timestep;
	if( !timestep) //checkboxes must have init time
	{
		timestep = 100;
	}

	timeinit = true;
	timer = SDL_GetTicks();

	permhovrender = false;

	if(keyname != "")
	{
		key = Game::ctrl->GetKey(keyname);
	}
	else
		key = nullptr;
}

bool Button::CheckPosition(int x, int y)
{
	if(x>=img->position.x && x<=(img->position.x+img->position.w)
			&& y>=img->position.y && y<=(img->position.y+img->position.h) )
	{
		return true;
	}
	else
		return false;
}

void Button::AlignHoverImage()
{
	if(hoverimg != nullptr)
	{
		int shiftx = (hoverimg->position.w - img->position.w) / 2;
		int shifty = (hoverimg->position.h - img->position.h) / 2;

		hoverimg->position.x = img->position.x-shiftx;//*Game::settings->options.ratio_x;
		hoverimg->position.y = img->position.y-shifty;//*Game::settings->options.ratio_x;
		hoverimg->SetDisplay(false);
	}
}

void Button::AlignLabel()
{
	if(label != nullptr)
		label->Align(img->position, AlignmentX::CENTER, AlignmentY::CENTER);
}

void Button::SetDisplay(bool value)
{
	img->SetDisplay(value);

	if(label != nullptr)
		label->SetDisplay(value);
}

Image* Button::GetHoverImage()
{
	return hoverimg;
}

Image* Button::GetLabelImage()
{
	return label;
}

void Button::ChangeLabel(const std::string& labelname)
{
	if(label != nullptr)
		delete label;

	label = new Image(labelname);
	label->Align(img->position, AlignmentX::CENTER, AlignmentY::CENTER);
}


void Button::OnClick()
{
	if(img->GetDisplay())
	{
		if(checkbox)
		{
			if( *checkboxvalue)
				img->ChangeSource(imgwidth, 0);
			else
				img->ChangeSource(0, 0);
		}

		if( (Game::ctrl->lmb_state == MouseState::BUTTONUP
				&& CheckPosition(Game::ctrl->mouse_x, Game::ctrl->mouse_y)
				&& CheckPosition(Game::ctrl->click_x, Game::ctrl->click_y) )
				|| Game::ctrl->Check(key) )
		{
			if(timeinit && (timer<SDL_GetTicks()))
			{
				if(click)
					click();

				if(checkbox)
					*checkboxvalue = !(*checkboxvalue);

				timer= SDL_GetTicks()+timestep;
			}
			else if(!timeinit)
			{
				if(click)
					click();
			}
		}
	}
}

void Button::OnHover()
{
	if(img->GetDisplay() && hoverimg != nullptr)
	{
		if(permhovrender)
		{
			hoverimg->SetDisplay(false);
		}
		else if(permhovrender!=true)
		{
			hoverimg->SetDisplay(false);
			if(CheckPosition(Game::ctrl->mouse_x, Game::ctrl->mouse_y))
			{
				hoverimg->SetDisplay(true);
			}
		}

		if(hover && CheckPosition(Game::ctrl->mouse_x, Game::ctrl->mouse_y))
		{
			hover();
		}
	}
}

void Button::ApplyLayout(SDL_Rect source)
{
	img->ApplyLayout(source);
	AlignHoverImage();
	AlignLabel();
}

void Button::Align(SDL_Rect source, AlignmentX value, AlignmentY value2)
{
	img->Align(source, value, value2);

	AlignHoverImage();
	AlignLabel();
}

void Button::Translate(int x, int y)
{
	img->Translate(x, y);

	AlignHoverImage();
	if(label != nullptr)
		label->Align(img->position, AlignmentX::CENTER, AlignmentY::CENTER);
}

void Button::SetPermHovRender(bool param)
{
	permhovrender = param;
}

void Button::Update()
{
	OnClick();
	OnHover();
}

void Button::Render(Camera* cam)
{
	if(hoverimg != nullptr)
	{
		if(hoverdepth > depth)
		{
			img->Render(cam);
			hoverimg->Render(cam);
		}
		else
		{
			hoverimg->Render(cam);
			img->Render(cam);
		}
	}
	else
		img->Render(cam);

	if(label != nullptr)
		label->Render(cam);
}


Button::~Button()
{
	delete hoverimg;
}
