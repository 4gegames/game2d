#ifndef GUIOBJECT_HPP_INCLUDED
#define GUIOBJECT_HPP_INCLUDED

#include <list>
#include "../Images.hpp"

enum GuiDepth
{
	BACKGROUND = 0,
	PANEL = 1,
	DEFAULT = 3,
	UNDERBUTTON = 4,
	BUTTON = 5,
	ABOVEBUTTON = 6,
	TEXT = 7,
	CURSOR = 8
};

class GuiObject
{
	protected:
		Image* img;

	public:
		int depth;

		bool todestroy;

		virtual void Render(Camera* cam)
		{
			img->Render(cam);
		}

		virtual void Update() { }

		Image* GetImage()
		{
			return img;
		}

		SDL_Rect GetPosition()
		{
			return img->position;
		}

		void Destroy()
		{
			todestroy = true;
		}

		virtual void ApplyLayout(SDL_Rect source)
		{
			img->ApplyLayout(source);
		}

		virtual void Align(SDL_Rect source, AlignmentX value = AlignmentX::NONE,
						   AlignmentY value2 = AlignmentY::NONE)
		{
			img->Align(source, value, value2);
		}

		virtual void Translate(int x, int y)
		{
			img->Translate(x, y);
		}

		virtual void SetDisplay(bool value)
		{
			img->SetDisplay(value);
		}

		virtual bool GetDisplay()
		{
			return img->GetDisplay();
		}
		/*
				GuiObject(cost std::string& textid, int x, int y, const std::string& fontname,
						  SDL_Color color)
				{

				}
		*/
		GuiObject(const std::string& name, int x = 0, int y = 0, int pdepth = GuiDepth::DEFAULT,
				  IsGui isgui = IsGui::GUISHIFT)
		{
			img = new Image(name, isgui, x, y);
			depth = pdepth;
			todestroy = false;
		}

		GuiObject(Image* imgp, int pdepth = GuiDepth::DEFAULT)
		{
			img = imgp;
			todestroy = false;
			depth = pdepth;
		}
		GuiObject()
		{
			img = nullptr;
			todestroy = false;
			depth = GuiDepth::DEFAULT;
		}

		virtual ~GuiObject()
		{
			delete img;
		}
};


#endif // GUIOBJECT_HPP_INCLUDED
