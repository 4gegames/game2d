#ifndef CURSOR_HPP_INCLUDED
#define CURSOR_HPP_INCLUDED

#include "GuiObject.hpp"

class Cursor : public GuiObject
{
	public:
		void Update()
		{
			GetImage()->position.x = Game::ctrl->mouse_x;
			GetImage()->position.y = Game::ctrl->mouse_y;
		}

		Cursor(const std::string& name)
			: GuiObject(name, 0,0, GuiDepth::CURSOR)
		{}

		~Cursor()
		{}
};



#endif // CURSOR_HPP_INCLUDED
