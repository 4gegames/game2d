#include "Images.hpp"

Image::Image(const std::string& texturename, IsGui is_gui, int x, int y, int w, int h)
{
	id = Game::resources->GetImageID();
	texturedata = Game::resources->GetTextureData(texturename);

	angle = 0;

	int shift;
	float ratio_x;

	isgui = is_gui;
	if(isgui == IsGui::NONE)
	{
		shift = 0;
		ratio_x = 1;
	}
	else if(isgui == IsGui::GUI)
	{
		shift = 0;
		ratio_x = Game::settings->options.ratio_x;
	}
	else //IsGui::GUISHIFT
	{
		shift = Game::settings->options.shift;
		ratio_x = Game::settings->options.ratio_x;
	}

	position.x = (x + shift) * ratio_x;
	position.y = y * ratio_x;

	if(w == 0)
		position.w = texturedata.get()->w * ratio_x;
	else
		position.w = w * ratio_x;
	if(h == 0)
		position.h = texturedata.get()->h * ratio_x;
	else
		position.h = h * ratio_x;

	displayed = true;
	source = false;

	src.left = 0;
	src.right = 1;
	src.top = 0;
	src.bottom = 1;
	src.w = w;
	src.h = h;

	align_x = AlignmentX::NONE;
	align_y = AlignmentY::NONE;

	givenpos = position;
}

Image::Image(const std::string& texturename, IsGui is_gui, int x, int y, int srcx, int srcy, int srcw, int srch)
{
	id = Game::resources->GetImageID();
	texturedata = Game::resources->GetTextureData(texturename);

	angle = 0;

	int shift;
	float ratio_x;

	isgui = is_gui;
	if(isgui == IsGui::NONE)
	{
		shift = 0;
		ratio_x = 1;
	}
	else if(isgui == IsGui::GUI)
	{
		shift = 0;
		ratio_x = Game::settings->options.ratio_x;
	}
	else //IsGui::GUISHIFT
	{
		shift = Game::settings->options.shift;
		ratio_x = Game::settings->options.ratio_x;
	}

	position.x = (x + shift) * ratio_x;
	position.y = y * ratio_x;

	float x1 = srcx * ratio_x;
	float y1 = srcy * ratio_x;

	src.w = srcw * ratio_x;
	src.h = srch * ratio_x;

	displayed = true;
	source = true;

	float texture_w = texturedata.get()->w * ratio_x;
	float texture_h = texturedata.get()->h * ratio_x;

	src.left = x1 / texture_w;
	src.right = src.left + (src.w / texture_w);
	src.top = y1 / texture_h;
	src.bottom = src.top + (src.h / texture_h);

	position.w = src.w;
	position.h = src.h;

	align_x = AlignmentX::NONE;
	align_y = AlignmentY::NONE;

	givenpos = position;
}

void Image::ChangeSource(int x, int y)
{
	float ratio_x;

	if(isgui == IsGui::NONE)
		ratio_x = 1;
	else
		ratio_x = Game::settings->options.ratio_x;

	float texture_w = texturedata.get()->w * ratio_x;
	float texture_h = texturedata.get()->h * ratio_x;
	float x1 = x * ratio_x;
	float y1 = y * ratio_x;
	src.left = x1 / texture_w;
	src.right = src.left + (src.w / texture_w);
	src.top = y1 / texture_h;
	src.bottom = src.top + (src.h / texture_h);
}

void Image::Scale(float xmultiplier, float ymultiplier)
{
	position.w = givenpos.w * xmultiplier;
	position.h = givenpos.h * ymultiplier;
}

void Image::SetAngle(int angleparam)
{
	angle = angleparam;
}

void Image::Translate(int x, int y)
{
	if(isgui != IsGui::NONE)
	{
		x *= Game::settings->options.ratio_x;
		y *= Game::settings->options.ratio_x;
	}

	position.x += x;
	position.y += y;

	givenpos.x += x;
	givenpos.y += y;
}

void Image::Align(SDL_Rect pos, AlignmentX x, AlignmentY y)
{
	float u = 0;
	float w = 0;
	align_x = x;
	align_y = y;

	switch(align_x)
	{
		case AlignmentX::LEFT:
			u = 0;
			break;
		case AlignmentX::RIGHT:
			u = pos.w - position.w;
			break;
		case AlignmentX::CENTER:
			if(pos.w > position.w)
			{
				u = (pos.w - position.w) / 2;
			}
			else
				u = 0;
			break;
		case AlignmentX::NONE:
			break;
	}

	if(align_x != AlignmentX::NONE)
		position.x = pos.x + u;


	switch(align_y)
	{
		case AlignmentY::UP:
			w = 0;
			break;
		case AlignmentY::DOWN:
			w = pos.h - position.h;
			break;
		case AlignmentY::CENTER:
			if(pos.h > position.h)
				w = (pos.h - position.h) / 2;
			else
				w = 0;
			break;
		case AlignmentY::NONE:
			break;
	}

	if(align_y != AlignmentY::NONE)
		position.y = pos.y+w;
}

void Image::ApplyLayout(SDL_Rect destination)
{
	position.x = givenpos.x + destination.x;
	position.y = givenpos.y + destination.y;
	Align(destination, align_x, align_y);
}

AlignmentX Image::GetHorizontalAlignment()
{
	return align_x;
}

AlignmentY Image::GetVerticalAlignment()
{
	return align_y;
}

void Image::Render(Camera* camera)
{
	if(displayed)
	{
		if(angle > 360 || angle < -360)
			angle = 0;

		Game::renderer->Render(texturedata.get()->id, position, src, camera, angle);
	}
}

void Image::SetDisplay(bool value)
{
	displayed = value;
}

bool Image::GetDisplay()
{
	return displayed;
}

Image::~Image()
{

}

