#ifndef SCRIPTENGINE_HPP_INCLUDED
#define SCRIPTENGINE_HPP_INCLUDED

#include<lua.hpp>
#include<luabind/luabind.hpp>

#include "Utils/Log.hpp"

#include "Utils/Singleton.hpp"
#include "Game.hpp"
#include "Spooky/tilemap.hpp"

class ScriptEngine : public Singleton<ScriptEngine>
{
		lua_State* lua;

	public:
		ScriptEngine();
		~ScriptEngine();

		int RunScript(const std::string& name);
		TiledMap* LoadMap(const std::string& name);
		void LoadTilesets(TiledMap* tmap);
		void LoadLayers(TiledMap* tmap);
};

#endif // SCRIPTENGINE_HPP_INCLUDED
